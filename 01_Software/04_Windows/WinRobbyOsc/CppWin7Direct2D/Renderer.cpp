/****************************** Module Header ******************************\
 Module Name:  Renderer.cpp
 Project:      CppWin7Direct2D
 Copyright (c) Microsoft Corporation.

 The Renderer class is responsible for most of the rendering task.

 This source is subject to the Microsoft Public License.
 See http://www.microsoft.com/en-us/openness/licenses.aspx#MPL
 All other rights reserved.

 THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
 EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
 WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/


#include "Stdafx.h"
#include "Renderer.h"

// Dash array for dashStyle D2D1_DASH_STYLE_CUSTOM
float dashes[] = {3.0f, 2.0f, 1.0f, 2.0f};

Renderer::Renderer(HWND hwnd, int width, int height)
{
	this->m_hwnd = hwnd;
	this->m_pDirect2dFactory = NULL;
	this->m_pRenderTarget = NULL;
	this->m_animate = false;
	this->m_animateTranslateX = 0;
	this->m_animateToRight = true;
	this->CreateDeviceIndependentResources();

	for (int i = 0; i < 1000; i++)
	{ 
		signal1[i]=0.0;
		signal2[i]=10.0;
		signal3[i]=20.0;
	}
	cur_xtime = 0;
	com_connect = FALSE;

	s1_ampl = 0.03;
    s1_offs = 0.0;
	s1_enable = true;

	s2_ampl = 0.03;
    s2_offs = 0.0;
	s2_enable = true;

	s3_ampl = 1.0;
    s3_offs = 0.0;
	s3_enable = false;

	s1_trig_enable = false;
	s1_trig_value = 260;
	s1_trig_lower = false;

	s2_trig_enable = false;
	s2_trig_value = 260;
	s2_trig_lower = false;

	trig_delay = 500;
	trig_flag = false;
	trig_cnt  = 500;

	freeze = false;

	debug_en = false;
}

Renderer::~Renderer(void)
{
	this->DiscardResources();
}

// Device independent resources are not specific to a particular device. For example, factory and font.
HRESULT Renderer::CreateDeviceIndependentResources()
{
	HRESULT hr = S_OK;
	// Create Direct 2D factory.
	hr = D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &m_pDirect2dFactory);
	return hr;
}

// Device dependent resources are specific to a particular device. For example,brush may render differently on different devices (hwnd, DXGI (Direct3D surface), etc...).
HRESULT Renderer::CreateDeviceResources()
{
	HRESULT hr = S_OK;
	if(!this->m_pRenderTarget)
	{
		// Create an HwndRenderTarget to draw to the hwnd.
		RECT rc;
		GetClientRect(m_hwnd, &rc);
		D2D1_SIZE_U size = D2D1::SizeU(rc.right - 100 - rc.left, rc.bottom - rc.top);
		hr = this->m_pDirect2dFactory->CreateHwndRenderTarget(
			D2D1::RenderTargetProperties(),
			D2D1::HwndRenderTargetProperties(this->m_hwnd, size),
			&m_pRenderTarget
			);
		if (SUCCEEDED(hr))
		{
			// Create a SolidColorBrush (planet background).
			hr = m_pRenderTarget->CreateSolidColorBrush(
				D2D1::ColorF(D2D1::ColorF::CadetBlue),
				&m_pSignal2Brush
				);
		}
		if (SUCCEEDED(hr))
		{
			// Create a SolidColorBrush (star).
			hr = m_pRenderTarget->CreateSolidColorBrush(
				D2D1::ColorF(D2D1::ColorF::Red),
				&m_pSignal1Brush
				);
		}
		if (SUCCEEDED(hr))
		{
			// Create a SolidColorBrush (star).
			hr = m_pRenderTarget->CreateSolidColorBrush(
				D2D1::ColorF(D2D1::ColorF::Yellow),
				&m_pSignal3Brush
				);
		}
		//if (SUCCEEDED(hr))
		//{
		//	// Create a SolidColorBrush (continent).
		//	hr = m_pRenderTarget->CreateSolidColorBrush(
		//		D2D1::ColorF(D2D1::ColorF(0x0BFF00)),
		//		&m_pContinentBrush
		//		);
		//}
		if (SUCCEEDED(hr))
		{
			// Create a SolidColorBrush (small stars).
			hr = m_pRenderTarget->CreateSolidColorBrush(
				D2D1::ColorF(D2D1::ColorF::White),
				&m_pAxisCoordBrush
				);
		}
		if (SUCCEEDED(hr))
		{

			hr = m_pDirect2dFactory->CreateStrokeStyle(
						D2D1::StrokeStyleProperties(
							D2D1_CAP_STYLE_FLAT,
							D2D1_CAP_STYLE_FLAT,
							D2D1_CAP_STYLE_ROUND,
							D2D1_LINE_JOIN_MITER,
							10.0f,
							D2D1_DASH_STYLE_CUSTOM,
							0.0f),
					        dashes,
							ARRAYSIZE(dashes),
							&m_pStrokeStyle
							);
}

		//// Create a RadialGradientBrush (star outline).
		//if (SUCCEEDED(hr))
		//{
		//	hr = this->CreateStarOutlineBrush();
		//}
		//// Prepare the geometry for the star's outline.
		//if (SUCCEEDED(hr))
		//{
		//	hr = this->	CreateAxisCoord();
		//}
		//// Prepare the geometry for the planet's up path.
		//if (SUCCEEDED(hr))
		//{
		//	hr = this->CreatePlanetUpPath();
		//}
		//// Prepare the geometry for the planet's down path.
		//if (SUCCEEDED(hr))
		//{
		//	hr = this->CreatePlanetDownPath();
		//}
	}
	return hr;
}

// Create a RadialGradientBrush (star outline).
//HRESULT Renderer::CreateStarOutlineBrush()
//{
//	HRESULT hr = S_OK;
//	ID2D1GradientStopCollection* gradientStopCollection;
//	D2D1_GRADIENT_STOP gradientStops[2];
//	gradientStops[0].color = D2D1::ColorF(0xFF7A00);
//	gradientStops[0].position = 0.72093f;
//	gradientStops[1].color = D2D1::ColorF(0xEBFF00, 0.5f);
//	gradientStops[1].position = 1.0f;
//	hr = this->m_pRenderTarget->CreateGradientStopCollection(gradientStops, 2, &gradientStopCollection);
//	hr = m_pRenderTarget->CreateRadialGradientBrush(
//		D2D1::RadialGradientBrushProperties(D2D1::Point2F(95, 95), D2D1::Point2F(0, 0), 95, 95),
//		gradientStopCollection,
//		&this->m_pStartOutlineBrush
//		);
//	return hr;
//}

//The main rendering function.
HRESULT Renderer::Render()
{

	HRESULT hr = S_OK;
	hr = this->CreateDeviceResources();
	if (SUCCEEDED(hr))
	{
		this->m_pRenderTarget->BeginDraw();

		// Do some clearing.
		this->m_pRenderTarget->SetTransform(D2D1::Matrix3x2F::Identity());
		this->m_pRenderTarget->Clear(D2D1::ColorF(D2D1::ColorF::Black));
	}
	if (SUCCEEDED(hr))
	{
		D2D1_SIZE_F size = m_pRenderTarget->GetSize();
		D2D1_RECT_F rectBackground = D2D1::RectF(0, 0, size.width, size.height);

		//Get the size of the RenderTarget.
		float rtWidth = this->m_pRenderTarget->GetSize().width;
		float rtHeight = this->m_pRenderTarget->GetSize().height;

		////Draw some small stars
		//for (int i = 0; i < 300; i++)
		//{
		//	int x = (float)(rand()) / RAND_MAX * rtWidth;
		//	int y = (float)(rand()) / RAND_MAX * rtHeight;
		//	D2D1_ELLIPSE smallStar = D2D1_ELLIPSE();
		//	smallStar.point = D2D1::Point2F(x, y);
		//	smallStar.radiusX = 1;
		//	smallStar.radiusY = 1;
//			this->m_pRenderTarget->FillEllipse(&smallStar, this->m_pSmallStarBrush);
//		}
//		this->m_pRenderTarget->DrawLine(D2D1::Point2F(0, 0), D2D1::Point2F(100, 100), this->m_pAxisCoordBrush, 1.0,0);

		//D2D1_ELLIPSE planet = D2D1_ELLIPSE();
		//planet.point = D2D1::Point2F(100, 100);
		//planet.radiusX = 100;
		//planet.radiusY = 100;

		// When animating from right to left, draw the planet afte the star so it has a smaller z-index, and will be covered by the star.
		//if(!this->m_animateToRight)
		//{
		//	this->DrawPlanet(planet);
		//}

		// Draw the star.
		/*D2D1_ELLIPSE star = D2D1_ELLIPSE();
		star.point = D2D1::Point2F(95, 95);
		star.radiusX = 75;
		star.radiusY = 75;*/
		// Scale the star, and translate it to the center of the screen. Note if translation is performed before scaling, you'll get different result.
//		D2D1::Matrix3x2F scaleMatrix = D2D1::Matrix3x2F::Scale(2, 2, D2D1::Point2F(95, 95));
//		D2D1::Matrix3x2F translationMatrix = D2D1::Matrix3x2F::Translation(rtWidth / 2 - 95, rtHeight / 2 - 95);
//		this->m_pRenderTarget->SetTransform(scaleMatrix * translationMatrix);
		this->m_pRenderTarget->DrawLine(D2D1::Point2F(50, rtHeight-50),D2D1::Point2F(1050, rtHeight-50),this->m_pAxisCoordBrush,1.0,0);
		this->m_pRenderTarget->DrawLine(D2D1::Point2F(50, rtHeight-50),D2D1::Point2F(50, rtHeight-650),this->m_pAxisCoordBrush,1.0,0);

		for (int i = 0; i < 10; i++)
		{
			this->m_pRenderTarget->DrawLine(D2D1::Point2F(50 + i *100, rtHeight-50),D2D1::Point2F(50 + i *100, rtHeight-40),this->m_pAxisCoordBrush,1.0,0);
		}
		this->m_pRenderTarget->DrawLine(D2D1::Point2F(40, rtHeight-350),D2D1::Point2F(50, rtHeight-350),this->m_pAxisCoordBrush,1.0,0);
		this->m_pRenderTarget->DrawLine(D2D1::Point2F(45, rtHeight-350+100),D2D1::Point2F(50, rtHeight-350+100),this->m_pAxisCoordBrush,1.0,0);
		this->m_pRenderTarget->DrawLine(D2D1::Point2F(45, rtHeight-350+200),D2D1::Point2F(50, rtHeight-350+200),this->m_pAxisCoordBrush,1.0,0);
		this->m_pRenderTarget->DrawLine(D2D1::Point2F(45, rtHeight-350+300),D2D1::Point2F(50, rtHeight-350+300),this->m_pAxisCoordBrush,1.0,0);
		this->m_pRenderTarget->DrawLine(D2D1::Point2F(45, rtHeight-350-100),D2D1::Point2F(50, rtHeight-350-100),this->m_pAxisCoordBrush,1.0,0);
		this->m_pRenderTarget->DrawLine(D2D1::Point2F(45, rtHeight-350-200),D2D1::Point2F(50, rtHeight-350-200),this->m_pAxisCoordBrush,1.0,0);
		this->m_pRenderTarget->DrawLine(D2D1::Point2F(45, rtHeight-350-300),D2D1::Point2F(50, rtHeight-350-300),this->m_pAxisCoordBrush,1.0,0);

		this->m_pRenderTarget->DrawLine(D2D1::Point2F(50, rtHeight-350),D2D1::Point2F(1050, rtHeight-350),this->m_pAxisCoordBrush,0.5,0);

		if (this->s1_enable)
			for (int i = 1; i < 999; i++)
			{
				//D2D1_ELLIPSE signal_point = D2D1_ELLIPSE();
				//signal_point.point = D2D1::Point2F(float (50 + ((1000 - cur_xtime + i)%1000)), rtHeight - 350 - this->signal1[i]);
				//signal_point.radiusX = 1;
				//signal_point.radiusY = 1;
				//this->m_pRenderTarget-> FillEllipse(&signal_point, this->m_pSignal1Brush);
				if ((i+1)!=cur_xtime)
				this->m_pRenderTarget->DrawLine(D2D1::Point2F(float (50 + ((1000 - cur_xtime + i)%1000)), rtHeight - 350 - this->signal1[i]),
												D2D1::Point2F(float (50 + ((1001 - cur_xtime + i)%1000)), rtHeight - 350 - this->signal1[i+1]),
												this->m_pSignal1Brush,1.0,0);

			}

		if (this->s2_enable)
			for (int i = 0; i < 999; i++)
			{
				//D2D1_ELLIPSE signal_point = D2D1_ELLIPSE();
				//signal_point.point = D2D1::Point2F(float (50 + ((1000 - cur_xtime + i)%1000)), rtHeight - 350 - this->signal2[i]);
				//signal_point.radiusX = 1;
				//signal_point.radiusY = 1;
				//this->m_pRenderTarget-> FillEllipse(&signal_point, this->m_pSignal2Brush);
				if ((i+1)!=cur_xtime)
				this->m_pRenderTarget->DrawLine(D2D1::Point2F(float (50 + ((1000 - cur_xtime + i)%1000)), rtHeight - 350 - this->signal2[i]),
												D2D1::Point2F(float (50 + ((1001 - cur_xtime + i)%1000)), rtHeight - 350 - this->signal2[i+1]),
												this->m_pSignal2Brush,1.0,0);
			}

		if (this->s3_enable)
			for (int i = 0; i < 999; i++)
			{
				//D2D1_ELLIPSE signal_point = D2D1_ELLIPSE();
				//signal_point.point = D2D1::Point2F(float (50 + ((1000 - cur_xtime + i)%1000)), rtHeight - 350 - this->signal2[i]);
				//signal_point.radiusX = 1;
				//signal_point.radiusY = 1;
				//this->m_pRenderTarget-> FillEllipse(&signal_point, this->m_pSignal2Brush);
				if ((i+1)!=cur_xtime)
				this->m_pRenderTarget->DrawLine(D2D1::Point2F(float (50 + ((1000 - cur_xtime + i)%1000)), rtHeight - 350 - this->signal3[i]),
												D2D1::Point2F(float (50 + ((1001 - cur_xtime + i)%1000)), rtHeight - 350 - this->signal3[i+1]),
												this->m_pSignal3Brush,1.0,0);
			}

		if (this->s1_trig_enable)
		{
			this->m_pRenderTarget->DrawLine(D2D1::Point2F(55, rtHeight-350 - this->s1_trig_value),D2D1::Point2F(1045, rtHeight-350 - this->s1_trig_value),this->m_pSignal1Brush , 1.0, m_pStrokeStyle);
		}

		if (this->s2_trig_enable)
		{
			this->m_pRenderTarget->DrawLine(D2D1::Point2F(55, rtHeight-350 - this->s2_trig_value),D2D1::Point2F(1045, rtHeight-350 - this->s2_trig_value),this->m_pSignal2Brush , 1.0, m_pStrokeStyle);
		}


		//this->m_pRenderTarget->DrawTextW("FREEZE",5,

//			FillGeometry(this->m_pAxisCoord, this->m_pAxisCoordBrush);
		//The transform matrix will be apllied to all rendered elements, until it is reset. So we don't need to set the matrix for the ellipse again.
//		this->m_pRenderTarget->FillEllipse(&star, this->m_pStarBrush);

		// By default, or when animating from left to right, draw the planet afte the star so it has a larger z-index.
		//if(this->m_animateToRight)
		//{
		//	this->DrawPlanet(planet);
		//}

		////if(this->m_animate)
		////{
		////	// Perform a hit test. If the user clicked the planet, let's animate it to make it move around the star.
		////	ID2D1EllipseGeometry* hitTestEllipse;
		////	this->m_pDirect2dFactory->CreateEllipseGeometry(&planet, &hitTestEllipse);
		////	D2D1_POINT_2F point;
		////	point.x = this->m_clickedPointX;
		////	point.y = this->m_clickedPointY;
		////	BOOL hit = false;
		////	D2D1::Matrix3x2F matrix = D2D1::Matrix3x2F::Translation(10, rtHeight / 2 - 100);
		////	hitTestEllipse->FillContainsPoint(point, &matrix, &hit);
		////	if(!hit)
		////	{
		////		this->m_animate = false;
		////	}
		////	else
		////	{
		////		// When moving from left to right, translate transform becomes larger and lager.
		////		if(this->m_animateToRight)
		////		{
		////			this->m_animateTranslateX++;
		////			if(this->m_animateTranslateX > rtWidth - 220)
		////			{
		////				this->m_animateToRight = false;
		////			}
		////		}
		////		else
		////		{
		////			// When moving from right to left, translate transform becomes smaller and smaller.
		////			this->m_animateTranslateX--;
		////			if(this->m_animateTranslateX <= 0)
		////			{
		////				this->m_animateToRight = true;
		////				this->m_animateTranslateX = 0;
		////				this->m_animate = false;
		////			}
		////		}
		////		SafeRelease(&hitTestEllipse);
		////		InvalidateRect(this->m_hwnd, NULL, FALSE);
		////	}
		////}

		// Finish drawing.
		hr = this->m_pRenderTarget->EndDraw();
	}
	if (hr == D2DERR_RECREATE_TARGET)
	{
		hr = S_OK;
		DiscardResources();
	}
	return hr;
}

// Draw the planet.
//void Renderer::DrawPlanet(D2D1_ELLIPSE planet)
//{
//	//Get the size of the RenderTarget.
//	float rtWidth = this->m_pRenderTarget->GetSize().width;
//	float rtHeight = this->m_pRenderTarget->GetSize().height;
//
//	this->m_pRenderTarget->SetTransform(D2D1::Matrix3x2F::Translation(10 + this->m_animateTranslateX, rtHeight / 2 - 100));
//	this->m_pRenderTarget->FillEllipse(&planet, this->m_pPlanetBackgroundBrush);
//	this->m_pRenderTarget->SetTransform(D2D1::Matrix3x2F::Translation(23 + this->m_animateTranslateX, rtHeight / 2 - 121));
//	this->m_pRenderTarget->FillGeometry(this->m_pPlanetUpPath, this->m_pContinentBrush);
//	this->m_pRenderTarget->SetTransform(D2D1::Matrix3x2F::Translation(15 + this->m_animateTranslateX, rtHeight / 2 + 1));
//	this->m_pRenderTarget->FillGeometry(this->m_pPlanetDownPath, this->m_pContinentBrush);
//}

void Renderer::DiscardResources()
{
	SafeRelease(&m_pRenderTarget);
	SafeRelease(&m_pSignal2Brush);
	SafeRelease(&m_pSignal1Brush);
	//SafeRelease(&m_pContinentBrush);
	//SafeRelease(&m_pStartOutlineBrush);
	SafeRelease(&m_pAxisCoordBrush);
//	SafeRelease(&m_pAxisCoord);
	//SafeRelease(&m_pPlanetUpPath);
	//SafeRelease(&m_pPlanetDownPath);
	SafeRelease(&m_pDirect2dFactory);
}

// This function creates the path geometry that represents the star's outline.
// The data of the path is created in Expression Blend first,
// and then use a PowerShell script to translate the XAML to C++ code.
//HRESULT Renderer::CreateAxisCoord()
//{
//	HRESULT hr = S_OK;
//
//	ID2D1GeometrySink *pSink = NULL;
//	hr = this->m_pDirect2dFactory->CreatePathGeometry(&this->m_pAxisCoord);
//	if (SUCCEEDED(hr))
//	{
//		hr = this->m_pAxisCoord->Open(&pSink);
//	}
//	if (SUCCEEDED(hr))
//	{
//		pSink->BeginFigure(
//			D2D1::Point2F(100,100),
//			D2D1_FIGURE_BEGIN_FILLED
//			);
//
//		pSink->AddLine(D2D1::Point2F(800,100));
//
//		pSink->AddBezier(
//			D2D1::BezierSegment(
//			D2D1::Point2F(58.5555980936983,5.84704768741611),
//			D2D1::Point2F(57.1665925149922,7.18044670767652),
//			D2D1::Point2F(55.7775869362862,8.51374572801038)
//			));
//
//
//		pSink->EndFigure(D2D1_FIGURE_END_CLOSED);
//		hr = pSink->Close();
//	}
//	return hr;
//}

// This function creates the path geometry that represents the up continent in the planet. It demonstrates how to substitute clipping by intersecting.
//HRESULT Renderer::CreatePlanetUpPath()
//{
//	HRESULT hr = S_OK;
//	ID2D1GeometrySink* pSink = NULL;
//	// Since clip is required, we must draw to a temporary path, and then intersect it with the ellipse, and save the result in m_pPlanetUpPath.
//	ID2D1PathGeometry* tempPath;
//	hr = this->m_pDirect2dFactory->CreatePathGeometry(&this->m_pPlanetUpPath);
//	if (SUCCEEDED(hr))
//	{
//		hr = this->m_pDirect2dFactory->CreatePathGeometry(&tempPath);
//	}
//	if (SUCCEEDED(hr))
//	{
//		hr = tempPath->Open(&pSink);
//	}
//	if (SUCCEEDED(hr))
//	{
//		pSink->BeginFigure(
//			D2D1::Point2F(0.0288963486137668,71.2923486227374),
//			D2D1_FIGURE_BEGIN_FILLED
//			);
//
//		// The following AddBezier code are generated by the PowerShell script.
//		pSink->AddBezier(
//			D2D1::BezierSegment(
//			D2D1::Point2F(-0.581304006712504,71.029349563583),
//			D2D1::Point2F(8.52890129825862,88.2922878076214),
//			D2D1::Point2F(22.0289091594593,91.7922752868622)
//			));
//
//		pSink->AddBezier(
//			D2D1::BezierSegment(
//			D2D1::Point2F(35.5289170206599,95.292262766103),
//			D2D1::Point2F(42.528921096838,64.2923736642557),
//			D2D1::Point2F(42.528921096838,64.2923736642557)
//			));
//
//		pSink->AddBezier(
//			D2D1::BezierSegment(
//			D2D1::Point2F(49.0289248818605,71.2923486227374),
//			D2D1::Point2F(59.5289309961277,76.2923307359385),
//			D2D1::Point2F(61.52893216075,94.7922645547829)
//			));
//
//		pSink->AddBezier(
//			D2D1::BezierSegment(
//			D2D1::Point2F(63.5289333253723,113.292198373627),
//			D2D1::Point2F(96.5289525416406,85.7922967510208),
//			D2D1::Point2F(99.5289542885741,86.2922949623409)
//			));
//
//		pSink->AddBezier(
//			D2D1::BezierSegment(
//			D2D1::Point2F(102.528956035508,86.792293173661),
//			D2D1::Point2F(127.528970593286,111.792203739667),
//			D2D1::Point2F(146.028981366043,106.792221626466)
//			));
//
//		pSink->AddBezier(
//			D2D1::BezierSegment(
//			D2D1::Point2F(164.528992138799,101.792239513265),
//			D2D1::Point2F(178.029,80.2923164264995),
//			D2D1::Point2F(178.029,80.2923164264995)
//			));
//
//		pSink->AddBezier(
//			D2D1::BezierSegment(
//			D2D1::Point2F(178.029,80.2923164264995),
//			D2D1::Point2F(105.028957491285,-94.2070593242214),
//			D2D1::Point2F(0.0288963486137668,71.2923486227374)
//			));
//
//
//
//		pSink->EndFigure(D2D1_FIGURE_END_CLOSED);
//		hr = pSink->Close();
//		SafeRelease(&pSink);
//	}
//	ID2D1GeometrySink* pSink2;
//	if (SUCCEEDED(hr))
//	{
//		hr = this->m_pPlanetUpPath->Open(&pSink2);
//	}
//	D2D1_ELLIPSE clip;
//	ID2D1EllipseGeometry* clipEllipse;
//	if (SUCCEEDED(hr))
//	{
//		// Create a clip ellipse.
//		clip = D2D1::Ellipse(
//			D2D1::Point2F(87.0f, 121.0f),
//			100.0f,
//			100.0f
//			);
//		hr = this->m_pDirect2dFactory->CreateEllipseGeometry(&clip, &clipEllipse);
//	}
//	if (SUCCEEDED(hr))
//	{
//		// There's no direct support for clipping path in Direct2D. So we can intersect a path with its clip instead.
//		hr = tempPath->CombineWithGeometry(clipEllipse, D2D1_COMBINE_MODE_INTERSECT, NULL, 0, pSink2);
//	}
//	if (SUCCEEDED(hr))
//	{
//		hr = pSink2->Close();
//		SafeRelease(&pSink2);
//		SafeRelease(&tempPath);
//		SafeRelease(&clipEllipse);
//	}
//	return hr;
//}

// This function creates the path geometry that represents the down continent in the planet. It demonstrates how to substitute clipping by intersecting.
//HRESULT Renderer::CreatePlanetDownPath()
//{
//	HRESULT hr = S_OK;
//	ID2D1GeometrySink* pSink = NULL;
//	//Since clip is required, we must draw to a temporary path, and then intersect it with the ellipse, and save the result in m_pPlanetDownPath.
//	ID2D1PathGeometry* tempPath;
//	hr = this->m_pDirect2dFactory->CreatePathGeometry(&this->m_pPlanetDownPath);
//	if (SUCCEEDED(hr))
//	{
//		hr = this->m_pDirect2dFactory->CreatePathGeometry(&tempPath);
//	}
//	if (SUCCEEDED(hr))
//	{
//		hr = tempPath->Open(&pSink);
//	}
//	if (SUCCEEDED(hr))
//	{
//		pSink->BeginFigure(
//			D2D1::Point2F(0.0288963486137668,71.2923486227374),
//			D2D1_FIGURE_BEGIN_FILLED
//			);
//
//		// The following AddBezier code are generated by the PowerShell script.
//		pSink->AddBezier(
//			D2D1::BezierSegment(
//			D2D1::Point2F(0.715499175522696,-0.486801532710843),
//			D2D1::Point2F(26.1927417195169,14.0124923433061),
//			D2D1::Point2F(48.6926998092458,17.5124667261123)
//			));
//
//		pSink->AddBezier(
//			D2D1::BezierSegment(
//			D2D1::Point2F(71.1926578989747,21.0124411089184),
//			D2D1::Point2F(94.1926150573642,39.512305703751),
//			D2D1::Point2F(93.192616920043,53.512703231316)
//			));
//
//		pSink->AddBezier(
//			D2D1::BezierSegment(
//			D2D1::Point2F(92.1926187827217,67.5126007625406),
//			D2D1::Point2F(123.192561039681,82.5124909745669),
//			D2D1::Point2F(123.192561039681,82.5124909745669)
//			));
//
//		pSink->AddBezier(
//			D2D1::BezierSegment(
//			D2D1::Point2F(123.192561039681,82.5124909745669),
//			D2D1::Point2F(134.192540550216,62.0126410181309),
//			D2D1::Point2F(121.6925638337,45.0122654481606)
//			));
//
//		pSink->AddBezier(
//			D2D1::BezierSegment(
//			D2D1::Point2F(109.192587117184,28.0123898745307),
//			D2D1::Point2F(99.6926048126313,2.01258017368507),
//			D2D1::Point2F(122.692561971021,21.5124374493193)
//			));
//
//		pSink->AddBezier(
//			D2D1::BezierSegment(
//			D2D1::Point2F(145.69251912941,41.0122947249536),
//			D2D1::Point2F(151.692507953338,62.5126373585318),
//			D2D1::Point2F(149.192512610035,67.0126044221397)
//			));
//
//		pSink->AddBezier(
//			D2D1::BezierSegment(
//			D2D1::Point2F(146.692517266732,71.5125714857476),
//			D2D1::Point2F(159.192493983248,73.5125568473511),
//			D2D1::Point2F(147.192516335392,84.5124763361704)
//			));
//
//		pSink->AddBezier(
//			D2D1::BezierSegment(
//			D2D1::Point2F(135.192538687537,95.5123958249898),
//			D2D1::Point2F(108.692588048523,102.012348250201),
//			D2D1::Point2F(108.692588048523,102.012348250201)
//			));
//
//		pSink->AddBezier(
//			D2D1::BezierSegment(
//			D2D1::Point2F(108.692588048523,102.012348250201),
//			D2D1::Point2F(-5.30719960610358,132.012128674254),
//			D2D1::Point2F(0.19280014914486,0.0125948120815593)
//			));
//
//		pSink->EndFigure(D2D1_FIGURE_END_CLOSED);
//		hr = pSink->Close();
//		SafeRelease(&pSink);
//	}
//	ID2D1GeometrySink* pSink2;
//	if (SUCCEEDED(hr))
//	{
//		hr = this->m_pPlanetDownPath->Open(&pSink2);
//	}
//	D2D1_ELLIPSE clip;
//	ID2D1EllipseGeometry* clipEllipse;
//	if (SUCCEEDED(hr))
//	{
//		//Create a clip ellipse.
//		clip = D2D1::Ellipse(
//			D2D1::Point2F(95.0f, 1.0f),
//			100.0f,
//			100.0f
//			);
//		hr = this->m_pDirect2dFactory->CreateEllipseGeometry(&clip, &clipEllipse);
//	}
//	if (SUCCEEDED(hr))
//	{
//		//There's no direct support for clipping path in Direct2D. So we can intersect a path with its clip instead.
//		hr = tempPath->CombineWithGeometry(clipEllipse, D2D1_COMBINE_MODE_INTERSECT, NULL, 0, pSink2);
//	}
//	if (SUCCEEDED(hr))
//	{
//		hr = pSink2->Close();
//		SafeRelease(&pSink2);
//		SafeRelease(&tempPath);
//		SafeRelease(&clipEllipse);
//	}
//	return hr;
//}
