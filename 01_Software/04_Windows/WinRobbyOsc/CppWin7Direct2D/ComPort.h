#pragma once

#include "resource.h"
#include "renderer.h"


typedef struct signal_values {
	signed short S1;
	signed short S2;
} signal_values_t;

int SetupUart(char* in_port);
int WriteUart(unsigned char *buf, int len);

int ReadUart(char *buf, int mlen, HANDLE hPort);

int CloseUart();