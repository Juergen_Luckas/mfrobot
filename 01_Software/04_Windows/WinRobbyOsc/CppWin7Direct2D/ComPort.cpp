
#include "stdafx.h"
#include "ComPort.h"
#include "Renderer.h"
#include <commctrl.h>
#include <process.h>
#include <cstring>

uintptr_t      readfct;
HANDLE		   hThread;

HANDLE         LPort;

DCB            PortDCB; 
COMMTIMEOUTS   CommTimeouts; 
HANDLE         hPort1,hPort2,hPort;

char           lastError[1024],buf1[100],buf2[100];

char	       trace[1000][20];
unsigned int   ptrc;

extern Renderer* pRenderer;

unsigned int __stdcall myreadthread(void*) 
{
	int len;
	int glen = 0;
	int dpos = 0;
	unsigned char htext[20];
	char dtext[20];
	char rtext[20];
	unsigned short sigval1,sigval2,sigval3;
	int i;
	float SV1, SV2, SV3;

	while (TRUE) {

		len = ReadUart(rtext, 13, hPort1);

		rtext[len]=0;

		if (len > 0)
		{
			for (i=0; i<len; i++)
			{
				// is there new start flag				
				if (*((unsigned char*)(&rtext[i])) == 0xFF)
				{
					glen = 1;
				}

				if (glen>0)
				{
					// collect signal data
					htext[glen-1] = rtext[i];
					
					glen++;
					if (glen>13)
					{
						sigval1 = (((((int)htext[1])&0xF)<<12) + (((int)(htext[0x2])&0xF)<<8) + ((((int)(htext[0x3])&0xF))<<4) + ((int)htext[0x4]&0xF)) ;
						sigval2 = (((((int)htext[5])&0xF)<<12) + (((int)(htext[0x6])&0xF)<<8) + ((((int)(htext[0x7])&0xF))<<4) + ((int)htext[0x8]&0xF)) ;
						sigval3 = (((((int)htext[9])&0xF)<<12) + (((int)(htext[0xA])&0xF)<<8) + ((((int)(htext[0xB])&0xF))<<4) + ((int)htext[0xC]&0xF)) ;

						SV1 = ((float)(*(short*) (&sigval1))) * pRenderer->s1_ampl + pRenderer->s1_offs;
						pRenderer->signal1[pRenderer->cur_xtime] = SV1;

						SV2 = ((float)(*(short*) (&sigval2))) * pRenderer->s2_ampl + pRenderer->s2_offs;
						pRenderer->signal2[pRenderer->cur_xtime] = SV2;

						SV3 = ((float)(*(short*) (&sigval3))) * pRenderer->s3_ampl + pRenderer->s3_offs;
						pRenderer->signal3[pRenderer->cur_xtime] = SV3;

						glen = 0;

						if (pRenderer->s1_trig_enable) 
						{
							if ((pRenderer->s1_trig_lower) && (SV1<pRenderer->s1_trig_value))
							{
								pRenderer->trig_flag = true;
							}
							if ((!(pRenderer->s1_trig_lower)) && (SV1 > pRenderer->s1_trig_value))
							{
								pRenderer->trig_flag = true;
							}
						}

						if (pRenderer->s2_trig_enable) 
						{
							if ((pRenderer->s2_trig_lower) && (SV2<pRenderer->s2_trig_value))
							{
								pRenderer->trig_flag = true;
							}
							if ((!(pRenderer->s2_trig_lower)) && (SV2 > pRenderer->s2_trig_value))
							{
								pRenderer->trig_flag = true;
							}
						}

						if (pRenderer->trig_flag)
						{
							if (pRenderer->trig_cnt == 0) 
							{
								pRenderer->freeze = true;
								pRenderer->trig_flag = false;
	  							InvalidateRect(pRenderer->m_hwnd, NULL, FALSE);
							}
							else 
								(pRenderer->trig_cnt)--;
						}

						if (pRenderer->freeze == false)
							if (pRenderer->cur_xtime++ >= 999)
								pRenderer->cur_xtime = 0;

						if (!(pRenderer->cur_xtime % 8))
  							InvalidateRect(pRenderer->m_hwnd, NULL, FALSE);

					}

				}
				else
				{
					// debug data
					dtext[dpos] = rtext[i];
					dpos++;
				}
			} // end for loop

			// check, if there is something for the print port
			if ((dpos>0) && (pRenderer->debug_en)) 
			{
    			HWND hwndOutput = GetDlgItem( pRenderer->hDlg, IDC_EDIT_DEBUG );
						
				wchar_t* bufrt = new wchar_t[len+1];

				dtext[dpos] = 0;
				MultiByteToWideChar(CP_ACP, 0, dtext, dpos+1, bufrt, dpos+1);

				int outLength = GetWindowTextLength( hwndOutput ) + dpos + 2;
				TCHAR * buf = ( TCHAR * ) GlobalAlloc( GPTR, outLength * sizeof(TCHAR) );
				GetWindowText( hwndOutput, buf, outLength );
				_tcscat_s( buf, outLength, bufrt);
				SetWindowText( hwndOutput, buf );
				GlobalFree( buf );
				free(bufrt); 
				SendMessage(hwndOutput, WM_VSCROLL, SB_BOTTOM, 0L);
			}
			dpos = 0;
			rtext[0]=0;

		} // end if len
	} // end forever loop

//		//old
//
//		rtext[len]=0;
//
//		if ((len>0) && (pRenderer->debug_en)) 
//		{
//    		HWND hwndOutput = GetDlgItem( pRenderer->hDlg, IDC_EDIT_DEBUG );
//						
//			wchar_t* bufrt = new wchar_t[len+1];
//			MultiByteToWideChar(CP_ACP, 0, rtext, len+1, bufrt, len+1);
//
//			int outLength = GetWindowTextLength( hwndOutput ) + len + 2;
//			TCHAR * buf = ( TCHAR * ) GlobalAlloc( GPTR, outLength * sizeof(TCHAR) );
//			GetWindowText( hwndOutput, buf, outLength );
//			_tcscat_s( buf, outLength, bufrt);
//			SetWindowText( hwndOutput, buf );
//			GlobalFree( buf );
//			free(bufrt); 
//
////			SetDlgItemTextA(pRenderer->hDlg,IDC_EDIT_DEBUG,(LPCSTR)rtext);
//		}
//
//		if ((len>0) && (glen<100)) 
//		{
//			if (rtext[0] = 0xFF)
//			{
//				for (i=0; i<len; i++)
//				{
//					htext[i] = rtext[i];
//				}
//			    glen = len;
//			}
//			else if (glen > 0)
//			{
//				for (i=0; i<len; i++)
//				{
//					htext[glen + i] = rtext[i];
//				}
//				glen = glen + len;
//			}
//			if (glen>=13)
//			{
//
//				ptrc++;
//				if (ptrc == 1000) 
//				{
//					ptrc=0;
//				}
//
//				for (i=0; i<glen; i++)
//				{
//					trace[ptrc][i] = htext[i];
//				}
//				trace[ptrc][i++] = 0xAF;
//				trace[ptrc][i] = 0xFE;
//
////			    sigval = (((((int)htext[1])&0xF)<<12) + (((int)(htext[2])&0xF)<<8) + ((((int)(htext[3])&0xF))<<4) + ((int)htext[4]&0xF)) ;
////				sigval = sigval / 10;
//			    sigval1 = (((((int)htext[1])&0xF)<<12) + (((int)(htext[0x2])&0xF)<<8) + ((((int)(htext[0x3])&0xF))<<4) + ((int)htext[0x4]&0xF)) ;
//			    sigval2 = (((((int)htext[5])&0xF)<<12) + (((int)(htext[0x6])&0xF)<<8) + ((((int)(htext[0x7])&0xF))<<4) + ((int)htext[0x8]&0xF)) ;
//			    sigval3 = (((((int)htext[9])&0xF)<<12) + (((int)(htext[0xA])&0xF)<<8) + ((((int)(htext[0xB])&0xF))<<4) + ((int)htext[0xC]&0xF)) ;
//
//				SV1 = ((float)(*(short*) (&sigval1))) * pRenderer->s1_ampl + pRenderer->s1_offs;
//				pRenderer->signal1[pRenderer->cur_xtime] = SV1;
//
//				SV2 = ((float)(*(short*) (&sigval2))) * pRenderer->s2_ampl + pRenderer->s2_offs;
//				pRenderer->signal2[pRenderer->cur_xtime] = SV2;
//
//				SV3 = ((float)(*(short*) (&sigval3))) * pRenderer->s3_ampl + pRenderer->s3_offs;
//				pRenderer->signal3[pRenderer->cur_xtime] = SV3;
//
//
////				SendMessage(myhDlg, WM_COMMAND, 0x040103f2, sigval);
////				PostMessage(myhDlg, WM_COMMAND, 0x040103f2, *((LPARAM*)(&signal_array)));
//				if (pRenderer->s1_trig_enable) 
//				{
//					if ((pRenderer->s1_trig_lower) && (SV1<pRenderer->s1_trig_value))
//					{
//						pRenderer->trig_flag = true;
//					}
//					if ((!(pRenderer->s1_trig_lower)) && (SV1 > pRenderer->s1_trig_value))
//					{
//						pRenderer->trig_flag = true;
//					}
//				}
//
//				if (pRenderer->s2_trig_enable) 
//				{
//					if ((pRenderer->s2_trig_lower) && (SV2<pRenderer->s2_trig_value))
//					{
//						pRenderer->trig_flag = true;
//					}
//					if ((!(pRenderer->s2_trig_lower)) && (SV2 > pRenderer->s2_trig_value))
//					{
//						pRenderer->trig_flag = true;
//					}
//				}
//
//				if (pRenderer->trig_flag)
//				{
//					if (pRenderer->trig_cnt == 0) 
//					{
//						pRenderer->freeze = true;
//						pRenderer->trig_flag = false;
//	  					InvalidateRect(pRenderer->m_hwnd, NULL, FALSE);
//					}
//					else 
//						(pRenderer->trig_cnt)--;
//				}
//
//				if (pRenderer->freeze == false)
//					if (pRenderer->cur_xtime++ >= 999)
//						pRenderer->cur_xtime = 0;
//
//				if (!(pRenderer->cur_xtime % 8))
//  					InvalidateRect(pRenderer->m_hwnd, NULL, FALSE);
//
//
//				//calc values
//				glen = 0;
//
//			}
//			rtext[0]=0;
//		}
//		else
//		{
//		  //SetDlgItemTextA(myhDlg,IDC_EDIT2,(LPCSTR)htext);
//          if (glen>0)
//		  {
//			 glen = 0;
//       		 rtext[0]=0;
//		  }
//		}
//    }
	return 0;
}

int configuretimeout()
{
	//memset(&CommTimeouts, 0x00, sizeof(CommTimeouts)); 
	CommTimeouts.ReadIntervalTimeout = 5; 
	CommTimeouts.ReadTotalTimeoutConstant = 5; 
	CommTimeouts.ReadTotalTimeoutMultiplier= 2;
	CommTimeouts.WriteTotalTimeoutMultiplier=10;
	CommTimeouts.WriteTotalTimeoutConstant = 50; 
   return 1;
}

int SetupUart(char* in_port)
{
	int STOPBITS;
	char  com_port_name[30];
	ptrc = 0;
   
	wchar_t com_port_name_w[30];

//	hPort1 = CreateFile (TEXT("\\\\.\\COM10"),			            // Name of the port

	strcpy(com_port_name, "\\\\.\\");
	strcat(com_port_name,in_port);

	com_port_name_w [0] = com_port_name[0];
	com_port_name_w [1] = com_port_name[1];
	com_port_name_w [2] = com_port_name[2];
	com_port_name_w [3] = com_port_name[3];
	com_port_name_w [4] = com_port_name[4];
	com_port_name_w [5] = com_port_name[5];
	com_port_name_w [6] = com_port_name[6];
	com_port_name_w [7] = com_port_name[7];
	com_port_name_w [8] = com_port_name[8];
	com_port_name_w [9] = com_port_name[9];
	com_port_name_w [10] = com_port_name[10];
	com_port_name_w [11] = com_port_name[11];

//	hPort1 = CreateFile (TEXT("\\\\.\\COM10"),			            // Name of the port
	hPort1 = CreateFile (com_port_name_w,			            // Name of the port
//	hPort1 = CreateFile ((LPCTSTR)(com_port_name),			            // Name of the port 
						GENERIC_READ | GENERIC_WRITE,     // Access (read-write) mode 
						0,                                  
						NULL,                             
						OPEN_EXISTING,
						FILE_ATTRIBUTE_NORMAL,                     
			            NULL);                             
             
	
	if ( hPort1 == INVALID_HANDLE_VALUE )
	{ 
		
		MessageBox (NULL, L"Port Open Failed" ,L"Error", MB_OK);
		return 0;
	}   

	  //Initialize the DCBlength member. 
      PortDCB.DCBlength = sizeof (DCB); 
      
      // Get the default port setting information.
      GetCommState (hPort1, &PortDCB);

//		PortDCB.BaudRate= 115200;            
//		PortDCB.BaudRate = 19200;            
//		PortDCB.BaudRate= 38400;            
        PortDCB.BaudRate = 57600;            
//	    PortDCB.BaudRate = 9600;            
		PortDCB.ByteSize=8;            
//		PortDCB.Parity= EVENPARITY;                
//	     PortDCB.Parity = MARKPARITY;               
	    PortDCB.Parity = NOPARITY;  
		PortDCB.StopBits=0;
//		 PortDCB.Parity = ODDPARITY;           
//		 PortDCB.Parity = SPACEPARITY;     

//		PortDCB.fOutxCtsFlow = TRUE;                        // CTS output flow control 
//        PortDCB.fDtrControl = DTR_CONTROL_ENABLE;           // DTR flow control type 
//        PortDCB.fOutX = FALSE;                              // No XON/XOFF out flow control 
//        PortDCB.fInX = FALSE;                               // No XON/XOFF in flow control 
//        PortDCB.fRtsControl = RTS_CONTROL_ENABLE;           // RTS flow control 

//		PortDCB.fOutxCtsFlow = FALSE;                      // No CTS output flow control 
//        PortDCB.fDtrControl = DTR_CONTROL_ENABLE;          // DTR flow control type 
//        PortDCB.fOutX = FALSE;                             // No XON/XOFF out flow control 
//        PortDCB.fInX = FALSE;                              // No XON/XOFF in flow control 
//        PortDCB.fRtsControl = RTS_CONTROL_ENABLE;          // RTS flow control 

		PortDCB.fOutxCtsFlow = TRUE;                      // No CTS output flow control 
        PortDCB.fDtrControl = DTR_CONTROL_ENABLE;          // DTR flow control type 
        PortDCB.fOutX = FALSE;                              // Enable XON/XOFF out flow control 
        PortDCB.fInX = FALSE;                               // Enable XON/XOFF in flow control 
        PortDCB.fRtsControl = RTS_CONTROL_ENABLE;          // RTS flow control 

//	  configure();

	  // Retrieve the time-out parameters for all read and write operations  
	  GetCommTimeouts (hPort1, &CommTimeouts); 
	  configuretimeout();
   

	//Re-configure the port with the new DCB structure. 
	if (!SetCommState (hPort1, &PortDCB)) 
	{ 
        MessageBox (NULL, L"1.Could not create the read thread.(SetCommState Failed)" ,L"Error", MB_OK);
		CloseHandle(hPort1);   
		return 0; 
	 } 

	// Set the time-out parameters for all read and write operations on the port. 
	if (!SetCommTimeouts (hPort1, &CommTimeouts)) 
	{ 
        MessageBox (NULL, L"Could not create the read thread.(SetCommTimeouts Failed)" ,L"Error", MB_OK);
		CloseHandle(hPort1);  
		return 0; 
	} 

	// Clear the port of any existing data. 
	if(PurgeComm(hPort1, PURGE_TXCLEAR | PURGE_RXCLEAR)==0) 
	{   MessageBox (NULL, L"Clearing The Port Failed" ,L"Message", MB_OK);
		CloseHandle(hPort1); 
		return 0; 
	} 
    
//	    MessageBox (NULL, L"COM10 SERIAL SETUP OK." ,L"Message", MB_OK);
	    
//     	readfct = _beginthreadex(0, 0, &myreadthread, (void*)0, 0, 0);
		
		hThread = (HANDLE)_beginthreadex(0, 0, &myreadthread, (void*)0, 0, 0);

		return 1;
}


int ReadUart(char *buf, int mlen, HANDLE hPort)
{
	BOOL ret;
	DWORD dwRead;
    BOOL fWaitingOnRead = FALSE;
    OVERLAPPED osReader = {0};
    unsigned long retlen=0;

   // Create the overlapped event. Must be closed before exiting to avoid a handle leak.

   osReader.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
   if (osReader.hEvent == NULL)
       MessageBox (NULL, L"Error in creating Overlapped event" ,L"Error", MB_OK);
   if (!fWaitingOnRead)
   {
          if (!ReadFile(hPort, buf, mlen, &dwRead,  &osReader)) 

          {
			
           _endthreadex(0);
			  
		   FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
                        NULL,
                        GetLastError(),
                        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                        (LPWSTR)lastError,
                        1024,
                        NULL);
//		   MessageBox (NULL, (LPWSTR)lastError ,L"MESSAGE", MB_OK);
           MessageBox (NULL, L"thread termiinated" ,L"MESSAGE", MB_OK);

           }
           else
		   {
             
	         // MessageBox (NULL, L"ReadFile Suceess" ,L"Success", MB_OK);
           }
        
    }
	
	if(dwRead > 0)	
	{
		//MessageBox (NULL, L"Read DATA Success" ,L"Success", MB_OK);//If we have data
		return (int) dwRead;
	}
	     //return the length
    
	else return 0;     //else no data has been read
 }

int CloseUart()
{
//	_endthreadex(readfct);
	CloseHandle(hPort1); 

	CloseHandle( hThread );
	return 1;
}


int WriteUart(unsigned char *buf1, int len)
{
	DWORD dwNumBytesWritten;

	WriteFile (hPort1,buf1, len, &dwNumBytesWritten, NULL);			

	if(dwNumBytesWritten > 0)
	{
		//MessageBox (NULL, L"Transmission Success" ,L"Success", MB_OK);
		return 1;
	}
	
	else 
	   {
		MessageBox (NULL, L"Transmission Failed" ,L"Error", MB_OK);
		return 0;	
	   }
}
