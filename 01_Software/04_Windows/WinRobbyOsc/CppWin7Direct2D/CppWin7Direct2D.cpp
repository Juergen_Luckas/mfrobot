/****************************** Module Header ******************************\
 Module Name:  CppWin7Direct2D.cpp
 Project:      CppWin7Direct2D
 Copyright (c) Microsoft Corporation.

 This example demonstrates how to work with Direct2D using C#.

 This source is subject to the Microsoft Public License.
 See http://www.microsoft.com/en-us/openness/licenses.aspx#MPL
 All other rights reserved.

 THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
 EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
 WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

#include "stdafx.h"
#include "CppWin7Direct2D.h"
#include "Renderer.h"
#include "ComPort.h"
#include <process.h>
#include <cstring>
#include <stdio.h>

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	SetParemCallB(HWND, UINT, WPARAM, LPARAM);

//The Renderer object.
Renderer* pRenderer;

int APIENTRY _tWinMain(HINSTANCE hInstance,
					   HINSTANCE hPrevInstance,
					   LPTSTR    lpCmdLine,
					   int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_CPPWIN7DIRECT2D, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_CPPWIN7DIRECT2D));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage are only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_CPPWIN7DIRECT2D));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_COMPORT);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HWND hWnd;

	hInst = hInstance; // Store instance handle in our global variable

	hWnd = CreateWindow(szWindowClass, szTitle,
		WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

	if (!hWnd)
	{
		return FALSE;
	}

	//Create the Renderer object.
	pRenderer = new Renderer(hWnd, 800, 600);

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	wchar_t TextFreeze[6];
	wchar_t TextTrigger[7];

	float SV1,SV2,SV3;

	switch (message)
	{
 	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		switch (wmId)
		{
		case IDM_FILE_OPENPORT:

//			if (pRenderer->com_connect)
//			{
//				CloseUart();
//				pRenderer->com_connect = false;
//			}

//			pRenderer->com_connect = SetupUart(hWnd);

	    break;

		case IDM_SET_PARAM:
			  DialogBox(hInst, MAKEINTRESOURCE(IDD_SETPARAMBOX), hWnd, SetParemCallB);
	    break;

		case IDM_ABOUT:
//			  DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
		break;
		
		case IDM_EXIT:
			  DestroyWindow(hWnd);
		break;

		case 0x03f2:
//			pRenderer->signal1[pRenderer->cur_xtime]=((*((signal_values_t*)lParam)).S3);
//			pRenderer->signal1[pRenderer->cur_xtime]=(int)lParam;
			//SV1 = (*((signal_values_t*)&lParam)).S1 * pRenderer->s1_ampl + pRenderer->s1_offs;
			//pRenderer->signal1[pRenderer->cur_xtime] = SV1;

			//SV2 = (*((signal_values_t*)&lParam)).S2 * pRenderer->s2_ampl + pRenderer->s2_offs;
			//pRenderer->signal2[pRenderer->cur_xtime] = SV2;

//			SV3 = (*((signal_values_t*)&lParam)).S2 * pRenderer->s2_ampl + pRenderer->s2_offs;
//			pRenderer->signal2[pRenderer->cur_xtime] = SV2;

			//if (pRenderer->s1_trig_enable) 
			//{
			//	if ((pRenderer->s1_trig_lower) && (SV1<pRenderer->s1_trig_value))
			//	{
			//		pRenderer->trig_flag = true;
			//	}
			//	if ((!(pRenderer->s1_trig_lower)) && (SV1 > pRenderer->s1_trig_value))
			//	{
			//		pRenderer->trig_flag = true;
			//	}
			//}

			//if (pRenderer->s2_trig_enable) 
			//{
			//	if ((pRenderer->s2_trig_lower) && (SV2<pRenderer->s2_trig_value))
			//	{
			//		pRenderer->trig_flag = true;
			//	}
			//	if ((!(pRenderer->s2_trig_lower)) && (SV2 > pRenderer->s2_trig_value))
			//	{
			//		pRenderer->trig_flag = true;
			//	}
			//}

			//if (pRenderer->trig_flag)
			//{
			//	if (pRenderer->trig_cnt == 0) 
			//	{
			//		pRenderer->freeze = true;
			//		pRenderer->trig_flag = false;
	  //				InvalidateRect(pRenderer->m_hwnd, NULL, FALSE);
			//	}
			//	else 
			//		(pRenderer->trig_cnt)--;
			//}

			//if (pRenderer->freeze == false)
			//	if (pRenderer->cur_xtime++ >= 999)
			//		pRenderer->cur_xtime = 0;

			//if (!(pRenderer->cur_xtime % 8))
  	//			InvalidateRect(pRenderer->m_hwnd, NULL, FALSE);
		break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	break;

	case WM_PAINT:
	case WM_DISPLAYCHANGE:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
		pRenderer->Render();

		TextFreeze[0] = 'F';
		TextFreeze[1] = 'R';
		TextFreeze[2] = 'E';
		TextFreeze[3] = 'E';
		TextFreeze[4] = 'Z';
		TextFreeze[5] = 'E';

		TextTrigger[0] = 'T';
		TextTrigger[1] = 'R';
		TextTrigger[2] = 'I';
		TextTrigger[3] = 'G';
		TextTrigger[4] = 'G';
		TextTrigger[5] = 'E';
		TextTrigger[6] = 'R';

		if (pRenderer->freeze)
		{
			TextOut(hdc,1000,70,(LPCWSTR)TextFreeze ,6);
		}
		else
		{
			if (pRenderer->trig_flag) 
			TextOut(hdc,1000,70,(LPCWSTR)TextTrigger ,7);
		}

		EndPaint(hWnd, &ps);
		break;	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	case WM_LBUTTONDOWN:
		if(!pRenderer->m_animate)
		{
			pRenderer->m_animate = true;
			pRenderer->m_clickedPointX = LOWORD(lParam);
			pRenderer->m_clickedPointY = HIWORD(lParam);
			InvalidateRect(pRenderer->m_hwnd, NULL, FALSE);
		}
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

INT_PTR CALLBACK SetParemCallB(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	char buf[15];
	unsigned char buf_tx[15];

	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:

		sprintf(buf,"%f",pRenderer->s1_ampl);
		SetDlgItemTextA(hDlg,IDC_EDIT_S1_AMP,(LPCSTR)buf);

		sprintf(buf,"%f",pRenderer->s1_offs);
		SetDlgItemTextA(hDlg,IDC_EDIT_S1_OFFS,(LPCSTR)buf);

		CheckDlgButton(hDlg,IDC_CHECKBOX_S1,pRenderer->s1_enable);
  
		sprintf(buf,"%f",pRenderer->s2_ampl);
		SetDlgItemTextA(hDlg,IDC_EDIT_S2_AMP,(LPCSTR)buf);

		sprintf(buf,"%f",pRenderer->s2_offs);
		SetDlgItemTextA(hDlg,IDC_EDIT_S2_OFFS,(LPCSTR)buf);

		CheckDlgButton(hDlg,IDC_CHECKBOX_S2,pRenderer->s2_enable);

		sprintf(buf,"%f",pRenderer->s3_ampl);
		SetDlgItemTextA(hDlg,IDC_EDIT_S3_AMP,(LPCSTR)buf);

		sprintf(buf,"%f",pRenderer->s3_offs);
		SetDlgItemTextA(hDlg,IDC_EDIT_S3_OFFS,(LPCSTR)buf);

		CheckDlgButton(hDlg,IDC_CHECKBOX_S3,pRenderer->s3_enable);


		// Trigger S1
		CheckDlgButton(hDlg,IDC_CHECKBOX_S1_TRIG,pRenderer->s1_trig_enable);

		sprintf(buf,"%f",pRenderer->s1_trig_value);
		SetDlgItemTextA(hDlg,IDC_EDIT_S1_TRIG_VALUE,(LPCSTR)buf);

		CheckDlgButton(hDlg,IDC_CHECKBOX_S1_TRIGTYPE,pRenderer->s1_trig_lower);

		// Trigger S2
		CheckDlgButton(hDlg,IDC_CHECKBOX_S2_TRIG,pRenderer->s2_trig_enable);

		sprintf(buf,"%f",pRenderer->s2_trig_value);
		SetDlgItemTextA(hDlg,IDC_EDIT_S2_TRIG_VALUE,(LPCSTR)buf);

		CheckDlgButton(hDlg,IDC_CHECKBOX_S2_TRIGTYPE,pRenderer->s2_trig_lower);

		// trigger delay
		sprintf(buf,"%d",pRenderer->trig_delay);
		SetDlgItemTextA(hDlg,IDC_EDIT_TRIG_DELAY,(LPCSTR)buf);

		// 
		if (pRenderer->com_connect)
			sprintf(buf,"yes");
		else 
			sprintf(buf,"no");
		SetDlgItemTextA(hDlg,IDD_CONNECTED,(LPCSTR)buf);

		sprintf(buf,"COM");
		SetDlgItemTextA(hDlg,IDC_EDIT_COMPORT,(LPCSTR)buf);

		pRenderer->hDlg = hDlg;

		return (INT_PTR)TRUE;

	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);

		switch (wmId)
		{
		case IDC_BUTTON1:
			EndDialog(hDlg, LOWORD(wParam));
			break;

		case IDC_BUTTON_HALT:
			pRenderer->freeze = !pRenderer->freeze;

			if (!pRenderer->freeze)
			{
				pRenderer->trig_flag = false;
				pRenderer->trig_cnt = pRenderer->trig_delay;
			}

			InvalidateRect(pRenderer->m_hwnd, NULL, FALSE);
			break;

			// S1-----

		case IDC_CHECKBOX_S1:
			pRenderer->s1_enable = IsDlgButtonChecked(hDlg,IDC_CHECKBOX_S1); 
			InvalidateRect(pRenderer->m_hwnd, NULL, FALSE);
			break;

        case IDC_EDIT_S1_AMP:
			 switch(wmEvent)
			 {
			 case EN_KILLFOCUS:
				 int ret=GetDlgItemTextA(hDlg,IDC_EDIT_S1_AMP,(LPSTR)buf,sizeof(buf));			 
				 pRenderer->s1_ampl = atof(buf);
				 break;
	         }
			 break; 
        case IDC_EDIT_S1_OFFS:
			 switch(wmEvent)
			 {
			 case EN_KILLFOCUS:
				 int ret=GetDlgItemTextA(hDlg,IDC_EDIT_S1_OFFS,(LPSTR)buf,sizeof(buf));			 
				 pRenderer->s1_offs = atof(buf);
				 break;
	         }
			 break; 

		case IDC_CHECKBOX_S1_TRIG:
			pRenderer->s1_trig_enable = !pRenderer->s1_trig_enable;
			InvalidateRect(pRenderer->m_hwnd, NULL, FALSE);
			break;

        case IDC_EDIT_S1_TRIG_VALUE:
			 switch(wmEvent)
			 {
			 case EN_KILLFOCUS:
				 int ret=GetDlgItemTextA(hDlg,IDC_EDIT_S1_TRIG_VALUE,(LPSTR)buf,sizeof(buf));			 
				 pRenderer->s1_trig_value = atof(buf);
	 			 InvalidateRect(pRenderer->m_hwnd, NULL, FALSE);
				 break;
	         }
			 break; 

		case IDC_CHECKBOX_S1_TRIGTYPE:
			pRenderer->s1_trig_lower = !pRenderer->s1_trig_lower;
			break;

			// S2-----	

		case IDC_CHECKBOX_S2:
			pRenderer->s2_enable = IsDlgButtonChecked(hDlg,IDC_CHECKBOX_S2); 
			InvalidateRect(pRenderer->m_hwnd, NULL, FALSE);

			break;

        case IDC_EDIT_S2_AMP:
			 switch(wmEvent)
			 {
			 case EN_KILLFOCUS:
				 int ret=GetDlgItemTextA(hDlg,IDC_EDIT_S2_AMP,(LPSTR)buf,sizeof(buf));			 
				 pRenderer->s2_ampl = atof(buf);
				 break;
	         }
			 break; 
        case IDC_EDIT_S2_OFFS:
			 switch(wmEvent)
			 {
			 case EN_KILLFOCUS:
				 int ret=GetDlgItemTextA(hDlg,IDC_EDIT_S2_OFFS,(LPSTR)buf,sizeof(buf));			 
				 pRenderer->s2_offs = atof(buf);
				 break;
	         }
			 break; 

		case IDC_CHECKBOX_S2_TRIG:
			pRenderer->s2_trig_enable = !pRenderer->s2_trig_enable;
			InvalidateRect(pRenderer->m_hwnd, NULL, FALSE);
			break;

        case IDC_EDIT_S2_TRIG_VALUE:
			 switch(wmEvent)
			 {
			 case EN_KILLFOCUS:
				 int ret=GetDlgItemTextA(hDlg,IDC_EDIT_S2_TRIG_VALUE,(LPSTR)buf,sizeof(buf));			 
				 pRenderer->s2_trig_value = atof(buf);
	 			 InvalidateRect(pRenderer->m_hwnd, NULL, FALSE);
				 break;
	         }
			 break; 

		case IDC_CHECKBOX_S2_TRIGTYPE:
			pRenderer->s2_trig_lower = !pRenderer->s2_trig_lower;
			break;


		case IDC_CHECKBOX_S3:
			pRenderer->s3_enable = IsDlgButtonChecked(hDlg,IDC_CHECKBOX_S3); 
			InvalidateRect(pRenderer->m_hwnd, NULL, FALSE);
			break;

        case IDC_EDIT_S3_AMP:
			 switch(wmEvent)
			 {
			 case EN_KILLFOCUS:
				 int ret=GetDlgItemTextA(hDlg,IDC_EDIT_S3_AMP,(LPSTR)buf,sizeof(buf));			 
				 pRenderer->s3_ampl = atof(buf);
				 break;
	         }
			 break; 
        case IDC_EDIT_S3_OFFS:
			 switch(wmEvent)
			 {
			 case EN_KILLFOCUS:
				 int ret=GetDlgItemTextA(hDlg,IDC_EDIT_S3_OFFS,(LPSTR)buf,sizeof(buf));			 
				 pRenderer->s3_offs = atof(buf);
				 break;
	         }
			 break; 

        case IDC_EDIT_TRIG_DELAY:
			 switch(wmEvent)
			 {
			 case EN_KILLFOCUS:
				 int ret=GetDlgItemTextA(hDlg,IDC_EDIT_TRIG_DELAY,(LPSTR)buf,sizeof(buf));			 
				 pRenderer->trig_delay = atoi(buf);
  				 pRenderer->trig_cnt = pRenderer->trig_delay;
				 break;
	         }
			 break; 


		case IDC_BUTTON_CONNECT:

			if (pRenderer->com_connect) {
				CloseUart();
				sprintf(buf,"no");
				SetDlgItemTextA(hDlg,IDD_CONNECTED,(LPCSTR)buf);
				pRenderer->com_connect = false;
			}
			else
			{
				int ret=GetDlgItemTextA(hDlg,IDC_EDIT_COMPORT,(LPSTR)buf,sizeof(buf));			 

				pRenderer->com_connect = SetupUart(buf);
				if (pRenderer->com_connect)
				{
					sprintf(buf,"yes");
					SetDlgItemTextA(hDlg,IDD_CONNECTED,(LPCSTR)buf);
				}

			}

			break;

		case IDC_BUTTON_SEND_MSG1:

			if (pRenderer->com_connect) {

				int ret=GetDlgItemTextA(hDlg,IDC_EDIT_MSG1_ID,(LPSTR)buf,sizeof(buf));			 

				buf_tx[0]=0x80 + atoi(buf);
//				buf_tx[0]=atoi(buf);

				ret=GetDlgItemTextA(hDlg,IDC_EDIT_MSG1_VAL,(LPSTR)buf,sizeof(buf));	

				float hf = atof(buf);
				unsigned int hword = *((unsigned int*) &hf);

				buf_tx[1] = (hword>>28) & 0x0F;
				buf_tx[2] = (hword>>24) & 0x0F;
				buf_tx[3] = (hword>>20) & 0x0F;
				buf_tx[4] = (hword>>16) & 0x0F;
				buf_tx[5] = (hword>>12) & 0x0F;
				buf_tx[6] = (hword>>8) & 0x0F;
				buf_tx[7] = (hword>>4) & 0x0F;
				buf_tx[8] = hword & 0x0F;
				
				WriteUart(buf_tx, 9);
			}
			else
			{
		        MessageBox (NULL, L"No Connection" ,L"Error", MB_OK);
			}

			break;

		case IDC_BUTTON_SEND_MSG2:

			if (pRenderer->com_connect) {

				int ret=GetDlgItemTextA(hDlg,IDC_EDIT_MSG2_ID,(LPSTR)buf,sizeof(buf));			 

				buf_tx[0]=0x80 + atoi(buf);
//				buf_tx[0]=atoi(buf);

				ret=GetDlgItemTextA(hDlg,IDC_EDIT_MSG2_VAL,(LPSTR)buf,sizeof(buf));	

				float hf = atof(buf);
				unsigned int hword = *((unsigned int*) &hf);

				buf_tx[1] = (hword>>28) & 0x0F;
				buf_tx[2] = (hword>>24) & 0x0F;
				buf_tx[3] = (hword>>20) & 0x0F;
				buf_tx[4] = (hword>>16) & 0x0F;
				buf_tx[5] = (hword>>12) & 0x0F;
				buf_tx[6] = (hword>>8) & 0x0F;
				buf_tx[7] = (hword>>4) & 0x0F;
				buf_tx[8] = hword & 0x0F;
				
				WriteUart(buf_tx, 9);
			}
			else
			{
		        MessageBox (NULL, L"No Connection" ,L"Error", MB_OK);
			}

			break;

		case IDC_BUTTON_SEND_MSG3:

			if (pRenderer->com_connect) {

				int ret=GetDlgItemTextA(hDlg,IDC_EDIT_MSG3_ID,(LPSTR)buf,sizeof(buf));			 

				buf_tx[0]=0x80 + atoi(buf);
//				buf_tx[0]=atoi(buf);

				ret=GetDlgItemTextA(hDlg,IDC_EDIT_MSG3_VAL,(LPSTR)buf,sizeof(buf));	

				float hf = atof(buf);
				unsigned int hword = *((unsigned int*) &hf);

				buf_tx[1] = (hword>>28) & 0x0F;
				buf_tx[2] = (hword>>24) & 0x0F;
				buf_tx[3] = (hword>>20) & 0x0F;
				buf_tx[4] = (hword>>16) & 0x0F;
				buf_tx[5] = (hword>>12) & 0x0F;
				buf_tx[6] = (hword>>8) & 0x0F;
				buf_tx[7] = (hword>>4) & 0x0F;
				buf_tx[8] = hword & 0x0F;
				
				WriteUart(buf_tx, 9);
			}
			else
			{
		        MessageBox (NULL, L"No Connection" ,L"Error", MB_OK);
			}

			break;

		case IDC_EDIT_DEBUG_EN:
			pRenderer->debug_en = IsDlgButtonChecked(hDlg,IDC_EDIT_DEBUG_EN); 
			InvalidateRect(pRenderer->m_hwnd, NULL, FALSE);

			break;

		}

		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
	break;
	}
	return (INT_PTR)FALSE;
}