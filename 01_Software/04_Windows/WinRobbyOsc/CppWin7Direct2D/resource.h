//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by CppWin7Direct2D.rc
//
#define IDC_MYICON                      2
#define IDD_CPPWIN7DIRECT2D_DIALOG      102
#define IDS_APP_TITLE                   103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_CPPWIN7DIRECT2D             107
#define IDI_SMALL                       108
#define IDC_CPPWIN7DIRECT2D             109
#define IDC_COMPORT                     110
#define IDR_MAINFRAME                   128
#define IDD_SETPARAMBOX                 140
#define IDC_BUTTON1                     1012
#define IDC_BUTTON_HALT                 1013
#define IDC_CHECKBOX_S1                 1020
#define IDC_EDIT_S1_AMP                 1021
#define IDC_EDIT_S1_OFFS                1022
#define IDC_CHECKBOX_S1_TRIG            1023
#define IDC_EDIT_S1_TRIG_VALUE          1024
#define IDC_CHECKBOX_S1_TRIGTYPE		1025
#define IDC_CHECKBOX_S2                 1030
#define IDC_EDIT_S2_AMP                 1031
#define IDC_EDIT_S2_OFFS                1032
#define IDC_CHECKBOX_S2_TRIG            1033
#define IDC_EDIT_S2_TRIG_VALUE          1034
#define IDC_CHECKBOX_S2_TRIGTYPE		1035
#define IDC_CHECKBOX_S3                 1040
#define IDC_EDIT_S3_AMP                 1041
#define IDC_EDIT_S3_OFFS                1042
#define IDC_EDIT_TRIG_DELAY				1050
#define IDC_BUTTON_CONNECT				1051
#define IDC_EDIT_COMPORT				1053
#define IDD_CONNECTED					1054
#define IDC_BUTTON_SEND_MSG1			1060
#define IDC_EDIT_MSG1_ID				1061
#define IDC_EDIT_MSG1_VAL				1062
#define IDC_BUTTON_SEND_MSG2			1063
#define IDC_EDIT_MSG2_ID				1064
#define IDC_EDIT_MSG2_VAL				1065
#define IDC_BUTTON_SEND_MSG3			1066
#define IDC_EDIT_MSG3_ID				1067
#define IDC_EDIT_MSG3_VAL				1068
#define IDC_EDIT_DEBUG					1070
#define IDC_EDIT_DEBUG_EN				1071

#define IDM_FILE_OPENPORT               32771
#define IDM_SET_PARAM                   32772
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
