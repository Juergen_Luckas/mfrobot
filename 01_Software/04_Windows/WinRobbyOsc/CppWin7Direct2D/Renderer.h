/****************************** Module Header ******************************\
 Module Name:  Renderer.h
 Project:      CppWin7Direct2D
 Copyright (c) Microsoft Corporation.

 The Renderer class is responsible for most of the rendering task.

 This source is subject to the Microsoft Public License.
 See http://www.microsoft.com/en-us/openness/licenses.aspx#MPL
 All other rights reserved.

 THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
 EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
 WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

#pragma once
class Renderer
{
private:
	ID2D1Factory* m_pDirect2dFactory;
	ID2D1HwndRenderTarget* m_pRenderTarget;
	ID2D1SolidColorBrush* m_pSignal2Brush;

	ID2D1SolidColorBrush* m_pSignal1Brush;
	ID2D1SolidColorBrush* m_pSignal3Brush;

	//ID2D1SolidColorBrush* m_pContinentBrush;
	//ID2D1RadialGradientBrush* m_pStartOutlineBrush;

	ID2D1SolidColorBrush* m_pAxisCoordBrush;

	ID2D1StrokeStyle* m_pStrokeStyle;
	//ID2D1PathGeometry* m_pAxisCoord;
	//ID2D1PathGeometry* m_pPlanetUpPath;
	//ID2D1PathGeometry* m_pPlanetDownPath;
	int m_animateTranslateX;
	bool m_animateToRight;

public:
	Renderer(HWND hwnd, int width, int height);
	~Renderer(void);

	HRESULT Init();
	HRESULT Render();
	void DiscardResources();

	HWND m_hwnd;
	bool m_animate;
	int m_clickedPointX;
	int m_clickedPointY;

	float signal1[1000];
	float signal2[1000];
	float signal3[1000];

    float s1_ampl;
    float s1_offs;
	boolean s1_enable;

	boolean s1_trig_enable;
	float s1_trig_value;
	boolean s1_trig_lower;

    float s2_ampl;
    float s2_offs;
	boolean s2_enable;

	boolean s2_trig_enable;
	float s2_trig_value;
	boolean s2_trig_lower;

    float s3_ampl;
    float s3_offs;
	boolean s3_enable;

	int   trig_delay;
	int	  trig_cnt;
	float trig_flag;

	boolean freeze;

	int   cur_xtime;
	bool  com_connect;

	bool  debug_en;

	HWND hDlg;

private:
	//HRESULT CreateAxisCoord();

	HRESULT CreateDeviceIndependentResources();
	HRESULT CreateDeviceResources();

	//////HRESULT CreatePlanetUpPath();
	//////HRESULT CreatePlanetDownPath();
	//////HRESULT CreateStarOutlineBrush();
	//void DrawPlanet(D2D1_ELLIPSE planet);
};

