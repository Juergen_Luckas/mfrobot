#include "OLED_Driver.h"
#include "OLED_GFX.h"
#include "Fighter.h"

extern OLED_GFX oled;
ship_t ship;

#define laserMax 5
laser_t laser[laserMax] = {0};

#define alienMax 8
alien_t alien[alienMax] = {0};

#define bombMax 10
bomb_t bomb[bombMax] = {0};

bigalien_t bal;

int16_t alienWait=0;
int16_t alienDx=3;
int16_t alienDy=0;

void drawFighterFrame (void) {
  oled.Clear_Screen();
  ship.x=64;
  ship.y=111;
  ship.sprite = 0;
  ship.wait = 0;
  for(int i=0; i<laserMax; i++) laser[i].on = false;
  for(int i=0; i<bombMax; i++) bomb[i].on = false;
}

void initBal(void) {
   oled.Clear_Screen();
   bal.index=0;
   bal.ywait=0;
   for (int i=0; i<128; i++) bal.yDemaged[i]=128;
   bal.yBg=0;
   bal.bgSpeed=4;
   bal.drxmax=0;
   bal.rightBomb=true;
   bal.leftBomb=true;   
   oled.Write_Command(0xA1);
   oled.Write_Data(bal.yBg); //A  
}

void setupAlien(void) {
  for(int i=0; i<8; i++) {
    alien[i].on=true;
    alien[i].x=(i%4)*25-10;
    alien[i].y=(i/4)*25-40;
    alien[i].sprite=0;
  }
}

void startLaser(int16_t x, int16_t y) {
  for(int i=0; i<laserMax; i++) {
    if (laser[i].on == false) {
      laser[i].on = true;
      laser[i].x = x;
      laser[i].y = y-9;
      laser[i].dy = -3;
      laser[i].wait = 0;
      break;
    }
  }
}

void startBomb(int16_t x, int16_t y) {
  for(int i=0; i<bombMax; i++) {
    if (bomb[i].on == false) {
      bomb[i].on = true;
      bomb[i].x = 10*x;
      bomb[i].y = y+10;
      bomb[i].dy = 4;
      bomb[i].dx = 0;
      bomb[i].wait = 0;
      bomb[i].boType = 0;
      break;
    }
  }
}

void startBomb2(void) {
  if ((random(100)>95) && (bal.yBg > 35)) {
    for(int i=0; i<bombMax; i++) {
      if (bomb[i].on == false) {
        int bdx=random(40)-20;
        if (bdx>0) {
          bomb[i].x=10*(64-25);
          if (bal.leftBomb) bomb[i].on = true;
        } else {
          bomb[i].x=10*(64+25);
          if (bal.rightBomb) bomb[i].on = true;
        }
        bomb[i].y = bal.yBg-15;
        bomb[i].dy = 2;
        bomb[i].dx = bdx;
        bomb[i].yBg = bal.yBg;
        bomb[i].wait = 0;
        bomb[i].boType = 1;
//        Serial.print("Bomb2: ");
//        Serial.print(i);
//        Serial.print("\n");
        break;
      }
    }
  }
}

void processLaser(void) {
  for(int i=0; i<laserMax; i++) {
    if (laser[i].on) {
      laser[i].wait++;
      if (laser[i].wait > 1) {
        laser[i].wait=0;
        oled.Set_Color(BLACK);
        oled.Draw_FastVLine(laser[i].x, -laser[i].yBg + laser[i].y-10, 10);
        laser[i].yBg = bal.yBg;
        laser[i].y += laser[i].dy;
        int xi=laser[i].x;
        if ((laser[i].y < 10) || (laser[i].y < (bal.yBg + 11 - bal.yDemaged[xi]))) {
          if (bal.yDemaged[xi] <= 23) bal.yDemaged[xi]++;
          laser[i].on = false;
        } else {
          oled.Set_Color(WHITE);
          oled.Draw_FastVLine(laser[i].x, -bal.yBg + laser[i].y-10, 10);
        }
      }
    }
  }
}

void processAlien(void) {
  int16_t newDx = alienDx;
  int16_t newDy = 0;
  int16_t numberOfAlien = 0;
  alienWait++;
  if (alienWait > 5) {
    alienWait = 0;
    for(int i=0; i<alienMax; i++) {
      if (alien[i].on) {
        numberOfAlien++;
        oled.drawSpriteObject(alien[i].x-7,alien[i].y-7,&clearframe);
        if ((random(100)>90) && (alien[i].x>0) && (alien[i].x<127) && (alien[i].y>-10)) startBomb(alien[i].x,alien[i].y);
        alien[i].x += alienDx;
        alien[i].y += alienDy;
        if (((alien[i].x > 137)||(alien[i].x < -10)) && (newDy==0)) {
          newDx = -alienDx; 
          newDy = 5;
        }
        oled.drawSpriteObject(alien[i].x-7,alien[i].y-7,&alienSprite);
      }    
      if (alien[i].sprite == 1) {
        oled.drawSpriteObject(alien[i].x-7,alien[i].y-7,&explosionbig);
        alien[i].wait++;
        if (alien[i].wait>1) {
          alien[i].sprite = 2;
          alien[i].wait = 0;
        }
      }
      if (alien[i].sprite == 2) {
        oled.drawSpriteObject(alien[i].x-7,alien[i].y-7,&explosionsmall);
        alien[i].wait++;
        if (alien[i].wait>1) {
          alien[i].sprite = 3;
          alien[i].wait = 0;
        }
      }
      if (alien[i].sprite == 3) {
        oled.drawSpriteObject(alien[i].x-7,alien[i].y-7,&clearframe);
        alien[i].sprite = 0;
      }
    }
    alienDx = newDx;
    alienDy = newDy;
//    Serial.print("Alien:");
//    Serial.print(numberOfAlien);
//    Serial.print("\n");    
    if (numberOfAlien==0) {
      oled.Set_Color(WHITE);    
      oled.print_String(30, 60, (uint8_t *)"Well Done !", FONT_5X8);
    }   
  }
}

void drawBaldata(void) {
  int drx, color;
  int drxold = 0;

  drx = pgm_read_byte_near(&bigAlien_data[bal.index]);
  if (drx>0) {
    do { 
      bal.index++;
      color = pgm_read_byte_near(&bigAlien_data[bal.index]);
      bal.index++;
      oled.Set_Color((fighter_map[color]>>8)+((fighter_map[color]&0xFF)<<8));
      oled.Draw_FastHLine(64+drxold, 127-bal.yBg, drx);
      oled.Draw_FastHLine(64-drxold-drx, 127-bal.yBg, drx);
      drxold=drxold+drx;
      drx = pgm_read_byte_near(&bigAlien_data[bal.index]);
    } while (drx>0);
    bal.index++;
    for (int i=bal.drxmax; i<drxold; i++) {
      if (bal.yDemaged[63-i]==128) bal.yDemaged[63-i]=bal.yBg;
      if (bal.yDemaged[64+i]==128) bal.yDemaged[64+i]=bal.yBg;
    }
    if (bal.drxmax < drxold) bal.drxmax = drxold;
  }
}

void animateBal(void) {
  if (bal.exploded>0) {
    
  } else {
    if (bal.yBg > 35) {
      if (bal.animateWait > 4) {
        oled.drawSpriteObject(61,128-35,&balg1);
      } else {
        oled.drawSpriteObject(61,128-35,&balg2);
      }  
      (++bal.animateWait) %=8;
    }    
  }
}

void moveBg(void) {
  if (bal.ywait==0) {
    drawBaldata();
    bal.yBg++;
    oled.Write_Command(0xA1);
    oled.Write_Data(127-bal.yBg); //A
  }
  if (bal.yBg==44) bal.bgSpeed=64;
  (++bal.ywait) %= bal.bgSpeed;
}

void clearBomb(int x, int y, int old_yBg, int type) {
  if (type==0) {
    oled.Set_Color(BLACK);
    oled.Draw_Circle(x, y, 3);
  } else {
    for (int i=0; i<8; i++) {
      if ((y+bomb2_ydata[i]) > (bal.yBg-bal.yDemaged[x+bomb2_xdata[i]])) {
          oled.Set_Color(BLACK);
        } else {
          oled.Set_Color(0x6922);
      }
      oled.Draw_Pixel(x+bomb2_xdata[i], (-old_yBg+y+bomb2_ydata[i]+128)&0x7F);
    }   
  }
  
}

void drawBomb(int x, int y, int type) {
  oled.Set_Color(GREEN);
  if (type==0) {
    oled.Draw_Circle(x, y, 3);
  } else {
    for (int i=0; i<8; i++) oled.Draw_Pixel(x+bomb2_xdata[i], (-bal.yBg+y+bomb2_ydata[i]+128)&0x7F); //-bal.yBg+
  }  
}


void processBomb(void) {
  for(int i=0; i<bombMax; i++) {
    if (bomb[i].on) {
      bomb[i].wait++;
      if (bomb[i].wait > 2) {
        bomb[i].wait = 0;
        clearBomb(bomb[i].x/10, bomb[i].y, bomb[i].yBg, bomb[i].boType); //-(bal.yBg-bomb[i].yBg
        bomb[i].yBg = bal.yBg;
        bomb[i].y += bomb[i].dy;
        bomb[i].x += bomb[i].dx;
        if (bomb[i].y > 122) {
          bomb[i].on = false;
        } else {
          drawBomb(bomb[i].x/10, bomb[i].y, bomb[i].boType);
        }
      }
    }
  }
}

void checkLaserHit(void) {
  int32_t dist;
  for(int lac=0; lac<laserMax; lac++) {
    for(int alc=0; alc<alienMax; alc++) {
      if (laser[lac].on && alien[alc].on) {
        dist=sq((int32_t)(2*(laser[lac].x-alien[alc].x)))+sq((int32_t)(laser[lac].y-5-alien[alc].y));
        if (dist<=111) {
          laser[lac].on=false;
          alien[alc].on=false;     
          alien[alc].sprite=1;
          alien[alc].wait=0;
          oled.Set_Color(BLACK);
          oled.Draw_FastVLine(laser[lac].x, laser[lac].y-10, 10);
        }   
      }
    }
  }
}
 
void checkbohit(void) {
  int32_t dist;
  for(int boc=0; boc<bombMax; boc++) {
    if (bomb[boc].on) {
      dist=sq(1.5*(bomb[boc].x/10-ship.x))+sq(bomb[boc].y-ship.y);
      if (dist<=111) {
        ship.sprite=1;
        bomb[boc].on = false;
      }
    }
  }
}

void moveFighter(bool shoot) {
  int val;
  uint8_t xpos;

  if (shoot && (ship.sprite==0))startLaser(ship.x, ship.y);
  
  val = analogRead(0);    // read the input pin
  xpos = (((long)val)*128)/1023;
  if (xpos < 5) xpos = 5;
  if (xpos > 123) xpos = 123;
  if (((xpos != ship.x) || (ship.yBg != bal.yBg)) && (ship.sprite==0)) {
    oled.drawSpriteObject(ship.x-7,-ship.yBg+ship.y,&clearframe);
    ship.x = xpos;
    ship.yBg = bal.yBg;
    oled.drawSpriteObject(ship.x-7,-bal.yBg+ship.y,&fighter);
  } 
  if (ship.sprite == 1) {
    oled.drawSpriteObject(ship.x-7,-bal.yBg+ship.y,&explosionbig);
    ship.wait++;
    if (ship.wait>6) {
      ship.sprite = 2;
      ship.wait = 0;
    }
  } else
  if (ship.sprite == 2) {
    oled.drawSpriteObject(ship.x-7,-bal.yBg+ship.y,&explosionsmall);
    ship.wait++;
    if (ship.wait>6) {
      ship.sprite = 3;
      ship.wait = 0;
    }
  } else
  if (ship.sprite == 3) {
    oled.drawSpriteObject(ship.x-7,-bal.yBg+ship.y,&clearframe);
    oled.Set_Color(WHITE);    
    oled.print_String(30, 60, (uint8_t *)"Game Over !", FONT_5X8);
    ship.wait++;
    if (ship.wait>6) {
      ship.sprite = 0;
      ship.wait = 0;
    }
  } 
}

