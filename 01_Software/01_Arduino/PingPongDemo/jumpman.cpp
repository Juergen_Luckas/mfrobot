#include "OLED_Driver.h"
#include "OLED_GFX.h"
#include "jumpman.h"

#define JumpDuration 6
#define NumBShit 5
#define NumHurdle 3

extern OLED_GFX oled;
background_t bg;
bird_t  bird;
jman_t  jman;
bshit_t bshit[NumBShit];
hurdle_t hurdle[NumHurdle];
int newHurdle=0;
int newHurldeHight;

const sprite_t *birdSprite[4]={&birdcenter,&birddown,&birdcenter,&birdup};
const sprite_t *jmanSprite[4]={&mancenter,&manright,&mancenter,&manleft};

void initJumpman(void) {
  oled.Clear_Screen();  
  bg.hiCnt=0;
  bg.loCnt=0;
  bg.hiY=200;
  bg.loY=100;
  bg.hiDY=0;
  bg.loDY=0;
  bg.xcur=127;
  bird.x=0;
  bird.y=0;
  bird.frame=0;
  bird.wait=0;
  bird.nr=0; 
  jman.wait=0;
  jman.jump=false;
  jman.x=64;
  jman.xold=64; 
  jman.yold=100; 
  jman.frame=0;
  jman.hit=0;
  jman.jumpActive=false;
  for (int i=0; i<NumBShit; i++) bshit[i].on = false;
  for (int i=0; i<128; i++) bg.hiValue[i] = 20;
  oled.Set_FillColor(0x9fff);
  oled.Fill_Rect(0, 0, 128, 128);
  oled.Set_FillColor(0xfff6);
  oled.Fill_Rect(0, 107, 128, 21); 
  oled.drawSpriteObject(0,0,&sun);
  oled.Write_Command(0xA1);
  oled.Write_Data(0); //A    
  oled.Set_Color(0xFfff);
  oled.Draw_FastHLine(60,0,20);
  oled.Draw_FastHLine(60,7,20);  
  oled.Draw_FastVLine(60,1,6);  
  oled.Draw_FastVLine(80,1,6);   
  newHurdle=0;
}

void calcNewGround(void) {
  int16_t hDy;
  int16_t lDy;
  int16_t hy;
  int16_t ly;
  bg.hiCnt +=1;
  if (bg.hiCnt > 10) {
     bg.hiCnt = 0;
     hDy = random(10)-5;
     if (hDy*bg.hiDY < 0) hDy = 0;
     bg.hiDY = hDy;
  }
  bg.loCnt +=1;
  if (bg.loCnt > 10) {
     bg.loCnt = 0;
     lDy = random(10)-5;
     if (lDy*bg.loDY < 0) lDy = 0;
     bg.loDY = lDy;
  }
  bg.hiY += bg.hiDY;
  if (bg.hiY > 400) bg.hiY = 400;
  if (bg.hiY < 100) bg.hiY = 100;
  bg.loY += bg.loDY;
  if (bg.loY > bg.hiY-50) bg.loY = bg.hiY-50;
  if (bg.loY < 50) bg.loY = 50;
  bg.xcur += 1;
  if (bg.xcur > 127) bg.xcur = 0;
  bg.hiValue[bg.xcur]=(bg.hiY/10); 
}

bool setHurdle(int16_t x) {
  int i;
  for (i=0; i<NumHurdle; i++) {
    if (hurdle[i].on == false) {
      hurdle[i].on = true;
      hurdle[i].bgx =x;
      break;      
    }
  } 
  return (i<NumHurdle);
}

bool checkHurdle(void) {
  for (int i=0; i<NumHurdle; i++) {
    if (hurdle[i].on) {
      if (hurdle[i].bgx == bg.xcur) hurdle[i].on = false;
    }
  } 
  return true;
}  

void drawGround(void) {
  oled.Set_Color(0x9fff);
  oled.Draw_FastVLine(127, 0, 127- (bg.hiY/10));
  oled.Set_Color(0xfff6);
  oled.Draw_FastVLine(127, 127- (bg.hiY/10) , bg.hiY/10-bg.loY/10+1);
  oled.Set_Color(0xE673);
  oled.Draw_FastVLine(127, 127- (bg.loY/10) , bg.loY/10);

  if (newHurdle) {   
    oled.Set_Color(0x2269);
    oled.Draw_FastVLine(127, newHurldeHight, 4);   
    newHurdle--;
  } else {
    if (random(100)>98) {
      if (setHurdle(bg.xcur)) {
        oled.Set_Color(0x2269);
        newHurldeHight = 127-(bg.hiY/10)-4;
        oled.Draw_FastVLine(127, newHurldeHight, 4);
        newHurdle = 2; 
      }
    } 
  }
}

void startScrollBackground(void) {
  oled.Write_Command(0x96);
  oled.Write_Data(0x3F); //A
  oled.Write_Data(0x08); //B
  oled.Write_Data(0x78); //C
  oled.Write_Data(0x00); //D
  oled.Write_Data(0x01); //E
  oled.Write_Command(0x9F);      
}

void stopScrollBackground(void){
  oled.Write_Command(0x9E);
}

void moveBird(void) {
  char buff[4];
  if (bird.x>0) {
    bird.x -=1;
    bird.wait += 1;
    if (bird.wait>3) {
       bird.wait=0;
       bird.frame += 1;
       if (bird.frame > 3) bird.frame = 0;
    }
    oled.drawSpriteObject(bird.x,bird.y,birdSprite[bird.frame]);
    if ((random(100)>95) && (bird.x < 127-20) && (bird.nr > 2)) {
      startBshit(bird.x,bird.y);
    }
  } else {
    if (random(100)>96) {
      bird.x = 127;
      bird.y = 8+random(12);
      bird.nr++;
      oled.Set_FillColor(0x9fff);
      oled.Fill_Rect(100, 0, 20, 8);
      oled.Set_Color(0x0000);
      oled.Set_BackColor(0x9fff);
      itoa(bird.nr, buff, 10);
      oled.print_String(100, 0, (uint8_t *)buff, FONT_5X8);   
    }
  }
}

void startBshit(int16_t x,int16_t y) {
  for (int i=0; i<NumBShit; i++) {
    if (bshit[i].on == false) {
      bshit[i].on = true;
      bshit[i].x = x * 10 + 200;
      bshit[i].y = y * 10 + 100;
      bshit[i].dx = random(5)-5; 
      bshit[i].dy = 10;
      break;
    }
  } 
}

void processBshit(void){
  int bsy;
  for(int i=0; i<NumBShit; i++) {
    bshit_t* b=&bshit[i];
    if (b->on) {
      (b->wait)++;
      if (b->wait>0) {
        b->wait=0;
        oled.Set_Color(0x9fff);
        oled.Draw_Circle((b->x)/10-1,(b->y)/10,2);
        b->x += b->dx;
        b->y += b->dy;
        b->dx = min(b->dx,127);
        if (bg.xcur>=(128-((b->x)/10))) {
          bsy=bg.hiValue[(bg.xcur-128+(b->x)/10)];
        }else{
          bsy=bg.hiValue[(bg.xcur+(b->x)/10)];
        }  
        if (((b->y)/10) >= (128-bsy-2)){
          b->on = 0;
        } else {
          oled.Set_Color(0x39A6);
          oled.Draw_Circle((b->x)/10,(b->y)/10,2);
        }
      }
    }
  }
}

bool checkBshitHit(void){
  int32_t dist;
  bool res=false;
  for(int i=0; i<NumBShit; i++) {
    bshit_t* b=&bshit[i];
    if (b->on) {
      dist=sq((b->x)/10-jman.xold)+sq(2*((b->y)/10-127+jman.yold));
      if (dist<=111) {
        jman.hit++;
        if (jman.hit>15) {
          oled.Set_Color(0x0000);
          oled.Set_BackColor(0x9fff);
          oled.print_String(40, 60, (uint8_t *)"Game Over", FONT_5X8);   
          res=true;
        }
        oled.Set_FillColor(0xF800);
        oled.Fill_Rect(61, 1, jman.hit, 6);
        b->on = false;
      }
    }
  }
  return res;  
}

void moveJman(void) {
  int16_t hy;
  int16_t hym=0;
  int16_t mesArea[3]={-9,0,9};
  for (int i=0; i<3; i++) {
    if (bg.xcur >= (128-(jman.x+mesArea[i]))) {
      hy=bg.hiValue[bg.xcur-128+(jman.x+mesArea[i])];
    } else {
      hy=bg.hiValue[bg.xcur+(jman.x+mesArea[i])];
    }
    hym=max(hy,hym);
  }
  hym +=16; 
  oled.Set_FillColor(0x9fff);
  if ((jman.jump == JumpDuration) || ((jman.jump == 0) && jman.jumpActive)) {
    oled.Fill_Rect(jman.xold-9-1, 127-jman.yold-16, 17, 32);
  } else {
    if (((jman.xold+1) <= jman.x) || jman.jumpActive) { 
      oled.Fill_Rect(jman.xold-9-1, 127-jman.yold-16, jman.x-jman.xold+1, 32);
    }
    if (jman.yold < hym) oled.Fill_Rect(jman.xold-9-1, 127-jman.yold+15, 17, hym-jman.yold);
    if (jman.yold > hym) oled.Fill_Rect(jman.xold-9-1, 127-jman.yold-16, 17, jman.yold-hym);
  }
  jman.yold = hym;
  jman.xold = jman.x;
  if (jman.jump>0) {
    jman.jumpActive = true;
    jman.jump--;
    jman.yold += 15;
    oled.drawSpriteObject(jman.xold-9,127-jman.yold-16,&manjump);
  } else {
    jman.jumpActive = false;    
    jman.frame++;
    if (jman.frame>15) jman.frame=0;
    oled.drawSpriteObject(jman.xold-9,127-jman.yold-16,
                          jmanSprite[jman.frame/4]);
  }
}

void actionJman(bool jump) {
  int val;
  int xpos;
  if (jump) jman.jump=JumpDuration;
  val = analogRead(0);
  int xgoal = (((long)val)*80)/1023+24;
  jman.x=jman.x+(xgoal-jman.x)/10;
}
