#include "OLED_Driver.h"
#include "OLED_GFX.h"

#include <SPI.h>
#include <Wire.h>

OLED_GFX oled = OLED_GFX();

#define xBallSize 15
#define yBallSize 10

int previousVal = 50;
int yspeed = 0;
int ballSpeed = 3;

int pressedPinPrev[2] = {0};
#define debounceCountMax 3
uint8_t debounceCount[2] = {0};

uint8_t currentMode = 0;
uint8_t previousMode = !currentMode;
volatile unsigned int timerFlag = 0;
volatile unsigned int timerCnt = 0;
unsigned int maxTimer = 0;
unsigned int timerFlagCnt = 0;

int errcount = 0;
int xpos  = 2000;
int ypos  = 4000;
int dxpos = 98;
int dypos = sqrt(10000-dxpos*dxpos);
int score = 0;

bool gameFailed=false;

void setup()  {

  //Init GPIO
  pinMode(oled_cs, OUTPUT);
  pinMode(oled_rst, OUTPUT);
  pinMode(oled_dc, OUTPUT);
  pinMode(2, INPUT);
  pinMode(3, INPUT);

  //Init UART
  Serial.begin(115200);

#if INTERFACE_4WIRE_SPI
  //Init SPI
  SPI.setDataMode(SPI_MODE0);
  SPI.setBitOrder(MSBFIRST);
  SPI.setClockDivider(SPI_CLOCK_DIV2);
  SPI.begin();

#elif INTERFACE_3WIRE_SPI

  pinMode(oled_sck, OUTPUT);
  pinMode(oled_din, OUTPUT);

#endif

  oled.Device_Init();

  oled.Clear_Screen();

  oled.Set_Color(BLUE);
  oled.print_String(20, 50, (uint8_t *)"Hello WaveShare !", FONT_5X8);
  delay(2000);

  oled.Set_Color(GREEN);

  // Setup the timer
  cli(); 
  TCCR1A = 0;
  TCCR1B = 0;	  
  TCCR1B |= B00000011;  // Set CS12 to 1 so we get prescalar 64 => 
  TCNT1 = 0;		
  TIMSK1 |= B00000010;   
  OCR1A = 6250;         // 25 ms
  sei();
  
}

ISR(TIMER1_COMPA_vect){
  TCNT1  = 0;                  //First, set the timer back to 0 so it resets for next interrupt
  timerFlag = 1;
  timerCnt += 1;
}

void drawMulitmeterFrame (void) {
  oled.Clear_Screen();
  oled.Set_Color(YELLOW);
  oled.Set_BackColor(0x0000);

  float vlen = 100.0;
  float deg2rad=0.0;
  float deg2radp1=0.0;  
  
  for (int16_t x=-5; x <= 5; x++)  {
    deg2rad   = x*6.0 / 180.0 * 3.1415926535;
    deg2radp1 = (x+1)*6.0 / 180.0 * 3.1415926535;
    oled.Draw_Line(64 - (vlen+5)*sin(deg2rad), 128-((vlen+5)*cos(deg2rad)), 64 - (vlen+12)*sin(deg2rad), 128-((vlen+12)*cos(deg2rad)));
    if (x<5) 
      oled.Draw_Line(64 - (vlen+5)*sin(deg2rad), 128-((vlen+5)*cos(deg2rad)), 64 - (vlen+5)*sin(deg2radp1), 128-((vlen+5)*cos(deg2radp1)));
  }
  oled.Set_Color(WHITE);
  oled.print_String(10, 60, (uint8_t *)"Hello WaveShare !", FONT_5X8);   
}

void drawMulitmeter (void) {
  char buff[15];
  float vlen = 100.0;  
  float voltage;
  float deg2rad=0.0;  
  int val;
  val = analogRead(0);    // read the input pin
  //Serial.println(val);             // debug value  

  deg2rad   = (previousVal*60.0/1024.0-30.0) / 180.0 * 3.1415926535;
  oled.Set_Color(BLACK);
  oled.Draw_Line(64 - (vlen)*sin(deg2rad), 128-((vlen)*cos(deg2rad)), 64, 127);
  deg2rad   = (val*60.0/1024.0-30.0) / 180.0 * 3.1415926535;
  oled.Set_Color(BLUE);
  oled.Draw_Line(64 - (vlen)*sin(deg2rad), 128-((vlen)*cos(deg2rad)), 64, 127);
  previousVal = val;
//  voltage = val*5.0/1024.0;
//  dtostrf(voltage, 7, 3, buff);
//  oled.Set_Color(WHITE);
//  oled.print_String(60, 100, (uint8_t *)buff , FONT_5X8);   
}

void drawPinPongFrame (void) {
  oled.Clear_Screen();
  oled.Set_Color(GREEN);
  oled.Draw_Line(0,0,SSD1351_WIDTH - 1, 0 );  
  oled.Draw_Line(0,1,SSD1351_WIDTH - 1, 1 );  
  oled.Draw_Line(SSD1351_WIDTH - 2, 2,SSD1351_WIDTH - 2,SSD1351_HEIGHT - 2);  
  oled.Draw_Line(SSD1351_WIDTH - 1, 2,SSD1351_WIDTH - 1,SSD1351_HEIGHT - 2);  
  oled.Draw_Line(0, SSD1351_HEIGHT - 2,SSD1351_WIDTH - 1,SSD1351_HEIGHT - 2);  
  oled.Draw_Line(0, SSD1351_HEIGHT - 1,SSD1351_WIDTH - 1,SSD1351_HEIGHT - 1);  
  errcount = 0;
  xpos  = 2000;
  ypos  = 4000;
  dxpos = 100;
  dypos = sqrt(10000-dxpos*dxpos);
  score = 0;
  ballSpeed = 3;
}  
  
void drawPinPongPaddle (void) {
  int val;
  uint8_t ypos;
  val = analogRead(0);    // read the input pin
  
  if (previousVal != val)
  {
    ypos = (((long)previousVal)*128)/1023;
    if (ypos < 2) ypos = 3;
    if (ypos > 106) ypos = 106;
    oled.Set_Color(BLACK);  
    oled.Draw_RoundRect(0, ypos, 5, 20, 2);
    ypos = (((long)val)*128)/1023;
    if (ypos < 2) ypos = 3;
    if (ypos > 106) ypos = 106;
    oled.Set_Color(BLUE);  
    oled.Draw_RoundRect(0, ypos, 5, 20, 2);
    yspeed = val - previousVal;
    previousVal = val;
  } else {
    yspeed = val - previousVal;
  }
  
}

void moveBall (void){
  char buff[4];
  oled.Set_Color(BLACK);
  oled.Fill_Circle(xpos/100, ypos/100, 4);

  // c2 = a2 + b2
  int nypos = ypos + dypos * ballSpeed;
  if (nypos > 12200) {
    nypos = 2*12200 - nypos; 
    dypos = -dypos;
  }  
  if (nypos < 600) {
    nypos = 1200 - nypos; 
    dypos = -dypos;
  }  

  int nxpos = xpos + dxpos * ballSpeed;
  if (nxpos > 12200) {
    nxpos = 2*12200 - nxpos; 
    dxpos = -dxpos;
  }  
  if (nxpos < 900) {
    int ydiff = ypos - 1000 - ((((long)previousVal)*12800)/1023);
    if (abs(ydiff)>1200) {
      errcount++;
      delay(1500);
    } else {
      score++;
      itoa(score, buff, 10);
      oled.Set_Color(WHITE);
      oled.print_String(80, 6, (uint8_t *)buff , FONT_5X8);   
      if ((score % 20) == 0) ballSpeed++;
    }
    
    oled.Set_Color(RED);  
    oled.Draw_Pixel(50+errcount,3);
    nxpos = 1800 - nxpos; 
    
//    Serial.print("ydiff: ");
//    Serial.print(ydiff);
//    Serial.print("\n");
//    Serial.print("previousVal: ");
//    Serial.print(previousVal);
//    Serial.print("\n");
    dxpos = -dxpos;
   
    dypos = dypos + (2*ydiff/100);
    if (dypos < -50) dypos = 50;
    if (dypos >  50) dypos = 50;

      dxpos = sqrt(10000 - dypos*dypos);
      
  }  
  xpos=nxpos;
  ypos=nypos;  
  oled.Set_Color(YELLOW);
  oled.Fill_Circle(xpos/100, ypos/100, 4);  
}


bool pressedButton(int pinId) {

  bool ret = false;
  int pressedPinCurr = 1-digitalRead(pinId+2);
  
  if (pressedPinCurr != pressedPinPrev[pinId]) {
    debounceCount[pinId]++;
    if (debounceCount[pinId] >= debounceCountMax) {
      debounceCount[pinId] = 0;
      pressedPinPrev[pinId] = pressedPinCurr;
      if (pressedPinPrev[pinId] == 1) ret = true;
    }
  } else {
    debounceCount[pinId] = 0;
  } 
  return ret;
}

void loop() {

  if (pressedButton(0)) currentMode++;
  
  switch (currentMode) {
  case 0:
    if (previousMode != currentMode){
       Serial.print("Multimeter\n");       
       drawMulitmeterFrame();
    } else {
       drawMulitmeter();
    }
    previousMode = currentMode;
    break;
  case 1:
    if (previousMode != currentMode){
       Serial.print("Pong\n");              
       drawPinPongFrame();   
    } else {
       drawPinPongPaddle();
       moveBall();
    }
    previousMode = currentMode;
    break;
  case 2:
    if (previousMode != currentMode){
       Serial.print("Fighter1\n");       
       drawFighterFrame();   
       setupAlien();
       initBal();          
       gameFailed=false;
    } else {
      if (!gameFailed) {
        moveFighter(pressedButton(1));       
        processLaser();
        processAlien();
        processBomb();
        checkLaserHit();
        checkbohit();
      }
    }
    previousMode = currentMode;
    break;
  case 3:
    if (previousMode != currentMode){
       Serial.print("Fighter2\n");       
       drawFighterFrame();   
       //setupAlien();
       initBal();          
    } else {
        moveBg();
        moveFighter(pressedButton(1));       
        processLaser();
        animateBal();
        startBomb2();
        //processAlien();
        processBomb();
        //checkLaserHit();
        checkbohit();
    }
    previousMode = currentMode;
    break;
  case 4:
    if (previousMode != currentMode){
       Serial.print("Jump Man\n");              
       initJumpman();
       gameFailed=false;
    } else {
      if (!gameFailed) {
        startScrollBackground();
        calcNewGround();
        delay(20);
        stopScrollBackground();
        checkHurdle();
        drawGround();
        moveBird();
        processBshit();
        actionJman(pressedButton(1));
        moveJman();    
        gameFailed=checkBshitHit();  
      }
    }
    previousMode = currentMode;
    break;
  default:
    currentMode = 0;
  }

  volatile unsigned int curTimer = TCNT1;
  volatile unsigned int localFlag = timerFlag;
  volatile unsigned int localCnt  = timerCnt;
  
  if ((curTimer > maxTimer) || ((localCnt%50)==0)) {
    if (curTimer > maxTimer) maxTimer = curTimer;
    if (timerFlag) timerFlagCnt++;
    Serial.print(curTimer);
    Serial.print(" - ");
    Serial.print(maxTimer);
    Serial.print(" - ");
    Serial.print(timerFlagCnt);
    Serial.print(" - ");
    Serial.print(localCnt);
    Serial.print("\n");
  }
  while (!timerFlag) {}
  timerFlag = 0;

//
//  oled.Set_Color(BLUE);
//  oled.print_String(20, 50, (uint8_t *)"Hello WaveShare !", FONT_5X8);
//
//  oled.Set_Color(WHITE);
//  oled.print_String(10, 10, (uint8_t *)"Read RAM !", FONT_5X8);
//  oled.drawRestorableObject(20, 50, xBallSize, yBallSize, NULL, restoreBitmap);
//
//  uint8_t *rPtr = restoreBitmap;
//  for (uint16_t xy=0; xy < (2 * xBallSize * yBallSize); xy++) {
//       Serial.print(*rPtr,HEX);
//       Serial.print(";");
//  }
//  
//
//  oled.drawRestorableObject(20, 50, xBallSize, yBallSize, origBitmap, NULL);
//  delay(3000);
//  
//  oled.drawRestorableObject(20, 80, xBallSize, yBallSize, restoreBitmap, NULL);
//  delay(3000);
//
//  oled.Set_Color(YELLOW);
//  oled.Fill_Circle(64, 64, 4);
//  delay(2000);

}





