// Protocol
// The data are transferred in chars - each message has 10 chars.
// Startflag         "\"
// Msg ID            0-255
// MsgValue[0..7]    4 Byte Hex-values, each nibble (4Bit) is transmitted as char in sequence, with nibble-value plus 0x30
//                   Nibble x0 ->  0+0x30 = "0"
//                   Nibble x1 ->  1+0x30 = "1"
//                   Nibble xA -> 14+0x30 = ":"
//                   Nibble xB -> 14+0x30 = ";"
//                   Nibble xC -> 14+0x30 = "<"
//                   Nibble xD -> 14+0x30 = "="
//                   Nibble xE -> 14+0x30 = ">"
//                   Nibble xF -> 15+0x30 = "?"
//                   0123456789ABCDEF
//                   0123456789:;<=>?
//
// Data  Raspberry --> Arduino
//  1: \1xxxxyyyy - move stepper; step1 - xxxx=int16; step2 - yyyy=int16
//  2: \2xxxx0000 - move head; pos xxxx=uint16 physical value [700..2300]; neutral 1500
//  3: \3000x000y - steppper_on: x=1 true, else false; y=1 then reset acc steps and angle
//  4: \400000000 - display Kx parameters
//  5: \5xxxxxxxx - set Kd             - 0.050
//  6: \6xxxxxxxx - set Kp             - 0.32
//  7: \7xxxxxxxx - set Ki_thr         - 0.1
//  8: \8xxxxxxxx - set Kp_thr         - 0.080
//  9: \9xxxxxxxx - set Kd_position    - 0.45
// 10: \:xxxxxxxx - set Kp_position    - 0.06
// 11: \;xxxxxxxx - set wheelbase
// 12: \<xxxxyyyy - set throttle int16 , steering int16; normalized to -0,5 .. 0,5
// 13: \=xxxxxxxx - set angle_offset   - 1.65 
//
// #define KP_RAISEUP 0.1   
// #define KD_RAISEUP 0.16   
//
// Data  Arduino --> Raspberry 
// \1xxxxxxxx - float acc_vect1_x
// \2xxxxxxxx - float acc_vect1_y
// \3xxxxxxxx - float acc_alpha
// \4xxxxxxxx - float angle_adjusted
// \5xxxxyyyy - int16_t adistance0, int16_t headpos
// \6xxxxyyyy - int16_t actual_robot_speed, int16_t adistance1

int rx_data_cnt;
int rx_data_id;
unsigned long rx_data_val;
byte com_tx[10];

void SER_init()
{
    rx_data_cnt = -1;
    rx_data_id = 0;
    rx_data_val = 0;
}


void SER_sent_data(uint8_t id, int v1, int v2)
{
  //prepare byte data array to be transmitted via USB
  //The following data are send start flag = 0xFF and the nibbles of two 16 bits words
  //It takes 11 Bits (8N1) * 10 (bytes) / 57600 baud = 2 ms
  //prepare byte data array to be transmitted via USB
  //The following data are send start flag = 0xFF and the nibbles of two 16 bits words
  //It takes 11 Bits (8N1) * 10 (bytes) / 57600 baud = 2 ms
  
  com_tx[0] = 92; // '\'
  com_tx[1] = id + 0x30;
  com_tx[2] = ((v1 & 0xF000) >> 12) + 0x30;
  com_tx[3] = ((v1 & 0x0F00) >> 8) + 0x30;
  com_tx[4] = ((v1 & 0x00F0) >> 4) + 0x30;
  com_tx[5] = (v1 & 0x000F) + 0x30;
  com_tx[6] = ((v2 & 0xF000) >> 12) + 0x30;
  com_tx[7] = ((v2 & 0x0F00) >> 8) + 0x30;
  com_tx[8] = ((v2 & 0x00F0) >> 4) + 0x30;
  com_tx[9] = ((v2 & 0x000F)) + 0x30;
  Serial.write(com_tx, 10);
}


boolean SER_receive_data()
{

  unsigned char rx_cur;
  boolean ret = false;
  int helpint; 

  if (Serial.available())
  {
    rx_cur = Serial.read();

    //         Serial.print("Raw:");Serial.println(((int)rx_cur),HEX);

    if (rx_cur == 92) // '\'
    {
      // that is the message start condition
      rx_data_cnt = 0;
      rx_data_id = 0;
      rx_data_val =  0;
    }
    else
    {
      if (rx_data_cnt == 0)
      {
          rx_data_id = rx_cur - 0x30; 
          rx_data_cnt = 1;
      }
      // A normal msg data, counter should be greater 0
      else 
      {
        if ((rx_data_cnt > 0) && (rx_data_cnt <= 8))
        {
          rx_data_val += ((unsigned long)((rx_cur-0x30) & 0x0F)) << (32 - (rx_data_cnt << 2));
  
          rx_data_cnt++;
  
          if (rx_data_cnt > 8)
          {
            rx_data_cnt = -1;
            
            // message is ready
            switch (rx_data_id)
            {
              case 1:
                OSCmove_steps1 = *(((int16_t*)&rx_data_val) + 1);
                OSCmove_steps2 = *((int16_t*)&rx_data_val);
                Serial.print("Move Steps1: "); Serial.print(OSCmove_steps1);Serial.print(" Steps2: ");Serial.println(OSCmove_steps2);
                OSCmove_mode = true;
                ret = true;
                break;
  
              case 2:
                headpos = *(((int16_t*)&rx_data_val)+1);
                BROBOT_moveServo(headpos);
                Serial.print("Move Head: "); Serial.println(headpos);
                break;

              case 3:
                stepper_on = ((*(((int16_t*)&rx_data_val)+1))==0x0001);
                Serial.print("Stepper On: "); Serial.println(stepper_on);
                if ((*((int16_t*)&rx_data_val))==0x0001) {
                  acc_vect1_x = 0.0;
                  acc_vect1_y = 0.0;
                  acc_alpha   = 0.0;
                  Serial.println("Reset acc values");
                }
                break;

              case 4: 
                Serial.println("Print Settings");
                Serial.print("5:Kd: "); Serial.println(Kd);
                Serial.print("6:Kp: "); Serial.println(Kp);
                Serial.print("7:Ki_thr: "); Serial.println(Ki_thr);
                Serial.print("8:Kp_thr: "); Serial.println(Kp_thr);
                Serial.print("9:Kd_pos: "); Serial.println(Kd_position);
                Serial.print("10:Kp_pos: "); Serial.println(Kp_position);
                break;

              case 5: 
                Kd_user = *((float*)&rx_data_val);
                Serial.print("Set Kd: "); Serial.println(Kd);
              break;
              
              case 6: 
                Kp_user = *((float*)&rx_data_val);
                Serial.print("Set Kp: "); Serial.println(Kp);
              break;

              case 7: 
                Ki_thr_user = *((float*)&rx_data_val);
                Serial.print("Set Ki_thr: "); Serial.println(Ki_thr);
              break;

              case 8: 
                Kp_thr_user = *((float*)&rx_data_val);
                Serial.print("Set Kp_thr: "); Serial.println(Kp_thr);
              break;
              
              case 9: 
                Kd_position = *((float*)&rx_data_val);
                Serial.print("Set Kd_position: "); Serial.println(Kd_position);
              break;
              
              case 10: 
                Kp_position = *((float*)&rx_data_val);
                Serial.print("Set Kp_position: "); Serial.println(Kp_position);
              break;

              case 11: 
                wheelbase = *((float*)&rx_data_val);
                Serial.print("Set wheelbase: "); Serial.println(wheelbase);
              break;

              case 12: 
                helpint = *(((int16_t*)&rx_data_val) + 1);
                OSCthrottle = float(helpint) / 65535.0;
                helpint = *((int16_t*)&rx_data_val);
                OSCsteering = float(helpint) / 65535.0;
                Serial.print("Throttle: "); Serial.print(OSCthrottle);Serial.print(" Steer: ");Serial.println(OSCsteering);
                ret = true;                
              break;
              
              case 13: 
                angle_offset = *((float*)&rx_data_val);
                Serial.print("Set angle_offset: "); Serial.println(angle_offset);
              break;
              
              default:
                Serial.print("Invalid Msg: "); Serial.print(rx_data_id);Serial.print(" val ");Serial.println(rx_data_val);              
                break;
            }
          }
        }
      }
    }
  }
  return ret;
}


