// BROBOT EVO 2 by JJROBOTS
// SELF BALANCE ARDUINO ROBOT WITH STEPPER MOTORS CONTROLLED WITH YOUR SMARTPHONE
// JJROBOTS BROBOT KIT: (Arduino Leonardo + BROBOT ELECTRONIC BRAIN SHIELD + STEPPER MOTOR drivers)
// This code is prepared for new BROBOT shield  with ESP8266 Wifi module
// Author: JJROBOTS.COM
// Date: 02/09/2014
// Updated: 25/06/2017
// Version: 2.82
// License: GPL v2
// Compiled and tested with Arduino 1.6.8. This new version of code does not need external libraries (only Arduino standard libraries)
// Project URL: http://jjrobots.com/b-robot-evo-2-much-more-than-a-self-balancing-robot (Features,documentation,build instructions,how it works, SHOP,...)
// New updates:
//   - New default parameters specially tuned for BROBOT EVO 2 version (More agile, more stable...)
//   - New Move mode with position control (for externally programming the robot with a Blockly or pyhton programming interfaces)
//   - New telemtry packets send to TELEMETRY IP for monitoring Battery, Angle, ... (old battery packets for touch osc not working now)
//   - Default telemetry server is 192.168.4.2 (first client connected to the robot)
//  Get the free android app (jjrobots) from google play. For IOS users you need to use TouchOSC App + special template (info on jjrobots page)
//  Thanks to our users on the forum for the new ideas. Specially sasa999, KomX, ...

// The board needs at least 10-15 seconds with no motion (robot steady) at beginning to give good values... Robot move slightly when itÂ´s ready!
// MPU6050 IMU connected via I2C bus. Angle estimation using complementary filter (fusion between gyro and accel)
// Angle calculations and control part is running at 100Hz

// The robot is OFF when the angle is high (robot is horizontal). When you start raising the robot it
// automatically switch ON and start a RAISE UP procedure.
// You could RAISE UP the robot also with the robot arm servo (Servo button on the interface)
// To switch OFF the robot you could manually put the robot down on the floor (horizontal)

// We use a standard PID controllers (Proportional, Integral derivative controller) for robot stability
// More info on the project page: How it works page at jjrobots.com
// We have a PI controller for speed control and a PD controller for stability (robot angle)
// The output of the control (motors speed) is integrated so itÂ´s really an acceleration not an speed.

// We control the robot from a WIFI module using OSC standard UDP messages
// You need an OSC app to control de robot (Custom free JJRobots APP for android, and TouchOSC APP for IOS)
// Join the module Wifi Access Point (by default: JJROBOTS_XX) with your Smartphone/Tablet...
//   Wifi password: 87654321
// For TouchOSC users (IOS): Install the BROBOT layout into the OSC app (Touch OSC) and start play! (read the project page)
// OSC controls:
//    fader1: Throttle (0.0-1.0) OSC message: /1/fader1
//    fader2: Steering (0.0-1.0) OSC message: /1/fader2
//    push1: Move servo arm (and robot raiseup) OSC message /1/push1 
//    if you enable the touchMessage on TouchOSC options, controls return to center automatically when you lift your fingers
//    PRO mode (PRO button). On PRO mode steering and throttle are more aggressive
//    PAGE2: PID adjustements [optional][dont touch if you dont know what you are doing...;-) ]

#include <Wire.h>

// Uncomment this lines to connect to an external Wifi router (join an existing Wifi network)
//#define EXTERNAL_WIFI
//#define WIFI_SSID "YOUR_WIFI"
//#define WIFI_PASSWORD "YOUR_PASSWORD"
//#define WIFI_IP "192.168.1.101"  // Force ROBOT IP
//#define TELEMETRY "192.168.1.38" // Tlemetry server port 2223

#define TELEMETRY "192.168.4.2" // Default telemetry server (first client) port 2223

// NORMAL MODE PARAMETERS (MAXIMUN SETTINGS)
#define MAX_THROTTLE 550
#define MAX_STEERING 140
#define MAX_TARGET_ANGLE 14
  
// PRO MODE = MORE AGGRESSIVE (MAXIMUN SETTINGS)
#define MAX_THROTTLE_PRO 780   // Max recommended value: 860
#define MAX_STEERING_PRO 260   // Max recommended value: 280
#define MAX_TARGET_ANGLE_PRO 26   // Max recommended value: 32

// Default control terms for EVO 2
#define KP 0.36       
#define KD 0.075     
#define KP_THROTTLE 0.080 
#define KI_THROTTLE 0.1 
#define KP_POSITION 0.037 // 0.06  
#define KD_POSITION 0.45  
//#define KI_POSITION 0.02

// Control gains for raiseup (the raiseup movement requiere special control parameters)
#define KP_RAISEUP 0.1   
#define KD_RAISEUP 0.16   
#define KP_THROTTLE_RAISEUP 0   // No speed control on raiseup
#define KI_THROTTLE_RAISEUP 0.0

#define MAX_CONTROL_OUTPUT 150 // 500 old value - 
#define ITERM_MAX_ERROR 30   // Iterm windup constants for PI control 
#define ITERM_MAX 10000

#define ANGLE_OFFSET -1.65;  // Offset angle for balance (to compensate robot own weight distribution)

// Servo definitions
#define SERVO_AUX_NEUTRO 1500  // Servo neutral position
#define SERVO_MIN_PULSEWIDTH 700
#define SERVO_MAX_PULSEWIDTH 2300

// Telemetry
#define TELEMETRY_BATTERY 1
#define TELEMETRY_ANGLE 1
//#define TELEMETRY_DEBUG 1  // Dont use TELEMETRY_ANGLE and TELEMETRY_DEBUG at the same time!

#define ZERO_SPEED 65535
#define MAX_ACCEL 14      // Maximun motor acceleration (MAX RECOMMENDED VALUE: 20) (default:14)

#define MICROSTEPPING 16   // 8 or 16 for 1/8 or 1/16 driver microstepping (default:16)

#define DEBUG 0   // 0 = No debug info (default) DEBUG 1 for console output

// AUX definitions
#define CLR(x,y) (x&=(~(1<<y)))
#define SET(x,y) (x|=(1<<y))
#define RAD2GRAD 57.2957795
#define GRAD2RAD 0.01745329251994329576923690768489

String MAC;  // MAC address of Wifi module

uint8_t cascade_control_loop_counter = 0;
uint8_t loop_counter;       // To generate a medium loop 40Hz
uint8_t slow_loop_counter;  // slow loop 2Hz
uint8_t sendBattery_counter; // To send battery status
int16_t BatteryValue;

long timer_old;
long timer_value;
long timer_value_max;
long timer_value_test;
float debugVariable;
float dt;

// Angle of the robot (used for stability control)
float angle_adjusted;
float angle_adjusted_Old;
float angle_adjusted_filtered=0.0;

// Default control values from constant definitions
float Kp = KP;
float Kd = KD;
float Kp_thr = KP_THROTTLE;
float Ki_thr = KI_THROTTLE;
float Kp_user = KP;
float Kd_user = KD;
float Kp_thr_user = KP_THROTTLE;
float Ki_thr_user = KI_THROTTLE;
float Kp_position = KP_POSITION;
float Kd_position = KD_POSITION;
bool newControlParameters = false;
bool modifing_control_parameters = false;
int16_t position_error_sum_M1;
int16_t position_error_sum_M2;
float PID_errorSum;
float PID_errorOld = 0;
float PID_errorOld2 = 0;
float setPointOld = 0;
float target_angle;
int16_t throttle;
float steering;
float max_throttle = MAX_THROTTLE;
float max_steering = MAX_STEERING;
float max_target_angle = MAX_TARGET_ANGLE;
float control_output;
float angle_offset = ANGLE_OFFSET;

boolean positionControlMode = false;
uint8_t mode;  // mode = 0 Normal mode, mode = 1 Pro mode (More agressive)

int16_t motor1;
int16_t motor2;


// position control
volatile int32_t steps1;
volatile int32_t steps2;
int32_t target_steps1;
int32_t target_steps2;
int16_t motor1_control;
int16_t motor2_control;

int32_t step1help;
int32_t step2help;
int32_t step1delta;
int32_t step2delta;
int32_t step1last;
int32_t step2last;

float c_s1;
float c_s2;
float mv_vect1_x;
float mv_vect1_y;
float mv_alpha;
float imrad;
float imang;
float acc_vect1_x;  
float acc_vect1_y;  
float acc_alpha;
float sin_out;  
float sin_out_last;
float cos_out;  
float cos_out_last;

int16_t adistance0;
int16_t adistance1;
int txtoggle;
boolean stepper_on;

int16_t speed_M1, speed_M2;        // Actual speed of motors
int8_t  dir_M1, dir_M2;            // Actual direction of steppers motors
int16_t actual_robot_speed;        // overall robot speed (measured from steppers speed)
int16_t actual_robot_speed_Old;
float estimated_speed_filtered;    // Estimated robot speed

// OSC output variables
uint8_t OSCpage;
uint8_t OSCnewMessage;
float OSCfader[4];
float OSCxy1_x;
float OSCxy1_y;
float OSCxy2_x;
float OSCxy2_y;
uint8_t OSCpush[4];
uint8_t OSCtoggle[4];
uint8_t OSCmove_mode;
int16_t OSCmove_speed;
int16_t OSCmove_steps1;
int16_t OSCmove_steps2;
float OSCthrottle;
float OSCsteering;

#define SMIO_EN 12
#define SMIO_LEFT_STEP 11
#define SMIO_LEFT_DIR 10
#define SMIO_RIGHT_STEP 9
#define SMIO_RIGHT_DIR 8

#define SMIO_MS1 7
#define SMIO_MS2 6
#define SMIO_MS3 4

#define WHEEL_DIAM 0.09
#define WHEEL_CIRCUM (3.141592653*0.09)
#define WHEEL_BASE 0.18

float wheelbase;
uint16_t headpos;

// INITIALIZATION
void setup()
{
  // STEPPER PINS ON JJROBOTS BROBOT BRAIN BOARD
  pinMode(SMIO_EN, OUTPUT); // ENABLE MOTORS
  pinMode(SMIO_LEFT_STEP, OUTPUT); // STEP MOTOR 1 PORTE,6
  pinMode(SMIO_LEFT_DIR, OUTPUT); // DIR MOTOR 1  PORTB,4
  pinMode(SMIO_RIGHT_STEP, OUTPUT); // STEP MOTOR 2 PORTD,6
  pinMode(SMIO_RIGHT_DIR, OUTPUT); // DIR MOTOR 2  PORTC,6
  digitalWrite(SMIO_EN, HIGH);  // Disbale motors

  Serial.begin(57600); // Serial output to console

  pinMode(SMIO_MS1, OUTPUT); //
  pinMode(SMIO_MS2, OUTPUT); //
  pinMode(SMIO_MS3, OUTPUT); //
    
  // Initialize I2C bus (MPU6050 is connected via I2C)
  Wire.begin();

#if DEBUG > 0
  delay(5000);
#else
  delay(1000);
#endif
  Serial.println("JJROBOTS");
  delay(200);
  Serial.println("Don't move for 10 sec...");
  MPU6050_setup();  // setup MPU6050 IMU
  delay(500);

  SER_init();
  delay(100);

  // Calibrate gyros
  MPU6050_calibrate();
  
  BROBOT_initServo();
  BROBOT_moveServo(SERVO_AUX_NEUTRO);

  // STEPPER MOTORS INITIALIZATION
  Serial.println("Steppers init");
  // MOTOR1 => TIMER1
  TCCR1A = 0;                       // Timer1 CTC mode 4, OCxA,B outputs disconnected
  TCCR1B = (1 << WGM12) | (1 << CS11); // Prescaler=8, => 2Mhz
  OCR1A = ZERO_SPEED;               // Motor stopped
  dir_M1 = 0;
  TCNT1 = 0;

  // MOTOR2 => TIMER3
  TCCR3A = 0;                       // Timer3 CTC mode 4, OCxA,B outputs disconnected
  TCCR3B = (1 << WGM32) | (1 << CS31); // Prescaler=8, => 2Mhz
  OCR3A = ZERO_SPEED;   // Motor stopped
  dir_M2 = 0;
  TCNT3 = 0;
  delay(200);

  // Enable stepper drivers and TIMER interrupts
  digitalWrite(SMIO_EN, LOW);   // Enable stepper drivers
  stepper_on = false;
  
  // Enable TIMERs interrupts
  TIMSK1 |= (1 << OCIE1A); // Enable Timer1 interrupt
  TIMSK3 |= (1 << OCIE1A); // Enable Timer1 interrupt

  digitalWrite(SMIO_MS1, HIGH);
  digitalWrite(SMIO_MS2, HIGH);
  digitalWrite(SMIO_MS3, HIGH);
  
  // Little motor vibration and servo move to indicate that robot is ready
  for (uint8_t k = 0; k < 2; k++)
  {
    setMotorSpeedM1(5);
    setMotorSpeedM2(5);
    delay(1000);
    setMotorSpeedM1(-5);
    setMotorSpeedM2(-5);
    delay(1000);
  }
    setMotorSpeedM1(0);
    setMotorSpeedM2(0);

  Serial.println("BROBOT by JJROBOTS v2.82");
  Serial.println("Start...");
  timer_old = micros();
  
  step1help = 0;
  step2help = 0;
  step1delta = 0;
  step2delta = 0;
  step1last = 0;
  step2last = 0;
  
  c_s1 = 0.0;
  c_s2 = 0.0;
  mv_vect1_x = 0.0;
  mv_vect1_y = 0.0;
  mv_alpha = 0.0;
  imrad = 0.0;
  imang = 0.0;  
  
  acc_vect1_x = 0.0;  
  acc_vect1_y = 0.0;  
  acc_alpha = 0.0;
  
  sin_out_last =0.0;
  cos_out_last =1.0;
  
  txtoggle = 0;
  OSCthrottle = 0.0;
  OSCsteering = 0.0;
  
  wheelbase = WHEEL_BASE;
  headpos = SERVO_AUX_NEUTRO;  
}


// MAIN LOOP
void loop()
{

  if (SER_receive_data())
  {
      if (OSCmove_mode)
      {
        positionControlMode = true;
        OSCmove_mode = false;
        target_steps1 = steps1 + OSCmove_steps1;
        target_steps2 = steps2 + OSCmove_steps2;

      }
      else
      {
        positionControlMode = false;
        throttle = OSCthrottle * max_throttle;
        
        // We add some exponential on steering to smooth the center band
        steering = OSCsteering;
        if (steering > 0)
          steering = (steering * steering + 0.5 * steering) * max_steering;
        else
          steering = (-steering * steering + 0.5 * steering) * max_steering;
          
      }
  }
  
  timer_value = micros();

  // New IMU data?
  if (MPU6050_newData())
  {
    MPU6050_read_3axis();
    loop_counter++;
    slow_loop_counter++;
    dt = (timer_value - timer_old) * 0.000001; // dt in seconds
    timer_old = timer_value;

    angle_adjusted_Old = angle_adjusted;
    // Get new orientation angle from IMU (MPU6050)
    float MPU_sensor_angle = MPU6050_getAngle(dt);
    angle_adjusted = MPU_sensor_angle + angle_offset;
    if ((MPU_sensor_angle>-15)&&(MPU_sensor_angle<15))
      angle_adjusted_filtered = angle_adjusted_filtered*0.99 + MPU_sensor_angle*0.01;

    //Serial.print("\t");

    // We calculate the estimated robot speed:
    // Estimated_Speed = angular_velocity_of_stepper_motors(combined) - angular_velocity_of_robot(angle measured by IMU)
    actual_robot_speed = (speed_M1 + speed_M2) / 2; // Positive: forward  

    int16_t angular_velocity = (angle_adjusted - angle_adjusted_Old) * 25.0; // 25 is an empirical extracted factor to adjust for real units
    int16_t estimated_speed = -actual_robot_speed + angular_velocity;
    estimated_speed_filtered = estimated_speed_filtered * 0.9 + (float)estimated_speed * 0.1; // low pass filter on estimated speed

#if DEBUG==2
    Serial.print(angle_adjusted);
    Serial.print(" ");
    Serial.println(estimated_speed_filtered);
#endif

    if (positionControlMode)
    {
      // POSITION CONTROL. INPUT: Target steps for each motor. Output: motors speed
      motor1_control = positionPDControl(steps1, target_steps1, Kp_position, Kd_position, speed_M1);
      motor2_control = positionPDControl(steps2, target_steps2, Kp_position, Kd_position, speed_M2);

      // Convert from motor position control to throttle / steering commands
      throttle = (motor1_control + motor2_control) / 2;
      throttle = constrain(throttle, -190, 190);
      steering = motor2_control - motor1_control;
      steering = constrain(steering, -50, 50);
    }

    // ROBOT SPEED CONTROL: This is a PI controller.
    //    input:user throttle(robot speed), variable: estimated robot speed, output: target robot angle to get the desired speed
    target_angle = speedPIControl(dt, estimated_speed_filtered, throttle, Kp_thr, Ki_thr);
    target_angle = constrain(target_angle, -max_target_angle, max_target_angle); // limited output


#if DEBUG==3
    Serial.print(angle_adjusted);
    Serial.print(" ");
    Serial.print(estimated_speed_filtered);
    Serial.print(" ");
    Serial.println(target_angle);
#endif

    // Stability control (100Hz loop): This is a PD controller.
    //    input: robot target angle(from SPEED CONTROL), variable: robot angle, output: Motor speed
    //    We integrate the output (sumatory), so the output is really the motor acceleration, not motor speed.
    control_output += stabilityPDControl(dt, angle_adjusted, target_angle, Kp, Kd);
    control_output = constrain(control_output, -MAX_CONTROL_OUTPUT, MAX_CONTROL_OUTPUT); // Limit max output from control

    // The steering part from the user is injected directly to the output
    motor1 = control_output + steering;
    motor2 = control_output - steering;

    // Limit max speed (control output)
    motor1 = constrain(motor1, -MAX_CONTROL_OUTPUT, MAX_CONTROL_OUTPUT);
    motor2 = constrain(motor2, -MAX_CONTROL_OUTPUT, MAX_CONTROL_OUTPUT);

    int angle_ready;
//    if (OSCpush[0])     // If we press the SERVO button we start to move
//      angle_ready = 82;
//    else
    angle_ready = 35;  // Default angle
      
    if ((angle_adjusted < angle_ready) && (angle_adjusted > -angle_ready) && stepper_on) // Is robot ready (upright?)
    {
      // NORMAL MODE
      digitalWrite(SMIO_EN, LOW);  // Motors enable
      // NOW we send the commands to the motors
      setMotorSpeedM1(motor1);
      setMotorSpeedM2(motor2);
    }
    else   // Robot not ready (flat), angle > angle_ready => ROBOT OFF
    {
      digitalWrite(SMIO_EN, HIGH);  // Disable motors
      setMotorSpeedM1(0);
      setMotorSpeedM2(0);
      PID_errorSum = 0;  // Reset PID I term
      Kp = KP_RAISEUP;   // CONTROL GAINS FOR RAISE UP
      Kd = KD_RAISEUP;
      Kp_thr = KP_THROTTLE_RAISEUP;
      Ki_thr = KI_THROTTLE_RAISEUP;
      // RESET steps
      steps1 = 0;
      steps2 = 0;
      positionControlMode = false;
      OSCmove_mode = false;
      throttle = 0;
      steering = 0;
    }


    // Normal condition?
    if ((angle_adjusted < 56) && (angle_adjusted > -56))
    {
      Kp = Kp_user;            // Default user control gains
      Kd = Kd_user;
      Kp_thr = Kp_thr_user;
      Ki_thr = Ki_thr_user;
    }
    else    // We are in the raise up procedure => we use special control parameters
    {
      Kp = KP_RAISEUP;         // CONTROL GAINS FOR RAISE UP
      Kd = KD_RAISEUP;
      Kp_thr = KP_THROTTLE_RAISEUP;
      Ki_thr = KI_THROTTLE_RAISEUP;
    }
    
    // calculate the position went during last cycles
    // read the current number of steps
    // keep the steps moved up to now together - even it should not make any difference so ...
    noInterrupts();
    step1help = steps1;
    step2help = steps2;
    interrupts();
    step1delta = step1help - step1last;
    step2delta = step2help - step2last;
    step1last = step1help;
    step2last = step2help;

    c_s1 = WHEEL_CIRCUM * (float)step1delta / (((float)MICROSTEPPING) * 200.0);  // distance in m
    c_s2 = WHEEL_CIRCUM * (float)step2delta / (((float)MICROSTEPPING) * 200.0);  // distance in m
    
    if (step1delta==step2delta)
    {
        mv_vect1_x = c_s1;
        mv_vect1_y = 0.0;
        imang      = 0.0;
        sin_out    = 0.0;
        cos_out    = 1.0;
    }
    else
    {
        imrad = c_s2 * wheelbase / (c_s1 - c_s2) + 0.5 * wheelbase; // fictive radius (r+1/2c)
        imang = (c_s1 - c_s2) / wheelbase;                          // fictive angle in radian 
        
        mv_vect1_x = imrad * sin(imang);
        mv_vect1_y = imrad * (1.0-cos(imang));
        mv_alpha   = imang;
    }
    //accumulate vector
    // rotating matrix with the accumulated alpha
    // /x'\    / cos a   -sin a \   /x\
    // |  | =  |                | * | |
    // \y'/    \ sin a   cos a  /   \y/
    
    sin_out = sin(acc_alpha);
    cos_out = cos(acc_alpha);  
    
    acc_vect1_x = acc_vect1_x + mv_vect1_x * cos_out - mv_vect1_y * sin_out_last;
    acc_vect1_y = acc_vect1_y + mv_vect1_x * sin_out + mv_vect1_y * cos_out_last;
    acc_alpha   = acc_alpha + imang;
    
    adistance0 = analogRead(0);
    adistance1 = analogRead(1);
    
  } // End of new IMU data

  // Medium loop x Hz
  if (loop_counter >= 10)
  {
    loop_counter = 0;
  
    if (txtoggle == 0)
    {
      SER_sent_data(1, *(((int16_t*)&acc_vect1_x) + 1), *((int16_t*)&acc_vect1_x));
      SER_sent_data(2, *(((int16_t*)&acc_vect1_y) + 1), *((int16_t*)&acc_vect1_y));
      SER_sent_data(3, *(((int16_t*)&acc_alpha) + 1), *((int16_t*)&acc_alpha));
    }
    else
    {
      SER_sent_data(4, *(((int16_t*)&angle_adjusted) + 1), *((int16_t*)&angle_adjusted));
      SER_sent_data(5, adistance0, headpos);
      SER_sent_data(6, actual_robot_speed, adistance1);
    }    

    txtoggle++;
    if (txtoggle>1) txtoggle=0;

//    Serial.println(step1delta);
//    Serial.println(step2delta);
//    Serial.print("adistance");
//    Serial.println(adistance);

    // Telemetry here?
#if TELEMETRY_ANGLE==1
    char auxS[25];
    int ang_out = constrain(int(angle_adjusted * 10),-900,900);
    sprintf(auxS, "$tA,%+04d", ang_out);
    Serial1.println(auxS);
#endif
#if TELEMETRY_DEBUG==1
    char auxS[50];
    sprintf(auxS, "$tD,%d,%d,%ld", int(angle_adjusted * 10), int(estimated_speed_filtered), steps1);
    Serial1.println(auxS);
#endif
  } // End of medium loop

  if (slow_loop_counter >= 50) // 2Hz
  {
    slow_loop_counter = 0;
    // Read  status
#if DEBUG==1
    Serial.print(dt * 1000);
    Serial.print(" ");
    Serial.print(angle_offset);
    Serial.print(" ");
    Serial.print(angle_adjusted);
    Serial.print(",");
    Serial.println(angle_adjusted_filtered);
#endif
  }  // End of slow loop
  
  timer_value_test = micros()-timer_value;
  if (timer_value_test > timer_value_max)
  {
     Serial.print("max_timer value:");
     Serial.println(timer_value_test);
     timer_value_max = timer_value_test;
  }
  
}


