#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/objdetect.hpp"
#include "opencv2/imgcodecs.hpp"
#include <opencv2/dnn/dnn.hpp>

#include <opencv2/core/utility.hpp>
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <string>
#include <math.h>

#include <dirent.h>

#include <X11/Xlib.h>

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <pthread.h>

#include <sstream>

#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
            ( std::ostringstream() << std::dec << x ) ).str()

using namespace cv;
using namespace std;
using namespace cv::dnn;

#define PI 3.14159

// Arduino communication
extern "C" void* serialReadT(void *arg);
extern "C" void* keythread(void *arg);
extern "C" void serialWrite(int cmdid, short v1, short v2);
extern "C" void serialWriteT2(int pos,void *arg);

float acc_vect1_x;
float acc_vect1_y; 
float acc_angle;
float angle_z;
short adist0;
short adist1;
unsigned short headpos;
short actual_motor_speed;


// Initialize the parameters
float confThreshold = 0.5; // Confidence threshold
float nmsThreshold = 0.45; // Non-maximum suppression threshold
int inpWidth = 416;        // Width of network's input image
int inpHeight = 416;       // Height of network's input image
Net net;
Mat blob,dispPicDNN;
bool imageDNNinProgress;

typedef struct{
    float confidence;
    float distance;
    float angle;
} obj_spec_t;

obj_spec_t eggtree[2];

pthread_t tid0;
pthread_t tid1;
pthread_t tid2;

pthread_mutex_t condition_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t  condition_cond  = PTHREAD_COND_INITIALIZER;

pthread_mutex_t imageDNN_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t  imageDNN_cond  = PTHREAD_COND_INITIALIZER;

void drawText(Mat & image, int i, double fscale);

// Get the names of the output layers
vector<String> getOutputsNames(const Net& net)
{
    static vector<String> names;
    if (names.empty())
    {
        //Get the indices of the output layers, i.e. the layers with unconnected outputs
        vector<int> outLayers = net.getUnconnectedOutLayers();
         
        //get the names of all the layers in the network
        vector<String> layersNames = net.getLayerNames();
         
        // Get the names of the output layers in names
        names.resize(outLayers.size());
        for (size_t i = 0; i < outLayers.size(); ++i)
        names[i] = layersNames[outLayers[i] - 1];
    }
    return names;
}

// Draw the predicted bounding box
void drawPred(int classId, float conf, int left, int top, int right, int bottom, Mat& frame)
{
    //Draw a rectangle displaying the bounding box
    rectangle(frame, Point(left, top), Point(right, bottom), Scalar(0, 0, 255));
     
    //Get the label for the class name and its confidence
    string label = format("%.2f", conf);
    label = to_string(classId) + ":" + label;
     
    //Display the label at the top of the bounding box
    int baseLine;
    Size labelSize = getTextSize(label, FONT_HERSHEY_SIMPLEX, 0.5, 1, &baseLine);
    top = max(top, labelSize.height);
    putText(frame, label, Point(left, top), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(255,255,255));
}

// Remove the bounding boxes with low confidence using non-maxima suppression
void postprocess(Mat& frame, const vector<Mat>& outs)
{
    vector<int> classIds;
    vector<float> confidences;
    vector<Rect> boxes;
     
    for (size_t i = 0; i < outs.size(); ++i)
    {
        // Scan through all the bounding boxes output from the network and keep only the
        // ones with high confidence scores. Assign the box's class label as the class
        // with the highest score for the box.
        float* data = (float*)outs[i].data;
        for (int j = 0; j < outs[i].rows; ++j, data += outs[i].cols)
        {
            Mat scores = outs[i].row(j).colRange(5, outs[i].cols);
            Point classIdPoint;
            double confidence;
            // Get the value and location of the maximum score
            minMaxLoc(scores, 0, &confidence, 0, &classIdPoint);
            if (confidence > confThreshold)
            {
                int left = (int)(data[0] * frame.cols);
                int top = (int)(data[1] * frame.rows);
                int width = (int)(data[2] * frame.cols);
                int height = (int)(data[3] * frame.rows);
               
                cout << data[0]+data[2]*0.5 << " " << data[1]+data[3]*0.5 << " " << data[3] << " " << data[3]/data[2] << " " << endl;                
                 
                float tanp = tan(PI/180.0 * (22.5 + 5.0));
                float tanm = tan(PI/180.0 * (22.5 - 5.0));
                float cos2 = cos(PI/180.0 * 5.0) * cos(PI/180.0 * 5.0);
                float y = data[1]+data[3];
                float x = data[0]+0.5*data[2];
                
                float distance1 = 20.0 *(1.0-y) * (1.0+tanm/tanp) / (y*tanp*cos2 - (1.0-y)*tanm);
                float distance2 = 6.0 / tan(data[3]/0.5*22.5 * PI/180.0);
                float angle = 180.0/PI * atan((x-0.5)*tan(28.8 * PI/180.0));
                
                cout << "Dist1: " << distance1 + 38.0 << endl;
                cout << "Dist2: " << distance2 << endl;
                cout << "Angle: " << angle << endl;
                cout << "classId: " << classIdPoint.x << endl;
                
                if (eggtree[classIdPoint.x].confidence < confidence) {
                    eggtree[classIdPoint.x].confidence = confidence;
                    eggtree[classIdPoint.x].distance = distance1;
                    eggtree[classIdPoint.x].angle = angle;
                }
                 
                classIds.push_back(classIdPoint.x);
                confidences.push_back((float)confidence);
                boxes.push_back(Rect(left, top, width, height));
            }
        }
    }
     
    // Perform non maximum suppression to eliminate redundant overlapping boxes with
    // lower confidences
    vector<int> indices;
    NMSBoxes(boxes, confidences, confThreshold, nmsThreshold, indices);
    for (size_t i = 0; i < indices.size(); ++i)
    {
        int idx = indices[i];
        Rect box = boxes[idx];
        drawPred(classIds[idx], confidences[idx], box.x, box.y,
                 box.x + box.width, box.y + box.height, frame);
    }
}

void *imageProcT(void *arg)
{

    while (1)
    {
        pthread_mutex_lock( &imageDNN_mutex );
        pthread_cond_wait( &imageDNN_cond, &imageDNN_mutex );
        pthread_mutex_unlock( &imageDNN_mutex );

        cout << "Start DNN" << endl;

        net.setInput(blob);
        // Runs the forward pass to get output of the output layers
        vector<Mat> outs;
        net.forward(outs, getOutputsNames(net));
        postprocess(dispPicDNN, outs);
     
        cout << "End DNN" << endl;
        imageDNNinProgress = false;        
    }
}

int main()
{
    int err;
    //int explosureCtrl = 10;
    int headcmd = 1500;

    Mat image, dispPic, dispPicSmall;
    VideoCapture capture;
    capture.open(0);
    cout << "Capture is opened" << endl;
    capture.set(CAP_PROP_FPS,2);
    capture.set(CAP_PROP_FRAME_WIDTH,640);
    capture.set(CAP_PROP_FRAME_HEIGHT,480);
    //capture.set(CAP_PROP_ZOOM,-2);
    
    usleep(1000);
    
    cout << "FPS " << capture.get(CAP_PROP_FPS) << endl;
    namedWindow("Capture",WINDOW_AUTOSIZE);
    namedWindow("ShowDNN",WINDOW_AUTOSIZE);
    //createTrackbar( " Expose: ", "Capture", &explosureCtrl, 1000, NULL );
 
    Rect roi;
    int i=0;
    bool zflag = false;
    bool trackFlag = false;    
    bool headmoveFlag = false;
    string snum,fname;
    
    string modelConfiguration = "yolodata/eggtree-yolov3-tiny.cfg";
    string modelWeights = "yolodata/eggtree-yolov3-tiny_8000.weights";

    net = readNetFromDarknet(modelConfiguration, modelWeights);
    net.setPreferableBackend(DNN_BACKEND_OPENCV);
    net.setPreferableTarget(DNN_TARGET_CPU);

    cout << "DNN ready" << endl;
    
    err = pthread_create(&tid0, NULL, &imageProcT, NULL);
    if (err != 0){
      printf("\ncan't create thread 1:[%s]", strerror(err));
      return 1 ;
    }

    err = pthread_create(&tid1, NULL, &keythread, NULL);
    if (err != 0){
      printf("\ncan't create thread 1:[%s]", strerror(err));
      return 1 ;
    }

    err = pthread_create(&tid2, NULL, &serialReadT, NULL);
    if (err != 0){
      printf("\ncan't create thread 2:[%s]", strerror(err));
      return 1 ;
    }
     
    imageDNNinProgress = false; 
            
    if(capture.isOpened())
    {
        capture >> dispPicDNN;
        
        for(;;)
        {

            i++;
            capture >> image;

            struct timeval tv;
            gettimeofday(&tv, NULL); 
            // printf("Time: %ld.%ld \n", tv.tv_sec%100, tv.tv_usec); 
                        
            roi.x = (image.size().width - 640)/2;
            roi.y = (image.size().height - 480)/2;
            roi.width = 640;
            roi.height = 480;
            
            dispPic = image(roi).clone();

            // capture.set(CAP_PROP_EXPOSURE,-((float)explosureCtrl)/10.0);

            if (!imageDNNinProgress) {                
                

                imshow("ShowDNN", dispPicDNN);
                
                if ((eggtree[0].confidence > 0) && (trackFlag) && (!headmoveFlag)) {
                    
                    headcmd = headcmd - (int)(eggtree[0].angle *2.0 / 90.0 * 800.0);
                    
                    if ((headcmd > 1200) and (headcmd < 1800)){
                        serialWrite(2, headcmd, 0);      
                    } else {
                        cout << "Invalid headcmd: " << headcmd << endl;
                    }
                    headmoveFlag = true;
                } else {headmoveFlag = false;}
   
                if (!headmoveFlag) {
                pthread_mutex_lock( &imageDNN_mutex );
                // Create a 4D blob from a frame.
                dispPicDNN = dispPic.clone();
                blobFromImage(dispPicDNN, blob, 1/255.0, Size(inpWidth, inpHeight), Scalar(0,0,0), true, false);
                imageDNNinProgress = true;
                
                eggtree[0].confidence = 0.0;
                eggtree[1].confidence = 0.0;
                                
                pthread_cond_signal( &imageDNN_cond );            
                pthread_mutex_unlock( &imageDNN_mutex );                           
                }
/*              cout << "Start DNN" << endl;

                //Sets the input to the network
                net.setInput(blob);
                // Runs the forward pass to get output of the output layers
                vector<Mat> outs;
                net.forward(outs, getOutputsNames(net));
                // Remove the bounding boxes with low confidence
                postprocess(dispPic, outs);

                cout << "End DNN" << endl;
                drawText(dispPic, i, 1.0);
                imshow("Capture", dispPic);
*/
            } 
 
            if (i%2) {                
                drawText(dispPic, i, 1.0);
                resize(dispPic, dispPicSmall, cv::Size(dispPic.cols*0.5,dispPic.rows*0.5), 0, 0, INTER_LINEAR);
                imshow("Capture", dispPicSmall);
            }
            
            int key = cv::waitKey(5);
            switch (key)
            {

             case 122: //"z" zoom
                
                if (zflag) {
                    zflag = false;
                    capture.set(CAP_PROP_FRAME_WIDTH,640);
                    capture.set(CAP_PROP_FRAME_HEIGHT,480);
                } else {
                    // 1296x972 1280x1024 1920x1080	
                    capture.set(CAP_PROP_FRAME_WIDTH,1920);
                    capture.set(CAP_PROP_FRAME_HEIGHT,1080);
                    zflag = true;
                }
             
              cout << "switch zoom " << endl;
                break;

            case 104: //"h" move heade
                headcmd -= 50;
                cout << "Move Head " << headcmd << endl;
                if ((headcmd > 1200) and (headcmd < 1800)){
                    serialWrite(2, headcmd, 0);      
                } else {
                    cout << "Invalid headcmd: " << headcmd << endl;
                }
              break; 

            case 106: //"j" move heade
              headcmd = 1500;
              cout << "Center Head " << headcmd << endl;
              serialWrite(2, headcmd, 0);      
              break; 

            case 116: //"t" move heade
                trackFlag = !trackFlag;
                cout << "trackFlag " << trackFlag << endl;
              break; 
 
            case 120: //"x" exit
              cout << "exit " << endl;
              return 0;
      			  break;

            default:
              break;
            }

        }
    }
    return 0;
}

void drawText(Mat & image, int i, double fscale)
{
	std::string s1 = SSTR( i );
	
    putText(image, "Frame No: " + s1,
            Point(20, 50),
            FONT_HERSHEY_COMPLEX, fscale, // font face and scale
            Scalar(255, 255, 255), // white
            1, LINE_AA); // line thickness and type
}






