#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/objdetect.hpp"
#include "opencv2/imgcodecs.hpp"

#include <opencv2/core/utility.hpp>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <unistd.h>
#include <string>

#include <dirent.h>

#include <X11/Xlib.h>

#include <errno.h>
#include <stdio.h>
#include <string.h>

#include <sstream>

#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
            ( std::ostringstream() << std::dec << x ) ).str()

using namespace cv;
using namespace std;

Mat croppedImage;
Mat image;
float zoomfactor      = 1.0;
float zoomfactor_prev = 1.0;
float offs_x_prev = 0.0;
float offs_y_prev = 0.0;
unsigned int mx = 0;
unsigned int my = 0;
float org_x = 0.0;
float org_y = 0.0;
int line_cnt = 0;
unsigned int edgevalue[4];
Point corners[2];

static void onMouse( int event, int x, int y, int flags, void* )
{
    Mat frame;

    if( event == EVENT_LBUTTONDOWN )
    {
 //       cout << "b down" << endl;
 //       cout << "tx: " << x << "ty: " << y << endl;
       
        
        float xreal = offs_x_prev + ((float)x / zoomfactor_prev); 
        float yreal = offs_y_prev + ((float)y / zoomfactor_prev); 
        
        switch (line_cnt)
        {
        case 0:
        case 2:
              edgevalue[line_cnt] = yreal;

 //             cout << "yreal: " << yreal << endl;
              
              line(croppedImage, 
                  Point(0,y),
                  Point(640,y), 
                  Scalar( 255, 255, 255 ), 1, 8, 0 );
          break;
        case 1:
        case 3:
              edgevalue[line_cnt] = xreal;
              
              line(croppedImage, 
                  Point(x,0),
                  Point(x,480), 
                  Scalar( 255, 255, 255 ), 1, 8, 0 );
          break;

        default:
          break;
        }

 //       cout << line_cnt << ": " << edgevalue[line_cnt] << endl;
        
        line_cnt++;

        if (line_cnt == 4) {
          
          corners[0] = Point(edgevalue[3],edgevalue[0]);
          corners[1] = Point(edgevalue[1],edgevalue[2]);
        }

        imshow("FrameView", croppedImage);
           
    }
    
    if( event == EVENT_MOUSEWHEEL )
    {
 //       cout << getMouseWheelDelta(flags) << endl;;

        if (getMouseWheelDelta(flags)>0) {
          zoomfactor = zoomfactor * 1.1;
        } else {
          zoomfactor = zoomfactor / 1.1;
          if (zoomfactor < 1.0) {
            zoomfactor = 1.0;
          } 
        }
 //       cout << "factor: " << zoomfactor << endl;;
 //       cout << "x: " << mx << "y: " << my << endl;    

        resize(image, frame, cv::Size(), zoomfactor, zoomfactor, INTER_LINEAR);

        float newmid_x = offs_x_prev + ((float)mx / zoomfactor_prev);
        float newmid_y = offs_y_prev + ((float)my / zoomfactor_prev);
       
        float x1 = (newmid_x * zoomfactor) - (org_x * 0.5);
        float y1 = (newmid_y * zoomfactor) - (org_y * 0.5);

        if (x1<0.0) {x1 = 0.0;}
        if ((x1+org_x) > (org_x*zoomfactor)) {x1=org_x*(zoomfactor-1.0);}
        
        if (y1<0.0) {y1 = 0.0;}
        if ((y1+org_y) > (org_y*zoomfactor)) {y1=org_y*(zoomfactor-1.0);}

        offs_x_prev = x1 / zoomfactor;  
        offs_y_prev = y1 / zoomfactor;
        zoomfactor_prev = zoomfactor;

        Rect myROI((unsigned int)x1, (unsigned int)y1, (unsigned int)org_x, (unsigned int)org_y);
        croppedImage = frame(myROI);

      	std::string s1 = SSTR( zoomfactor );
      	
          putText(croppedImage, "Factor  " + s1,
                  Point(20, 50),
                  FONT_HERSHEY_COMPLEX, 0.5, // font face and scale
                  Scalar(255, 255, 255), // white
                  1, LINE_AA); // line thickness and type

        imshow("FrameView", croppedImage);
        line_cnt =0;        
    }

    if( event == EVENT_MOUSEMOVE )
    {
        mx = x; 
        my = y;
        
    }
  
    
    if( event == EVENT_LBUTTONUP )
    {

//      line(imgroom, 
//          Point(acc_vect1_x*gf + 300,acc_vect1_y*gf + 300),
//          Point(acc_vect1_x_old*gf + 300,acc_vect1_y_old*gf + 300), 
//          Scalar( 0, 0, 255 ), 1, 8, 0 );

        if (line_cnt==4){
            croppedImage = image.clone();
            rectangle(croppedImage, corners[0], corners[1], Scalar( 255, 255, 255 ), 1, 8, 0 );  
            imshow("FrameView", croppedImage);
                    
        }
    }
    //cout << "Event: " << event << endl;    
}


int main()
{
 

    struct dirent *entry;
    DIR *Dir;
    string filename;

    namedWindow("FrameView",WINDOW_AUTOSIZE);
    setMouseCallback( "FrameView", onMouse, 0 );
    
    Dir = opendir("img/");

    while(entry = readdir(Dir)) {
        filename.assign(entry->d_name);
        if (filename.substr(1,1) == "0") {   
            cout << "Image1 " << filename << endl;
            break;
        } else {
            cout << "No Image " << filename << endl;
        }
    }
   
    //string filename = "picture.bmp";
    
    image = imread("img/"+filename, IMREAD_UNCHANGED);
    croppedImage = image.clone();
    
 //   cout << image.size() << endl;
 //   cout << image.cols << endl;

    namedWindow("FrameView",WINDOW_AUTOSIZE);
    
    imshow("FrameView", image);

    org_x = image.cols;
    org_y = image.rows;


    int i=0;   
       for(;;)
        {
			
     		i++;
	
            int key = cv::waitKey(100);
 
            switch (key)
            {
            case 99: //"c"
              cout << "clear image " << key << endl;
      			  break;

            case 53: //"5" ; "6"
            case 54:
                  if (line_cnt >= 4)
                  {
                       cout << "Append Label:" << filename.substr(0,6) << ".txt" << endl; 
                       cout << to_string(key-53) << " ";
                       cout << corners[0].x/org_x << " ";
                       cout << corners[0].y/org_y << " ";
                       cout << (corners[1].x-corners[0].x) /org_x << " ";
                       cout << (corners[1].y-corners[0].y) /org_y << endl;
           
                       string wfile = "label/"+filename.substr(0,6)+"_pic.txt";
                       std::ofstream out(wfile,std::ios_base::app);
                       out << to_string(key-53) << " ";
                       out << corners[0].x/org_x << " ";
                       out << corners[0].y/org_y << " ";
                       out << (corners[1].x-corners[0].x) /org_x << " ";
                       out << (corners[1].y-corners[0].y) /org_y << endl;
                       out.close();
                  }
                  break;
    
            case 49: //"1" ; "2"
            case 50:
                  if (line_cnt >= 4)
                  {
                       cout << "Label:" << filename.substr(0,6) << ".txt" << endl; 
                       cout << to_string(key-49) << " ";
                       cout << corners[0].x/org_x << " ";
                       cout << corners[0].y/org_y << " ";
                       cout << (corners[1].x-corners[0].x) /org_x << " ";
                       cout << (corners[1].y-corners[0].y) /org_y << endl;
           
                       string wfile = "label/"+filename.substr(0,6)+"_pic.txt";
                       std::ofstream out(wfile);
                       out << to_string(key-49) << " ";
                       out << corners[0].x/org_x << " ";
                       out << corners[0].y/org_y << " ";
                       out << (corners[1].x-corners[0].x) /org_x << " ";
                       out << (corners[1].y-corners[0].y) /org_y << endl;
                       out.close();
                        
                  } else break;

            case 110: //"n"
//                cout << "skip Image " << filename << endl;
                while(entry = readdir(Dir)) {
                    filename.assign(entry->d_name);
                    if (filename.substr(1,1) == "0") {   
 //                       cout << "Image " << filename << endl;
                        break;
                    } else {
 //                       cout << "No Image " << filename << endl;
                    }
                }
                if (!entry) {
                  cout << "exit - no file" << key << endl;
                  closedir(Dir);
                  return 0;
                }

                image = imread("img/"+filename, IMREAD_UNCHANGED);
                croppedImage = image.clone();

                imshow("FrameView", image);

                org_x = image.cols;
                org_y = image.rows;
                line_cnt = 0;
                zoomfactor      = 1.0;
                zoomfactor_prev = 1.0;
                    
                break;

            case 120: //"0"
                cout << "exit " << key << endl;
                closedir(Dir);
                return 0;
                break;


            default:
                break;
            }
        }

    closedir(Dir);
    return 0;
}

void drawText(Mat & image, int i, double fscale)
{
	std::string s1 = SSTR( i );
	
    putText(image, "Hello OpenCV " + s1,
            Point(20, 50),
            FONT_HERSHEY_COMPLEX, fscale, // font face and scale
            Scalar(255, 255, 255), // white
            1, LINE_AA); // line thickness and type
}






