#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/objdetect.hpp"
#include "opencv2/imgcodecs.hpp"

#include <opencv2/core/utility.hpp>
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <string>

#include <dirent.h>

#include <X11/Xlib.h>

#include <errno.h>
#include <stdio.h>
#include <string.h>

#include <sstream>

#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
            ( std::ostringstream() << std::dec << x ) ).str()

using namespace cv;
using namespace std;

void drawText(Mat & image, int i, double fscale);

int main()
{
    struct dirent *entry;
    DIR *Dir;
    string s;
    
    int curCount, maxCount = 0;
    Dir = opendir("img/");
    while((entry = readdir(Dir))) {
        s.assign(entry->d_name);        
        istringstream(s) >> curCount;
        cout << s << ": " << curCount << endl;
        if (curCount>maxCount) maxCount = curCount;
    }
    closedir(Dir);
    maxCount++;

    Mat image, dispPic;
    VideoCapture capture;
    capture.open(0);
    cout << "Capture is opened" << endl;
    capture.set(CAP_PROP_FPS,5);
    capture.set(CAP_PROP_FRAME_WIDTH,640);
    capture.set(CAP_PROP_FRAME_HEIGHT,480);
    //capture.set(CAP_PROP_ZOOM,-2);
    
    usleep(1000);
    capture.set(CAP_PROP_EXPOSURE,1.0);
    
    cout << "FPS " << capture.get(CAP_PROP_FPS) << endl;
    cout << "EXPOSURE " << capture.get(CAP_PROP_EXPOSURE) << endl;
    namedWindow("Capture",WINDOW_AUTOSIZE);

    vector<int> qualityType;
    qualityType.push_back(IMWRITE_JPEG_QUALITY);
    qualityType.push_back(90);

    Rect roi;
    int i=0;
    bool zflag = false;
    string snum,fname;
    
    if(capture.isOpened())
    {
        for(;;)
        {

            i++;
            capture >> image;
            
            roi.x = (image.size().width - 640)/2;
            roi.y = (image.size().height - 480)/2;
            roi.width = 640;
            roi.height = 480;
            
            dispPic = image(roi).clone();
            drawText(dispPic, i, 1.0);
            
            if (i%5 == 0) {                
                imshow("Capture", dispPic);
            }
            
            int key = cv::waitKey(5);
            switch (key)
            {
            case 99: //"c" capture frame
            
                snum = to_string(maxCount);
                fname = "";
                fname.append("000000",6-snum.length());
                fname = "img/" + fname + snum + "_pic.jpg";
                
                cout << "capture frame " << fname << " " << image.size() << endl;

                imwrite(fname, image,qualityType);

                maxCount++;
                            
      			  break;

             case 122: //"z" zoom
                
                if (zflag) {
                    zflag = false;
                    capture.set(CAP_PROP_FRAME_WIDTH,640);
                    capture.set(CAP_PROP_FRAME_HEIGHT,480);
                } else {
                    // 1296x972 1280x1024 1920x1080	
                    capture.set(CAP_PROP_FRAME_WIDTH,1920);
                    capture.set(CAP_PROP_FRAME_HEIGHT,1080);
                    zflag = true;
                }
             
              cout << "switch zoom " << endl;
                break;

 
            case 120: //"x" exit
              cout << "exit " << endl;
              return 0;
      			  break;

            default:
              break;
            }

        }
    }
    return 0;
}

void drawText(Mat & image, int i, double fscale)
{
	std::string s1 = SSTR( i );
	
    putText(image, "Frame No: " + s1,
            Point(20, 50),
            FONT_HERSHEY_COMPLEX, fscale, // font face and scale
            Scalar(255, 255, 255), // white
            1, LINE_AA); // line thickness and type
}






