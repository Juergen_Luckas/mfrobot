/*
 * serialTest.c:
 *	Very simple program to test the serial port. Expects
 *	the port to be looped back to itself
 *
 * Copyright (c) 2012-2013 Gordon Henderson. <projects@drogon.net>
 ***********************************************************************
 * This file is part of wiringPi:
 *	https://projects.drogon.net/raspberry-pi/wiringpi/
 *
 *    wiringPi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    wiringPi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with wiringPi.  If not, see <http://www.gnu.org/licenses/>.
 ***********************************************************************
 */

// Protocol
// The data are transferred in chars - each message has 10 chars.
// Startflag         "\"
// Msg ID            0-255
// MsgValue[0..7]    4 Byte Hex-values, each nibble (4Bit) is transmitted as char in sequence, with nibble-value plus 0x30
//                   Nibble x0 ->  0+0x30 = "0"
//                   Nibble x1 ->  1+0x30 = "1"
//                   Nibble xA -> 14+0x30 = ":"
//                   Nibble xB -> 14+0x30 = ";"
//                   Nibble xC -> 14+0x30 = "<"
//                   Nibble xD -> 14+0x30 = "="
//                   Nibble xE -> 14+0x30 = ">"
//                   Nibble xF -> 15+0x30 = "?"
//                   0123456789ABCDEF
//                   0123456789:;<=>?
//
// Data  Raspberry --> Arduino
//  1: \1xxxxyyyy - move stepper; step1 - xxxx=int16; step2 - yyyy=int16
//  2: \2xxxx0000 - move head; pos xxxx=uint16 physical value [700..2300]; neutral 1500
//  3: \3000x000y - steppper_on: x=1 true, else false; y=1 then reset acc steps and angle
//  4: \400000000 - display Kx parameters
//  5: \5xxxxxxxx - set Kd             - 0.050
//  6: \6xxxxxxxx - set Kp             - 0.32
//  7: \7xxxxxxxx - set Ki_thr         - 0.1
//  8: \8xxxxxxxx - set Kp_thr         - 0.080
//  9: \9xxxxxxxx - set Kd_position    - 0.45
// 10: \:xxxxxxxx - set Kp_position    - 0.06
// 11: \;xxxxxxxx - set wheelbase
// 12: \<xxxxyyyy - set throttle int16 , steering int16; normalized to -0,5 .. 0,5
// 13: \=xxxxxxxx - set angle_offset   - 1.65 
//
// #define KP_RAISEUP 0.1   
// #define KD_RAISEUP 0.16   
//
// Data  Arduino --> Raspberry 
// \1xxxxxxxx - float acc_vect1_x
// \2xxxxxxxx - float acc_vect1_y
// \3xxxxxxxx - float acc_alpha
// \4xxxxxxxx - float angle_adjusted
// \5xxxxyyyy - int16_t adistance, int16_t headpos


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include <wiringPi.h>
#include <wiringSerial.h>

int main ()
{
  int fd ;
  int count ;
  int i;
  unsigned int nextTime ;

  if ((fd = serialOpen ("/dev/ttyACM0", 9600/*115200*/)) < 0)
  {
    fprintf (stderr, "Unable to open serial device: %s\n", strerror (errno)) ;
    return 1 ;
  }

/*  if (wiringPiSetup () == -1)
  {
    fprintf (stdout, "Unable to start wiringPi: %s\n", strerror (errno)) ;
    return 1 ;
  }
*/
  nextTime = millis () + 300 ;

  char line[16];
  char sline[100];
  int cmdid;
  short intval1;
  short intval2;
  short intval3;
  short intval4;
  float f1;
  float f2;
  
  union{
  float f;
  unsigned int i;
  } val;
    
  for ( ;; )
  {

    printf ("Enter command (cmd,int,int):\n") ;	

	  fgets(sline, sizeof(sline), stdin);
	  //scanf("%[^\n]", line);
    fflush (stdout) ;
    
    cmdid   = atoi(sline);

    printf ("cmd: %d\n", cmdid) ;	
    
    char *pstr = strstr(sline, ",");
    printf ("-\n");

    if (pstr)
		{
  
      switch(cmdid)
      {  
      
      case 1:
      case 2:
      case 3:
      case 4:      
        pstr++;      
//  			printf ("str1: %s\n", pstr) ;	
  
  			intval1 = atoi(pstr);
//  			printf ("V1: %d\n", intval1) ;	
  
  			pstr = strstr(pstr, ",");
  			
  			if (pstr)
  				{
//  					printf ("str2: %s\n", pstr) ;	
  
  					intval2 = atoi(pstr+1);
//  					printf ("V2: %d\n", intval2) ;	
  				
  					line[0]=92;
  					line[1]=cmdid + 0x30;
  					line[2]=((intval1 & 0xF000) >> 12) + 0x30;
  					line[3]=((intval1 & 0x0F00) >> 8) + 0x30;
  					line[4]=((intval1 & 0x00F0) >> 4) + 0x30;
  					line[5]=(intval1 & 0x000F) + 0x30;
  					line[6]=((intval2 & 0xF000) >> 12) + 0x30;
  					line[7]=((intval2 & 0x0F00) >> 8) + 0x30;
  					line[8]=((intval2 & 0x00F0) >> 4) + 0x30;
  					line[9]=(intval2 & 0x000F) + 0x30;
  					line[10]=0;
  
  				}
          break;
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
      case 10:
      case 11:
      case 13:      
          pstr++;                         

          val.f = atof(pstr);

					line[0]=92;
					line[1]=cmdid + 0x30;
					line[2]=((val.i & 0xF0000000) >> 28) + 0x30;
					line[3]=((val.i & 0x0F000000) >> 24) + 0x30;
					line[4]=((val.i & 0x00F00000) >> 20) + 0x30;
					line[5]=((val.i & 0x000F0000) >> 16) + 0x30;
					line[6]=((val.i & 0x0000F000) >> 12) + 0x30;
					line[7]=((val.i & 0x00000F00) >> 8) + 0x30;
					line[8]=((val.i & 0x000000F0) >> 4) + 0x30;
					line[9]=(val.i & 0x0000000F) + 0x30;
					line[10]=0;
      
          break;

      case 12:
      		printf ("Cmd Throttle, Steering, Time in ms") ;

          pstr++;                         
  
          f1 = atof(pstr);

    			pstr = strstr(pstr, ",");
      		if (!pstr) break;
          pstr++;      

          f2 = atof(pstr);

          intval1 = (short)(f1 * 65535.0);  
          intval2 = (short)(f2 * 65535.0);  

      		pstr = strstr(pstr, ",");
  	    	if (!pstr) break;
		      pstr++;      

		      intval3 = atoi(pstr);
				
					line[0]=92;
					line[1]=cmdid + 0x30;
					line[2]=((intval1 & 0xF000) >> 12) + 0x30;
					line[3]=((intval1 & 0x0F00) >> 8) + 0x30;
					line[4]=((intval1 & 0x00F0) >> 4) + 0x30;
					line[5]=(intval1 & 0x000F) + 0x30;
					line[6]=((intval2 & 0xF000) >> 12) + 0x30;
					line[7]=((intval2 & 0x0F00) >> 8) + 0x30;
					line[8]=((intval2 & 0x00F0) >> 4) + 0x30;
					line[9]=(intval2 & 0x000F) + 0x30;
					line[10]=0;

          
    			serialPuts(fd, line);
    	    usleep(intval3 * 1000);

      		printf ("Clr Throttle, Steering \n") ;
         
          intval1 = 0;  
          intval2 = 0;  

					line[0]=92;
					line[1]=cmdid + 0x30;
					line[2]=((intval1 & 0xF000) >> 12) + 0x30;
					line[3]=((intval1 & 0x0F00) >> 8) + 0x30;
					line[4]=((intval1 & 0x00F0) >> 4) + 0x30;
					line[5]=(intval1 & 0x000F) + 0x30;
					line[6]=((intval2 & 0xF000) >> 12) + 0x30;
					line[7]=((intval2 & 0x0F00) >> 8) + 0x30;
					line[8]=((intval2 & 0x00F0) >> 4) + 0x30;
					line[9]=(intval2 & 0x000F) + 0x30;
					line[10]=0;

          break;

      case 99: 
        pstr++;      
    		intval1 = atoi(pstr);
 
    		pstr = strstr(pstr, ",");
    		if (!pstr) break;

    		pstr++;      
    		intval2 = atoi(pstr);

    		pstr = strstr(pstr, ",");
    		if (!pstr) break;

    		pstr++;      
    		intval3 = atoi(pstr);

    		pstr = strstr(pstr, ",");
    		if (!pstr) break;

    		pstr++;      
    		intval4 = atoi(pstr);

    		printf ("Cmd Repeat: %d times every % ms \n", intval3, intval4) ;
      				
    		line[0]=92;
    		line[1]=1 + 0x30;
    		line[2]=((intval1 & 0xF000) >> 12) + 0x30;
    		line[3]=((intval1 & 0x0F00) >> 8) + 0x30;
    		line[4]=((intval1 & 0x00F0) >> 4) + 0x30;
    		line[5]=(intval1 & 0x000F) + 0x30;
    		line[6]=((intval2 & 0xF000) >> 12) + 0x30;
    		line[7]=((intval2 & 0x0F00) >> 8) + 0x30;
    		line[8]=((intval2 & 0x00F0) >> 4) + 0x30;
    		line[9]=(intval2 & 0x000F) + 0x30;
    		line[10]=0;
		
    		for (i = 1; i < intval3; i++)
    		{
    			printf ("Move: %d %d\n", intval1, intval2) ;
    			serialPuts(fd, line);	
    			usleep(intval4 * 1000);
    		}
    		printf ("Move: %d %d\n", intval1, intval2) ;
    		break;
      					
      case 100: 
        serialClose(fd);
        printf ("Quit\n") ;
        return 0 ;
        
    		break;
                
                     
      default:
        // do nothing repeat old command
        printf ("repeat previous command \n") ;	
                
        break;

      }
      
  		count = 10;
			printf ("Out: %s-%d\n", line,count) ;	
			
			serialPuts(fd, line);
			// serialPutchar (fd, line);
          				
		}

//      serialPutchar (fd, count) ;
//      nextTime += 300 ;
//      ++count ;
  }

  printf ("\n") ;
  return 0 ;
}
