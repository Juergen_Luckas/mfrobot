#include <opencv2/core/utility.hpp>
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <string>
#include <math.h>

#include "way_ctrl.hpp"

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>

#include <sstream>

#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
            ( std::ostringstream() << std::dec << x ) ).str()

using namespace cv;
using namespace std;


typedef void * (*THREADFUNCPTR)(void *);

extern "C" void serialWrite(int cmdid, short v1, short v2);
extern float acc_vect1_x;
extern float acc_vect1_y; 
extern float acc_angle;
extern short actual_motor_speed;

extern unsigned int data_rec_cnt;
extern unsigned int data_rec_cnt_old;

extern int mverbose;
extern int Pthrottle;
extern int Psteering;
extern int Ptiming;
extern int NRecalc;


void putPathToJob(order_t order, queue_t *const queue_msg2ser, pathdata_t pdata, int *cnt)
{
    float length;
    float length_sum=0;
    
    Point currentPos(acc_vect1_x*100.0+300.0,acc_vect1_y*100.0+300.0);
    float currentAngle = acc_angle;

    int cscnt=calcNewPath(pdata, &length, currentPos, currentAngle, order.destPos, order.destAngle, order.rotateAtStart);
    *cnt = cscnt;
    data_rec_cnt_old = data_rec_cnt;
    
    if (mverbose) cout << "putPathToJob - cscnt: " << cscnt << " currentAngle " << currentAngle << endl;

    struct job jq;
    jq.id = 11;
    jq.f1 = cscnt*2.0;
    jq.f2 = 0.0;
    jq.f3 = 0.0;
    queue_put(queue_msg2ser, &jq);

    for (int istep = 1; istep <= cscnt; istep++)
    {
        if ((istep==1) && (order.rotateAtStart)) {
          // first rotate to directly to the direction
          jq.id = 1;
          jq.f1 = +1600.0 / 90.0 * pdata[istep].dalpha;
          jq.f2 = -1600.0 / 90.0 * pdata[istep].dalpha;
          jq.f3 = 250.0;
          queue_put(queue_msg2ser, &jq);

          jq.id = 12;
          jq.f1 = pdata[istep].norm;
          jq.f2 = 0.0;
          jq.f3 = 0.0;
          queue_put(queue_msg2ser, &jq);
        } else {
          jq.id = 12;
          jq.f1 = pdata[istep].norm;
          jq.f2 = pdata[istep].dalpha;
          jq.f3 = 0.0;
          queue_put(queue_msg2ser, &jq);
        }
       
        length_sum = length_sum + jq.f1; 
        if (mverbose) cout << std::fixed << std::setw( 11 ) << std::setprecision( 4 ) << "CS: " << jq.f1 << " " << pdata[istep].alpha << " | " << jq.f2 << " | " << pdata[istep].pnt << endl;
    }
}

way_ctrl::way_ctrl()
{
    int err;
          
    pathData_cnt=0;      
    err = pthread_create(&threadIdWayCtrl, NULL, (THREADFUNCPTR) &pathStepThread, this);
    if (err != 0){
      printf("\ncan't create Way Ctrl  thread :[%s]", strerror(err));
    }
    err = queue_init(&q_msg2ser, 30);
    if (err != 0){
      printf("\ncan't create queue:[%s]", strerror(err));
    }
}

void way_ctrl::addPathDest(Point indestPos, float indestAngle, bool inrotateAtStart, Point *objptr, bool trackObj)
{

    order_t order;

    order.j.id = 0;
    order.destPos = indestPos;
    order.destAngle = indestAngle;
    order.rotateAtStart = inrotateAtStart;
    order.objectPtr = objptr;
    order.tracking = trackObj;
    
    mutx.lock();
    if ((queue_currentUsage(&q_msg2ser)==0) and (orderbook.size()==0)) { 
        // queue directly
        if (mverbose) cout << "addPathDest: directly " << endl;
        putPathToJob(order, &q_msg2ser, pathData, &pathData_cnt);
        destPos = order.destPos;
        destAngle = order.destAngle;
    } else {
        orderbook.put(order);
        if (mverbose) cout << "Queue Order: " << endl;
    }
    mutx.unlock(); 
}

void way_ctrl::addDirectJob(struct job *const jqptr)
{
    mutx.lock();
    if ((queue_currentUsage(&q_msg2ser)==0) and (orderbook.size()==0)) { 
        // queue directly
        queue_put(&q_msg2ser, jqptr);
        if (mverbose) cout << "Queue Job directly: " << endl;
    } else {
        order_t order;
        order.j=*jqptr;
        orderbook.put(order);
        if (mverbose) cout << "Queue Job: " << endl;
    }           
    mutx.unlock(); 
}


bool way_ctrl::getStatusBusy(void)
{
    bool retvalue;
    mutx.lock();
    retvalue = (queue_currentUsage(&q_msg2ser)>0) or (orderbook.size()>0);
    mutx.unlock();   
    return retvalue;
}

void way_ctrl::clearPath(void)
{
    struct job jq;    

    mutx.lock();
    orderbook.clear();
    queue_clear(&q_msg2ser);
    jq.id = 12;
    jq.f1 = 0.0;
    jq.f2 = 0.0;
    jq.f3 = 250.0;
    queue_put(&q_msg2ser, &jq);                   
    destPos = Point(0,0);
    mutx.unlock();   
}

void *way_ctrl::pathStepThread(void *arg)
{
    way_ctrl *pThis = (way_ctrl*) arg;

    float throttle;
    float steering;
    unsigned int timing;
    long            ms; // Milliseconds
    time_t          s;  // Seconds
    struct timespec spec;
    int i;
    struct job jq;
    order_t order;
    float length;
    float dalpha;
    int cscnt;
    short sm1;  
    short sm2;  
    int job12MaxCnt=0;

    while (1)
    {
  
      Point currentPos(acc_vect1_x*100.0+300.0,acc_vect1_y*100.0+300.0);
      float currentAngle = acc_angle;
     
      pThis->mutx.lock(); 
      if (queue_currentUsage(&pThis->q_msg2ser)==0) {
        // check if orderbook has new order
        if (pThis->orderbook.size()>0) {
            // process new order
            order=pThis->orderbook.get();
            if (order.j.id==0) {

                if (mverbose) cout << "Read from Queue Order: " << endl;
                putPathToJob(order, &pThis->q_msg2ser, pThis->pathData, &pThis->pathData_cnt);

                pThis->destPos = order.destPos;
                pThis->destAngle = order.destAngle;

            } else {
                // job id is not zero, queue the job
                jq.id = 12;
                jq.f1 = 0.0;
                jq.f2 = 0.0;
                jq.f3 = 250.0;
                queue_put(&pThis->q_msg2ser, &jq);                

                if (mverbose) cout << "Read from Queue Job: " << endl;
                jq=order.j;
                queue_put(&pThis->q_msg2ser, &jq);                               
            }
          } else {
            // no new order, stop movement throttle, steering
            serialWrite(12, 0, 0);
        }
      } // else, still jobs in the queue
      pThis->mutx.unlock();   
    
      // wait for a new job
      queue_get(&pThis->q_msg2ser, &jq);      

      switch (jq.id)
      {  
      case 1:
          // send cmd1, sm1, sm2
          
          sm1 = jq.f1;
          sm2 = jq.f2;

          if (sm1 > sm2) 
            ms = (sm1 / 2) ; 
          else
            ms = (sm2 / 2) ;                    
          ms += (long)jq.f3;

          if (mverbose) cout << "job 1: " << sm1 << " " << sm2 << " " << ms << endl;
          serialWrite(1, sm1, sm2);                  
          usleep(ms * 1100);
          break;
      case 2:
          // send cmd2, headpos
          sm1 = jq.f1;
          sm2 = jq.f2;
          ms  = jq.f3;

          if (mverbose) cout << "job 2: " << sm1 << " " << sm2 << " " << ms << endl;
          serialWrite(2, sm1, sm2);      
          usleep(ms * 1000.0);
          break;
      case 3:
          sm1 = jq.f1;
          sm2 = jq.f2;

          if (mverbose) cout << "job 3: " << sm1 << " " << sm2 << endl;
          serialWrite(3, sm1, sm2);                  
          break;
      case 4:
          // rotate to fix angle
          serialWrite(1, 0, 0);                  
          usleep(200 * 1000.0);

          dalpha = jq.f1 - acc_angle;

          dalpha = fmod(dalpha, 2.0 * PI);
          if (dalpha > PI)
            dalpha = dalpha - 2.0 * PI;

          if (dalpha < -PI)
            dalpha = dalpha + 2.0 * PI;
          
          if (mverbose) cout << " rotate dalpha " << dalpha*180.0/PI << endl;
          
          sm1 = +3200.0 / PI * dalpha;
          sm2 = -3200.0 / PI * dalpha;

          
          if (sm1 > sm2) 
            ms = (sm1 / 2) ; 
          else
            ms = (sm2 / 2) ;                    
          ms += (long)jq.f1;

          serialWrite(1, sm1, sm2);      
          usleep(ms * 1000.0);
          break;
          
      case 11:
          // Start sequence for job 12
          job12MaxCnt = jq.f1 * 2.0;
          break;
      case 12:
          
          if ((data_rec_cnt >= (data_rec_cnt_old + NRecalc)) and (pThis->destPos.x != 0))
          {
            data_rec_cnt_old = data_rec_cnt;          
            cscnt = calcNewPath(pThis->pathData, &length, currentPos, currentAngle, pThis->destPos, pThis->destAngle, false);
    
            if ((length < 0.2) || (job12MaxCnt <= 0))
            {
              cout << "No Job: len: " << length << " job12MaxCnt " << job12MaxCnt << endl;
              queue_clear(&pThis->q_msg2ser);
              serialWrite(12, 0, 0);
              job12MaxCnt=0;
            }
            else
            {
              cout << "Recalculated Steps: " << cscnt << " Len: " << length << " currentAngle " << currentAngle*180.0/PI << endl;
              pThis->pathData_cnt = cscnt;

              queue_clear(&pThis->q_msg2ser);

              for (int istep = 1; istep <= cscnt; istep++)
              {
                  jq.id = 12;
                  jq.f1 = pThis->pathData[istep].norm;
                  jq.f2 = pThis->pathData[istep].dalpha;
                  jq.f3 = 0.0;
                  queue_put(&pThis->q_msg2ser, &jq);
                  
                  if (mverbose) cout << std::fixed << std::setw( 11 ) << std::setprecision( 4 ) << "CS: " << jq.f1 << " " << pThis->pathData[istep].alpha << " | " << jq.f2 << " | " << pThis->pathData[istep].pnt << endl;
              }
            }
          }
          else
          {
              job12MaxCnt--;

              if (pThis->objectTrack) {
                  lookAtObject(currentPos,currentAngle,pThis->objectPos);
              }
              
              throttle = -jq.f1 * Pthrottle / 100.0;
              steering = -jq.f2 * Psteering / 100.0 / 25.0;
              timing = (unsigned int)((jq.f1 - ((float) actual_motor_speed) / 4000.0) * Ptiming * 10); 
              
              if (timing > 400) timing = 400;
              if (throttle > 0.13) throttle = 0.13;
              if (throttle < -0.13) throttle = -0.13;
              if (steering > 0.5) steering = 0.5;
              if (steering < -0.5) steering = -0.5;
    
              if (mverbose) cout << std::fixed << std::setw( 11 ) << std::setprecision( 4 ) << "job 12: " << throttle << " " << steering << " | " << actual_motor_speed << " | " << timing << endl;
              
              serialWrite(12, (short) (throttle * 65535.0), (short) (steering * 65535.0));                  
              usleep(timing * 1000);
    
           }

          break;
          
      case 90:
          // stop
          queue_clear(&pThis->q_msg2ser);
          serialWrite(12, 0, 0);
          cout << "STOP!" << endl;
                    
          usleep(500 * 1000);
          break;
 
  
      default:
          break;
                
      }
    }        
}    
    
void lookAtObject(Point currentPos, float currentAngle, Point objectPos)
{
    int headcmd = 1500;
    if (objectPos.x!=currentPos.x) {
        headcmd = 1500 - (int)((atan((float)(objectPos.y-currentPos.y)/(float)(objectPos.x-currentPos.x)) - currentAngle) * 4.0 / PI * 800.0);
    }
    if (headcmd<1200) headcmd = 1200;
    if (headcmd>1800) headcmd = 1800;
    serialWrite(2, headcmd, 0);      
    if (mverbose) cout << "headcmd " << headcmd << "currentPos " << currentPos << "objectPos " << objectPos << "currentAngle " << currentAngle << endl;
}

void deCasteljau(Point *points, int degree, float t, Point2f *outpoint)
{
		float *pointsQ = new float[(degree + 1) * 2]; // same as pointsQ[numPoints + 1][3]
		int Qwidth = 2;
		for(int j = 0; j <= degree; j++){
			pointsQ[j*Qwidth + 0] = points[j].x;
			pointsQ[j*Qwidth + 1] = points[j].y;
		}
		for(int k = 1; k <= degree; k++){
			for(int j = 0; j<= degree - k; j++){
				pointsQ[j*Qwidth+0] = (1-t) * pointsQ[j*Qwidth+0] + t * pointsQ[(j+1)*Qwidth+0];
				pointsQ[j*Qwidth+1] = (1-t) * pointsQ[j*Qwidth+1] + t * pointsQ[(j+1)*Qwidth+1];
			}
		}
		outpoint->x = pointsQ[0];
		outpoint->y = pointsQ[1];
		delete[] pointsQ;
}

int calcNewPath(pathdata_t pd, float *length, Point startPos, float startAngle, Point destPos, float destAngle, bool rotateAtStart)
{
  int count;
  float len;  

  Point ipt[4];
  Point2f outptf,lastptf;
  
  float dx = startPos.x - destPos.x;
  float dy = startPos.y - destPos.y;
  
  len = sqrt(dx*dx + dy*dy);
  *length = len;

  ipt[0].x = startPos.x;
  ipt[0].y = startPos.y;

  if (rotateAtStart) {
    ipt[1].x = startPos.x - dx * 0.1;
    ipt[1].y = startPos.y - dy * 0.1;
  } else {
    ipt[1].x = startPos.x - len / 4 * cos(startAngle);
    ipt[1].y = startPos.y - len / 4 * sin(startAngle);
  }
      
  ipt[2].x = destPos.x + len / 4 * cos(destAngle);
  ipt[2].y = destPos.y + len / 4 * sin(destAngle);

  ipt[3].x = destPos.x;
  ipt[3].y = destPos.y;

if (mverbose==2) cout << ipt[0] << ipt[1] << ipt[2] << ipt[3] << endl;

  lastptf.x = ipt[0].x; 
  lastptf.y = ipt[0].y;
  
  double radius;
  double alpha;
  double cs1;
  double cs2;
  double ralpha; 
  Point2f dptf;          
  double halpha; 
  double dalpha;
  
  alpha = fmod(startAngle, 2.0 * PI);
  
  count = (len / 100) / 0.1; // first approximation 10cm

  deCasteljau(ipt, 3, 1 / ((float)count) , &outptf);
  dptf = (outptf-lastptf); 
  
  count = (float)count * norm(dptf) / 0.1 / 100.0 + 0.5;

  pd[0].pnt = Point(ipt[0].x,ipt[0].y);
                                              
  for (int istep = 1; istep <= count; istep++)
  {
     
     float step = ((float)istep) / ((float)count); 
     
     deCasteljau(ipt, 3, step, &outptf);

     Point lastpt = lastptf;
     Point outpt = outptf;


     pd[istep].pnt = outpt; 
   
     dptf = (outptf-lastptf); 

     if (dptf.x < -0.0001) {
       halpha = atan(dptf.y / dptf.x);
     } else {
       if (dptf.x > 0.0001) {
           halpha = PI + atan(dptf.y / dptf.x);
       } else {
         if (dptf.y > 0.0)
           halpha = -PI / 2.0;
         else
           halpha = PI / 2.0;
       }
     }
if (mverbose==2) cout << " halpha " << halpha;
         
     pd[istep].norm  = norm(dptf) / 100.0;
     pd[istep].alpha = halpha * 180.0 / PI;

     
     dalpha = halpha - alpha;
if (mverbose==2) cout << " dalpha " << dalpha;

     dalpha = fmod(dalpha, 2.0 * PI);
     if (dalpha > PI)
       dalpha = dalpha - 2.0 * PI;

     if (dalpha < -PI)
       dalpha = dalpha + 2.0 * PI;

if (mverbose==2) cout << " dalpha2 " << dalpha << endl;

     alpha = halpha;

     pd[istep].dalpha = dalpha * 180.0 / PI;
     
     lastptf = outptf;
  }  
  
  return count;
          
}




