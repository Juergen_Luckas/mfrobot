// Protocol
// The data are transferred in chars - each message has 10 chars.
// Startflag         "\"
// Msg ID            0-255
// MsgValue[0..7]    4 Byte Hex-values, each nibble (4Bit) is transmitted as char in sequence, with nibble-value plus 0x30
//                   Nibble x0 ->  0+0x30 = "0"
//                   Nibble x1 ->  1+0x30 = "1"
//                   Nibble xA -> 14+0x30 = ":"
//                   Nibble xB -> 14+0x30 = ";"
//                   Nibble xC -> 14+0x30 = "<"
//                   Nibble xD -> 14+0x30 = "="
//                   Nibble xE -> 14+0x30 = ">"
//                   Nibble xF -> 15+0x30 = "?"
//                   0123456789ABCDEF
//                   0123456789:;<=>?
//
// Data  Raspberry --> Arduino
//  1: \1xxxxyyyy - move stepper; step1 - xxxx=int16; step2 - yyyy=int16
//  2: \2xxxx0000 - move head; pos xxxx=uint16 physical value [700..2300]; neutral 1500
//  3: \3000x000y - steppper_on: x=1 true, else false; y=1 then reset acc steps and angle
//  4: \400000000 - display Kx parameters
//  5: \5xxxxxxxx - set Kd             - 0.050
//  6: \6xxxxxxxx - set Kp             - 0.32
//  7: \7xxxxxxxx - set Ki_thr         - 0.1
//  8: \8xxxxxxxx - set Kp_thr         - 0.080
//  9: \9xxxxxxxx - set Kd_position    - 0.45
// 10: \:xxxxxxxx - set Kp_position    - 0.06
// 11: \;xxxxxxxx - set wheelbase
// 12: \<xxxxyyyy - set throttle int16 , steering int16; normalized to -0,5 .. 0,5
// 13: \=xxxxxxxx - set angle_offset   - 1.65 
//
// #define KP_RAISEUP 0.1   
// #define KD_RAISEUP 0.16   
//
// Data  Arduino --> Raspberry 
// \1xxxxxxxx - float acc_vect1_x
// \2xxxxxxxx - float acc_vect1_y
// \3xxxxxxxx - float acc_alpha
// \4xxxxxxxx - float angle_adjusted
// \5xxxxyyyy - int16_t adistance0, int16_t headpos
// \6xxxxyyyy - int16_t actual_robot_speed, int16_t adistance1

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <wiringSerial.h>
#include <pthread.h>
#include <time.h>

#include "queue_job.h"

#define c_pi 3.141592
#define WHEEL_CIRCUM (c_pi * 0.09)
#define WHEEL_BASE 0.18
#define MICROSTEPPING 16

void* serialReadT(void *arg);
void* keythread(void *arg);
void serialWrite(int cmdid, short v1, short v2);

extern float acc_vect1_x;
extern float acc_vect1_y; 
extern float acc_angle;
extern float angle_z;
extern short adist0;
extern short adist1;
extern unsigned short headpos;
extern short actual_motor_speed;

extern int txTimestep;

char ptog;
char ptrc;

int fdescr;

extern pthread_mutex_t condition_mutex;
extern pthread_cond_t  condition_cond;

extern int Pthrottle;
extern int Psteering;
extern int Ptiming;

extern struct queue q_msg2ser;

void* keythread(void *arg)
{
    unsigned long i = 0;
    char c;

    pthread_t id = pthread_self();

    while (1)
    {
       c = getchar();
   
       if (c == 't')
       {
         ptrc = !ptrc; 
       }
       if (c == ' ')
       {     
         ptog = !ptog;
       }
       printf ("Keyboard char \n");
       putchar(c);
       fflush (stdout) ;

       usleep(1000);
    }

    return NULL;
}

void serialWriteT2(int pos,void* arg)
{
    int fd ;
    char com_tx[16];
    float posf; 
    short cs1;
    short cs2;
    
    if ((fd = serialOpen ("/dev/ttyACM0", 57600)) < 0)
    {
      printf ("Unable to open serial device: %s\n", strerror (errno)) ;
      return NULL;
    }
    printf ("Pos: %d  arg: %d\n", pos, *((int*)arg)) ;
    
    posf = ((float)pos) / 1000.0;

    cs2 = *((short*)&posf);
    cs1 = *(((short*)&posf) + 1);
    
    com_tx[0] = 92; // '\'
    com_tx[1] = *((int*)arg) + 0x30;
    com_tx[2] = ((cs1 & 0xF000) >> 12) + 0x30;
    com_tx[3] = ((cs1 & 0x0F00) >> 8) + 0x30;
    com_tx[4] = ((cs1 & 0x00F0) >> 4) + 0x30;
    com_tx[5] = ((cs1 & 0x000F)) + 0x30;
    com_tx[6] = ((cs2 & 0xF000) >> 12) + 0x30;
    com_tx[7] = ((cs2 & 0x0F00) >> 8) + 0x30;
    com_tx[8] = ((cs2 & 0x00F0) >> 4) + 0x30;
    com_tx[9] = ((cs2 & 0x000F)) + 0x30;
    com_tx[10] = 0;
    
    serialPuts(fd, com_tx);
    
    serialClose (fd);
}


void serialWrite(int cmdid, short v1, short v2)
{
            
    char com_tx[16];
            
    com_tx[0] = 92; // '\'
    com_tx[1] = cmdid + 0x30;
    com_tx[2] = ((v1 & 0xF000) >> 12) + 0x30;
    com_tx[3] = ((v1 & 0x0F00) >> 8) + 0x30;
    com_tx[4] = ((v1 & 0x00F0) >> 4) + 0x30;
    com_tx[5] = ((v1 & 0x000F)) + 0x30;
    com_tx[6] = ((v2 & 0xF000) >> 12) + 0x30;
    com_tx[7] = ((v2 & 0x0F00) >> 8) + 0x30;
    com_tx[8] = ((v2 & 0x00F0) >> 4) + 0x30;
    com_tx[9] = ((v2 & 0x000F)) + 0x30;
    com_tx[10] = 0;
    
    serialPuts(fdescr, com_tx);

}

void* serialReadT(void *arg)
{

    long            ms; // Milliseconds
    time_t          s;  // Seconds
    time_t          s0;  // Seconds
    struct timespec spec;

    clock_gettime(CLOCK_REALTIME, &spec);
    s0  = spec.tv_sec;

    unsigned long i = 0;

    float tf;

    int rx_data_cnt;
    int rx_data_id;
    int rx_data_val;
   	char rx_cur;
  
    rx_data_cnt = -1;
    rx_data_id = 0;
    rx_data_val =  0;

    pthread_t id = pthread_self();

    if ((fdescr = serialOpen ("/dev/ttyACM0", 57600)) < 0)
    {
      printf ("Unable to open serial device: %s\n", strerror (errno)) ;
      return NULL;
    }

    while (1)
    {

      if (serialDataAvail (fdescr))
  	  {
        rx_cur = serialGetchar (fdescr);
  
        if (rx_cur == 92) // '\'
        {
          // that is the message start condition
          rx_data_cnt = 0;
          rx_data_id = 0;
          rx_data_val =  0;
        }
        else
        {
          if (rx_data_cnt == 0)
          {
              rx_data_id = rx_cur - 0x30; 
              rx_data_cnt = 1;
          }
          // A normal msg data, counter should be greater 0
          else 
          {
            if ((rx_data_cnt > 0) && (rx_data_cnt <= 8))
            {
              rx_data_val += ((unsigned long)((rx_cur-0x30) & 0x0F)) << (32 - (rx_data_cnt << 2));
      
              rx_data_cnt++;
      
              if (rx_data_cnt > 8)
              {
                rx_data_cnt = -1;
                
                switch (rx_data_id)
                {
                  case 1:
                    tf = *((float*)&rx_data_val);
                    if (ptog) printf ("Msg 1: acc_vect1_x %f\n",tf);
                    acc_vect1_x = tf;
                    break;
    
                  case 2:
                    tf = *((float*)&rx_data_val);
                    if (ptog) printf ("Msg 2: acc_vect1_y %f\n",tf);
                    acc_vect1_y = tf;           

                    break;
    
                  case 3:
                    tf = *((float*)&rx_data_val);
                    if (ptog) printf ("Msg 3: acc_angle  %f\n",tf);
                    pthread_mutex_lock( &condition_mutex );
                    acc_angle = tf; 
                    pthread_cond_signal( &condition_cond );
                    pthread_mutex_unlock( &condition_mutex );
                        
                    break;
    
                  case 4:
                    tf = *((float*)&rx_data_val);
                    if (ptog) printf ("Msg 4: angle       %f\n",tf);
                    angle_z = tf; 
                    
                    break;
    
                  case 5:
                    adist0 = *((short*)&rx_data_val+1);
                    headpos = *((unsigned short*)&rx_data_val);
                    if (ptog) printf ("Msg 5: adist0  %d\n",adist0);
                    if (ptog) printf ("Msg 5: headpos %d\n",headpos);
    
                    break;

                  case 6:
                    actual_motor_speed = *((short*)&rx_data_val+1);
                    adist1 = *((short*)&rx_data_val);
                    if (ptog) printf ("Msg 6: motor speed %d\n",actual_motor_speed);
                    if (ptog) printf ("Msg 6: adist1      %d\n",adist1);
   
                    if (ptrc)
                    { 
                      clock_gettime(CLOCK_REALTIME, &spec);
                  
                      s  = spec.tv_sec - s0;
                      ms = round(spec.tv_nsec / 1.0e6); // Convert nanoseconds to milliseconds
                      if (ms > 999) {
                          s++;
                          ms = 0;
                      }
                      
                      printf ("Msg 6:xya:%f:%f:%f:sp:%d:%d.%*d\n", acc_vect1_x, acc_vect1_x, acc_angle /c_pi*180.0, actual_motor_speed, s,3, ms);
                    }
    
                    break;
    
                  default:
                    printf ("Msg invalid: %d\n",rx_data_id);
                  
                    break;
                 }
              }
            }
            else
             {
            	if (ptrc) {
                	putchar (rx_cur) ;
      		        fflush (stdout) ;
                }
                rx_data_cnt = -1;
             }
           }
         }          

      }

      usleep(100);
    }

    return NULL;
}


