#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/objdetect.hpp"
#include "opencv2/imgcodecs.hpp"
#include <opencv2/dnn/dnn.hpp>
#include "dnn_yolo.hpp"
#include "way_ctrl.hpp"

#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <string>

#include <X11/Xlib.h>

#include <errno.h>
#include <stdio.h>
#include <string.h>

#include <sstream>

extern "C" {
#include "queue_job.h"
}

#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
            ( std::ostringstream() << std::dec << x ) ).str()

using namespace cv;
using namespace std;
using namespace cv::dnn;

#include <wiringSerial.h>
#include <pthread.h>

#define c_pi 3.14159265359
#define WHEEL_CIRCUM (c_pi * 0.09)
#define WHEEL_BASE 0.18



pthread_t tid1;
pthread_t tid2;
pthread_t tid3;
pthread_t tid4;

pthread_mutex_t condition_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t  condition_cond  = PTHREAD_COND_INITIALIZER;

pthread_mutex_t grayimage_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t  grayimage_cond  = PTHREAD_COND_INITIALIZER;

pthread_mutex_t imageshadow_mutex = PTHREAD_MUTEX_INITIALIZER;

way_ctrl rway;

obj_spec_t eggtree[2];

float dnn_pos_x;
float dnn_pos_y;
float dnn_angle_xy;
float dnn_headangle;

float acc_vect1_x;
float acc_vect1_y; 
float acc_vect1_x_old; 
float acc_vect1_y_old;
float acc_angle;
float angle_z;
float headangle;
    
short actual_motor_speed;
short adist0;
short adist1;
unsigned short headpos;

float distm, distbottom;
Mat imgroomshadow;

Mat graypic;
Mat featpic;
    
extern "C" void* serialReadT(void *arg);
extern "C" void* keythread(void *arg);

extern "C" void serialWrite(int cmdid, short v1, short v2);

extern "C" void serialWriteT2(int pos,void *arg);

extern void* imageProcT(void *arg);

Mat dispPicDNN;

/** Global variables */
String face_cascade_name = "/home/pi/opencv_build/opencv/data/haarcascades/haarcascade_frontalface_alt.xml";
String eyes_cascade_name = "/home/pi/opencv_build/opencv/data/haarcascades/haarcascade_eye_tree_eyeglasses.xml";
CascadeClassifier face_cascade;
CascadeClassifier eyes_cascade;

bool trackFlag = false;    

int thresh = 30;
int max_thresh = 1000;

int precision = 10; // length / ;
int txTimestep = 40;

int KP_user = 360; 
int KD_user =  75;
int KP_thr_user = 80; 
int KI_thr_user = 100;

int Pthrottle = 100;
int Psteering = 24;
int Ptiming = 430;
int NRecalc = 10;

int KP_cmd = 6; 
int KD_cmd = 5;
int KP_thr_cmd = 8; 
int KI_thr_cmd = 7;

int newpic;

int mtx=-1;
int mty=-1;
float mta=0;

int mbp=0;      // mouse botton pressed
int clrscr=0;
int mcc=0;
int mso=0;
int mdir=0;     // direct, rotate before moving job 12
int mverbose=0; // print verbose

unsigned int data_rec_cnt = 0;
unsigned int data_rec_cnt_old = 0;

typedef struct objloc_st{
float pos_x;
float pos_y;
float angle_xy;
float headangle;
} objloc_t;

objloc_t locLastObjDetect;

Point eggPosition;
Point eggPositionTmp[3];
int eggIndex = 0;
int eggStatus = 0;
// 0 invalid
// 1 valid
// 2 confirmed
// 3 doubleconfirmed

const short headpos_cs[13] = {  1600,
                                1700, 
                                1800, 
                                1700, 
                                1600, 
                                1500, 
                                1400, 
                                1300, 
                                1200, 
                                1300, 
                                1400, 
                                1500,
                                1500 }; 

int lastcycleactive=0;

void WinPrintClr(Mat & imgroom, float gf);
void drawText(Mat & image, int i, double fscale);

void calcEggPosition(obj_spec_t relPosPbj, objloc_t locObj)
{
    int rpx, rpy;
    float cosout, sinout;

    if (relPosPbj.confidence > 0.4) {
      cosout = cos(locObj.angle_xy-locObj.headangle+c_pi);
      sinout = sin(locObj.angle_xy-locObj.headangle+c_pi);
      float obj_x = 0.01 * relPosPbj.distance;
      float obj_y = obj_x * tan(relPosPbj.angleXY);

      // rotating matrix with the accumulated alpha
      // /x'\    / cos a   -sin a \   /x\
      // |  | =  |                | * | |
      // \y'/    \ sin a   cos a  /   \y/

      rpx = 100.0 * (locObj.pos_x + obj_x * cosout - obj_y * sinout);   
      rpy = 100.0 * (locObj.pos_y + obj_x * sinout + obj_y * cosout);

      if (eggStatus == 0) {
        eggPositionTmp[0]=Point(rpx,rpy);
        eggPositionTmp[1]=Point(rpx,rpy);
        eggPositionTmp[2]=Point(rpx,rpy); 
      } else {
        eggPositionTmp[eggIndex]=Point(rpx,rpy);
        eggIndex++; 
        if (eggIndex>2) eggIndex=0;
      }
      eggStatus++;
      if (eggStatus>3) eggStatus=3;

      eggPosition = (eggPositionTmp[0] + eggPositionTmp[1] + eggPositionTmp[2]) / 3;

//      cout << "eggPosition " << eggPosition << endl;

    } else {
      if (eggStatus>0) eggStatus--;
    }
}

static void onMouse( int event, int x, int y, int flags, void* )
{
    if( event == EVENT_LBUTTONDOWN )
    {
        cout << "tx: " << x << "ty: " << y << endl;    
        mtx = x;
        mty = y;
        mbp = 1;
        mta = acc_angle;
    }
    
    if( event == EVENT_MOUSEWHEEL )
    {
        mta = mta + (15.0 / 180.0 * c_pi) * getMouseWheelDelta(flags);
    }
    
    if( event == EVENT_LBUTTONUP )
    {
        struct job jq;
        
        cout << "b up" << endl;
        mbp = 0;

//        cout << std::fixed << std::setw( 11 ) << std::setprecision( 4 ) << "LEN: " << length << " " << length_sum << " " << acc_angle << endl;    
//
//        queue_clear(&q_msg2ser);
//        
//        for (int i = 1; i <= cscount; i++)
//        {
//          //std::cout << std::fixed << std::setw( 11 ) << std::setprecision( 6 ) << my_double;
//          cout << std::fixed << std::setw( 11 ) << std::setprecision( 4 ) << "CS: " << cs1a[i] << " " << cs2a[i] << " | " << cs3a[i] << endl;
//  
//          jq.id = 12;
//          jq.f1 = cs1a[i];
//          jq.f2 = cs3a[i];
//          jq.f3 = (float)actual_motor_speed;
//  
//          queue_put(&q_msg2ser, &jq);
//        }
//        jq.id = 12;
//        jq.f1 = 0;
//        jq.f2 = 0;
//        jq.f3 = (float)actual_motor_speed;
//
//        queue_put(&q_msg2ser, &jq);
        
    }
    //cout << "Event: " << event << endl;    
}



void *WinPrintWay(void *arg)
{
    float length;
    float gf = 100.0;
    pathdata_t pdata;
    int cscount;
       
    pthread_t id = pthread_self();
    
    Mat imgroom;
    
    WinPrintClr(imgroom, gf);
  
    int i = 0;
    int rpx, rpy;
    float cosout, sinout;
    float distx;
    float disty;
    
    while (1)
    {
  
      pthread_mutex_lock( &condition_mutex );
      pthread_cond_wait( &condition_cond, &condition_mutex );
      pthread_mutex_unlock( &condition_mutex );

      data_rec_cnt = data_rec_cnt +1;
  
      if (clrscr)
      {
          WinPrintClr(imgroom, gf);
          clrscr = 0;
      }
  
      if (i == 0)
      {
          acc_vect1_x_old = acc_vect1_x;
          acc_vect1_y_old = acc_vect1_y;
      }
 
        distm = 0.01 * (10650.08 * pow(adist0,-0.935) - 10);
        // consider also the tilt angle of the segway
        distm = distm * cos(angle_z * c_pi / 180);

        headangle = ((float)(headpos-1500)) / 1500.0 * c_pi / 2.0;   // 1500 is equaviliant 90 deg

        /* distance in m */
        distbottom = 0.26707 * pow(adist1* 5.0 / 1023.0, -1.267);   

        /**************************************************
         * plot tilt angle in oscilloscope
         **************************************************/
        if (i==200) {
          i=0;
          rectangle(imgroom, Point(301,101), Point(500,199), Scalar(0, 0, 0),-1);
          line(imgroom, Point(300,150), Point(500,150), Scalar( 40, 40, 40 ), 1, 8, 0 );
        }

        circle(imgroom,
                 Point(300+i, 150 - 5.0 * angle_z),
                 1,
                 Scalar( 150, 150, 0 ),
                 1,
                 7 );            

        /**************************************************
         * plot bottom distance
         **************************************************/

        circle(imgroom,
                 Point(300+i, 200 - 250.0 * distbottom),
                 1,
                 Scalar( 200, 0, 50 ),
                 1,
                 7 );            


//        cout << "Head angle" << headangle * 90.0 * 2.0 / 3.141592 << endl;
//        cout << "distm     " << distm << endl;

        /**************************************************
         * head distance detection
         **************************************************/

        distx = distm * cos(headangle + c_pi);
        disty = -distm * sin(headangle + c_pi);

        // rotating matrix with the accumulated alpha
        // /x'\    / cos a   -sin a \   /x\
        // |  | =  |                | * | |
        // \y'/    \ sin a   cos a  /   \y/

        cosout = cos(acc_angle);
        sinout = sin(acc_angle);

        rpx =  300 + gf * (acc_vect1_x + distx * cosout - disty * sinout);   
        rpy =  300 + gf * (acc_vect1_y + distx * sinout + disty * cosout);


        //if ((distbottom < 0.15) || (distbottom > 0.35))
        //{
          //circle(imgroom,
                 //Point(rpx,rpy),
                 //2,
                 //Scalar( 255, 0, 0 ),
                 //1,
                 //7 );            
        //}

        /**************************************************
         * bottom distance detection
         **************************************************/

        if ((distbottom < 0.15) || (distbottom > 0.35))
        {
          circle(imgroom,
                 Point(rpx,rpy),
                 2,
                 Scalar( 255, 0, 0 ),
                 1,
                 7 );            
//          if (mverbose) cout << "distbottom    " << distbottom << endl;
                 
        }

        /**************************************************
         * calc object position
                  locAtPictureTime.pos_x = acc_vect1_x;
                  locAtPictureTime.pos_y = acc_vect1_y;
                  locAtPictureTime.angle_xy = acc_angle;
                  locAtPictureTime.headangle = headangle;
         * 
         **************************************************/
  
        if (eggStatus>0)
          circle(imgroom,
                 eggPosition+Point(300,300),
                 3,
                 Scalar( 255, 255, 0 ),
                 1,
                 7 );            
               
        //void line(InputOutputArray img, Point pt1, Point pt2, const Scalar& color, 
        // int thickness=1, int lineType=LINE_8, int shift=0 )
        line(imgroom, 
            Point(acc_vect1_x*gf + 300,acc_vect1_y*gf + 300),
            Point(acc_vect1_x_old*gf + 300,acc_vect1_y_old*gf + 300), 
            Scalar( 0, 0, 255 ), 1, 8, 0 );
        
        rectangle(imgroom, Point(500,0), Point(600,30), Scalar(0, 100, 0),-1);

        putText(imgroom, "i; " + SSTR( i ) +" "+ SSTR( eggStatus ),
            Point(510, 15),
            FONT_HERSHEY_COMPLEX, 0.5, // font face and scale
            Scalar(255, 255, 255), // white
            0.5, LINE_AA); // line thickness and type

        arrowedLine(imgroom, 
            Point(580,15),
            Point(580 - 14*cos(acc_angle),15 - 14 * sin(acc_angle)), 
            Scalar( 255, 255, 255 ), 1, 8, 0, 0.2);

        pthread_mutex_lock( &imageshadow_mutex);
        imgroomshadow = imgroom.clone();
        pthread_mutex_unlock( &imageshadow_mutex);         

        /**************************************************
         * print recalculated way
         **************************************************/

        if (rway.pathData_cnt>0) {
         for (int istep = 1; istep <= rway.pathData_cnt; istep++)
           line(imgroom, 
                rway.pathData[istep-1].pnt,
                rway.pathData[istep].pnt, 
                Scalar( 80, 80, 0 ), 1, 8, 0 );
          
          rway.pathData_cnt=0;
        }

        arrowedLine(imgroomshadow, 
            Point(acc_vect1_x*gf + 300,acc_vect1_y*gf + 300),
            Point(acc_vect1_x*gf + 300 - 14*cos(acc_angle), acc_vect1_y*gf + 300- 14 * sin(acc_angle) ), 
            Scalar( 255, 255, 255 ), 1, 8, 0, 0.2);
        
//        cout << "Received new Point" << endl;
//        cout << i << " " << acc_vect1_x << " " << acc_vect1_x_old << endl;
        
        acc_vect1_x_old = acc_vect1_x;
        acc_vect1_y_old = acc_vect1_y;
         
//       int thickness = 1;
//       int lineType = 7;
//      
//       circle( imgroom,
//               Point(200,200),
//               150+i,
//               Scalar( 0, 0, 255 ),
//               thickness,
//               lineType );            
           
//  			imshow("Robby", imgroomshadow);
//        
//  			int key = cv::waitKey(25);
//        
//      	key = (key==255) ? -1 : key; //#Solve bug in 3.2.0
//      	if (key>=0)
//      	  break;

        if (mbp == 1)
        { 

          
            arrowedLine(imgroomshadow, 
                Point(mtx,mty),
                Point(mtx - 14*cos(mta), mty - 14 * sin(mta) ), 
                Scalar( 120, 120, 120 ), 1, 8, 0, 0.2);
          
            cscount = calcNewPath(pdata, &length, Point(acc_vect1_x*100.0+300.0,acc_vect1_y*100.0+300.0), acc_angle, Point(mtx,mty), mta, mdir);

            for (int istep = 1; istep <= cscount; istep++)
               line(imgroomshadow, 
                    pdata[istep-1].pnt,
                    pdata[istep].pnt, 
                    Scalar( 0, istep*20+20, 0 ), 1, 8, 0 );

            lastcycleactive = 1;  
        }
        else 
        {

            float length_sum=0;
            struct job jq;
            
            if (lastcycleactive)
            {
             
                data_rec_cnt_old = data_rec_cnt; 
                rway.addPathDest(Point(mtx,mty), mta, mdir, &eggPosition, trackFlag);
            }                       
 
            
            lastcycleactive = 0;
        }
        i++;
    }
}

void *MissionWalkAroundEgg(void *arg)
{
    Point MissionStartPoint = Point(acc_vect1_x*100.0+300.0,acc_vect1_y*100.0+300.0);
    float MissionStartAngle = acc_angle;
    Point2f dest2f;
    Point pos1;
    Point2f start2f,obj2f,dvector;
    float dist;
    float destAngle;
    struct job jq;

    cout << "Mission start" << endl;
    // look for the egg
    int loopcnt=0;
    while ((eggStatus < 2) && (loopcnt < 20))
    { 
      sleep(1); 
      loopcnt++;
    }
    
    if (eggStatus < 2) {
      cout << "Mission No Egg Found" << endl;
      return NULL;
    }
 
    cout << "Mission Egg Found, move 50cm in front of the egg" << endl;
    
    start2f = MissionStartPoint;
    obj2f = eggPosition+Point(300,300);
    dvector = obj2f-start2f;
    dist = norm(dvector) / 100.0;
    
    if (dist > 0.5) {
      pos1 = obj2f - 0.5 / dist * (obj2f-start2f);
      destAngle = atan(dvector.y/dvector.x);
      rway.addPathDest(pos1,destAngle,false,NULL,false);
    }

    while (rway.getStatusBusy()) {
      sleep(1); 
    }

    cout << "Mission Look again at the egg" << endl;

    loopcnt = 0;
    while ((eggStatus < 2) && (loopcnt < 20))
    { 
      sleep(1); 
      loopcnt++;
    }

    if (eggStatus < 2) {
      cout << "Mission No Egg Found" << endl;
      return NULL;
    }

    start2f = Point(acc_vect1_x*100.0+300.0,acc_vect1_y*100.0+300.0);
    obj2f = eggPosition+Point(300,300);
    dvector = obj2f-start2f;
    dist = norm(dvector) / 100.0;

    cout << "Mission Distance to egg: " << dist << endl;

    if (dist < 0.6) {

      cout << "Mission Rotate +90 " << endl;
      jq.id = 1;
      jq.f1 = +1600.0;
      jq.f2 = -1600.0;
      jq.f3 = 200.0;
      rway.addDirectJob(&jq);

      // rotating matrix with the accumulated alpha
      // /x'\    / cos a   -sin a \   /x\
      // |  | =  |                | * | |
      // \y'/    \ sin a   cos a  /   \y/
      
      pos1 = obj2f + 0.5 / dist * Point2f(-dvector.y,dvector.x);
      rway.addPathDest(pos1,destAngle,false,NULL,false);

      pos1 = obj2f + dvector * 0.5 / dist ;
      destAngle = destAngle + PI*0.5;
      rway.addPathDest(pos1,destAngle,false,NULL,false);

      pos1 = obj2f + 0.5 / dist * Point2f(dvector.y,-dvector.x);
      destAngle = destAngle + PI*0.5;
      rway.addPathDest(pos1,destAngle,false,NULL,false);

      rway.addPathDest(MissionStartPoint,destAngle,false,NULL,false);
      jq.id = 4;
      jq.f1 = MissionStartAngle;
      jq.f2 = 0.0;
      jq.f3 = 200.0;
      rway.addDirectJob(&jq);
      
    }

    cout << "Mission done" << endl;
    
}

int main()
{
    int err;

    newpic = 0;
    cout << "Built with OpenCV " << CV_VERSION << endl;

    err = pthread_create(&tid1, NULL, &keythread, NULL);
    if (err != 0){
      printf("\ncan't create thread 1:[%s]", strerror(err));
      return 1 ;
    }

    err = pthread_create(&tid2, NULL, &serialReadT, NULL);
    if (err != 0){
      printf("\ncan't create thread 2:[%s]", strerror(err));
      return 1 ;
    }

    err = pthread_create(&tid3, NULL, &WinPrintWay, NULL);
    if (err != 0){
      printf("\ncan't create thread 3:[%s]", strerror(err));
      return 1 ;
    }

 
    //-- 1. Load the cascades
    //if( !face_cascade.load( face_cascade_name ) ){ printf("--(!)Error loading face cascade\n"); return -1; };
    //if( !eyes_cascade.load( eyes_cascade_name ) ){ printf("--(!)Error loading eyes cascade\n"); return -1; };

 
    dnn_yolo dnnYolo;
    objloc_t locAtPictureTime;
  
    Mat image;
    VideoCapture capture;
    capture.open(0);

    if(capture.isOpened())
    {
//        namedWindow("Sample",WINDOW_AUTOSIZE);
//        namedWindow("Feature",WINDOW_AUTOSIZE);
        namedWindow("Robby",WINDOW_AUTOSIZE);
        
//        createTrackbar( " txTimestep:", "Robby", &txTimestep, 1000, NULL );
//        createTrackbar( " cscount:", "Robby", &precision, 100, NULL );

        createTrackbar( " KP:", "Robby", &KP_user, 1000, &serialWriteT2, &KP_cmd );
        createTrackbar( " KD:", "Robby", &KD_user, 1000, &serialWriteT2, &KD_cmd );
        createTrackbar( " KP_thr:", "Robby", &KP_thr_user, 1000, &serialWriteT2, &KP_thr_cmd );
        createTrackbar( " KI_thr:", "Robby", &KI_thr_user, 1000, &serialWriteT2, &KI_thr_cmd );        

        createTrackbar( " Pthrottle: ", "Robby", &Pthrottle, 1000, NULL );
        createTrackbar( " Psteering: ", "Robby", &Psteering, 1000, NULL );       
        createTrackbar( " Ptiming: ", "Robby", &Ptiming, 1000, NULL ); 
        createTrackbar( " No Recalc: ", "Robby", &NRecalc, 20, NULL ); 

//        createTrackbar( " Canny thresh:", "Feature", &thresh, max_thresh, NULL );
        createTrackbar( " Thresh:", "Feature", &thresh, 200, NULL );

    
        setMouseCallback( "Robby", onMouse, 0 );

        cout << "Capture is opened" << endl;
        capture.set(CAP_PROP_FPS,2);
        
        cout << "FPS " << capture.get(CAP_PROP_FPS) << endl;

        Mat graypic2, dispPic;
        bool headmoveFlag = false;
        bool updateFlag = false;
        
        capture >> dispPicDNN;
                
        int i=0;
        Rect roi;
        int headcmd = 1500;
        
        for(;;)
        {
	      		i++;
            capture >> image;

            if(image.empty())
            {
                cout << "Image Empty." << endl;
                break;
            }

//            graypic = image.clone();
//         		cvtColor(image, graypic, COLOR_BGR2GRAY);
            
            roi.x = (image.size().width - 640)/2;
            roi.y = (image.size().height - 480)/2;
            roi.width = 640;
            roi.height = 480;
            
            dispPic = image(roi).clone();

            if (!dnnYolo.dnnBusy) {                

                imshow("ShowDNN", dispPicDNN);
                dnnYolo.getObjects(eggtree,&updateFlag);
                calcEggPosition(eggtree[0], locAtPictureTime);

                //if (eggtree[0].confidence > 0.4) {
                      //locLastObjDetect = locAtPictureTime;
                //}
                
                if ((eggStatus>0) && (trackFlag) && (!headmoveFlag)) {

                    lookAtObject(Point(acc_vect1_x*100.0+300.0,acc_vect1_y*100.0+300.0), acc_angle, eggPosition+Point(300,300));                  
                    
                    //headcmd = headcmd - (int)(eggPosition.angleXY *2.0 / 90.0 * 800.0);
                    
                    //if ((headcmd > 1200) and (headcmd < 1800)){
                        //serialWrite(2, headcmd, 0);      
                    //} else {
                        //cout << "Invalid headcmd: " << headcmd << endl;
                    //}
                    headmoveFlag = true;
                } else {headmoveFlag = false;}
   
                if (!headmoveFlag) {
                  locAtPictureTime.pos_x = acc_vect1_x;
                  locAtPictureTime.pos_y = acc_vect1_y;
                  locAtPictureTime.angle_xy = acc_angle;
                  locAtPictureTime.headangle = headangle;
                                    
                  // Create a 4D blob from a frame.
                  dispPicDNN = dispPic.clone();      
                  dnnYolo.startDnn(dispPicDNN, angle_z);
                }
            }

           
      			if (0) //i % 5==0)
      			{                
                
                  //graypic2 = graypic.clone();

                  pthread_mutex_lock( &grayimage_mutex );
                  
                  resize(graypic, graypic2, cv::Size(graypic.cols*0.5,graypic.rows*0.5), 0, 0, INTER_LINEAR);
                  pthread_cond_signal( &grayimage_cond );            
                  pthread_mutex_unlock( &grayimage_mutex );                           
                  
      						drawText(graypic2, i, 1.0);
                  
                  int rows = graypic2.rows / 2;
                  int cols = graypic2.cols / 2;
                                
                  line(graypic2, 
                        Point(cols -5, rows),
                        Point(cols +5, rows), 
                        Scalar( 120, 120, 120 ), 1, 8, 0 );

                  line(graypic2, 
                        Point(cols , rows - 5),
                        Point(cols , rows + 5), 
                        Scalar( 120, 120, 120 ), 1, 8, 0 );
                                   
                  if (distm < 1.5)
                  {
                    	std::string s1 = SSTR( distm );
                      putText(graypic2, ":" + s1,
                              Point(cols+5, rows+5),
                              FONT_HERSHEY_COMPLEX, 0.3, // font face and scale
                              Scalar(120, 120, 120), // white
                              1, LINE_AA); // line thickness and type                  
                  }                                   

                  imshow("Sample", graypic2);
                  
      				}

      			if (i % 3==0)
      			{                
//                  if(!imgroomshadow.empty())
//                  {
                      pthread_mutex_lock( &imageshadow_mutex);
                      imshow("Robby", imgroomshadow);
                      pthread_mutex_unlock( &imageshadow_mutex);
//                      imshow("Robby", graypic);
//                  }
            }
            

              //if (newpic==1)
              //{
                  //cout << "Show Feature " << endl;
                  //imshow("Feature", featpic);
                  //newpic = 0;
              //}
	    
//            cout << "imshow maybe done" << endl;
       
      			int key = cv::waitKey(5);
      			key = (key==255) ? -1 : key; //#Solve bug in 3.2.0
 
            struct job jq;
          
            switch (key)
            {
            case 99: //"c"
              cout << "clear image " << key << endl;
              clrscr = 1;
      			  break;

            case 100: //"d"
              mdir=!mdir;            
              cout << "move direct " << mdir << endl;
      			  break;

            case 104: //"h" move heade
            
              cout << "Move Head " << key << endl;

              jq.id = 2;
              jq.f2 = 0.0;
              jq.f3 = 200.0;
              
              for (int j = 0; j <12 ; j++)
              {
                jq.f1 = headpos_cs[j];
                rway.addDirectJob(&jq);
                jq.f1 = (headpos_cs[j] + headpos_cs[j+1]) * 0.5 ;
                rway.addDirectJob(&jq);
              }          
      			  break; 

            case 115: //"s" stop motor now - next job
                         
              cout << "Stop " << key << endl;
              rway.clearPath();
              jq.id = 90;
              jq.f1 = 0.0;
              jq.f2 = 0.0;
              rway.addDirectJob(&jq);
              
      			  break; 

            case 116: //"t" move heade
                trackFlag = !trackFlag;
                cout << "trackFlag " << trackFlag << endl;
                if (trackFlag==0) {
                  jq.id = 2;
                  jq.f1 = 1500.0;
                  jq.f2 = 0.0;
                  jq.f3 = 200.0;
                  rway.addDirectJob(&jq);
                }
                
              break; 

            case 118: //"v" verbose print
              mverbose++;
              if (mverbose>2) mverbose=0;
              cout << "mverbose " << mverbose << endl;
      			  break;

            case 121: //"y" rotate -45 degree
              cout << "Rotate -45 " << key << endl;
              jq.id = 1;
              jq.f1 = -800.0;
              jq.f2 = +800.0;
              jq.f3 = 0.0;
              rway.addDirectJob(&jq);
                          
              break;

            case 120: //"x" rotate +45 degree
              cout << "Rotate +45 " << key << endl;
              jq.id = 1;
              jq.f1 = +800.0;
              jq.f2 = -800.0;
              jq.f3 = 0.0;
              rway.addDirectJob(&jq);
            
              break;

            case 119: //"w" write image
              cout << "Write Image " << key << endl;
              imwrite("picture.bmp", image);
                           
              break;

            case 109: //"m" stepper motor on
              mso=!mso;
              cout << "Stepper On: " << mso << endl;
              jq.id = 3;
              jq.f1 = mso;
              jq.f2 = mso;
              jq.f3 = 0.0;
              rway.addDirectJob(&jq);
            
              break;

            case 110: //"n" new mission
              cout << "Start Mission Thread: " << endl;
              err = pthread_create(&tid4, NULL, &MissionWalkAroundEgg, NULL);
              if (err != 0){
                printf("\ncan't create thread 2:[%s]", strerror(err));
                return 1 ;
              }
            
              break;

            case 97: //"a"
              mcc=!mcc;
              cout << "Automatic control to cursor " << mcc << endl;

      			  break;

            default:
              break;
            }
        }
    }
    else
    {
        cout << "No capture" << endl;
        for(;;)
        {

            WinPrintWay(NULL);

//            usleep(1000);
          
          	int key = cv::waitKey(25);
          	key = (key==255) ? -1 : key; //#Solve bug in 3.2.0
          	if (key>=0)
          	  break;

        }
    }
    return 0;
}

void drawText(Mat & image, int i, double fscale)
{
	std::string s1 = SSTR( i );
	
    putText(image, "Hello OpenCV " + s1,
            Point(20, 50),
            FONT_HERSHEY_COMPLEX, fscale, // font face and scale
            Scalar(255, 255, 255), // white
            1, LINE_AA); // line thickness and type
}

void WinPrintClr(Mat & imgroom, float gf)
{
    imgroom = Mat::zeros(600, 600, CV_8UC3);

    line(imgroom, Point(550-gf-10,580), Point(560,580), Scalar( 255, 255, 255 ), 1, 8, 0 );
    line(imgroom, Point(550-gf,570), Point(550-gf,590), Scalar( 255, 255, 255 ), 1, 8, 0 );
    line(imgroom, Point(550,570), Point(550,590), Scalar( 255, 255, 255 ), 1, 8, 0 );

    putText(imgroom, "1m",
            Point(550-gf/2-10, 570),
            FONT_HERSHEY_PLAIN, 1, // font face and scale
            Scalar(255, 255, 255), // white
            1, LINE_AA); // line thickness and type

    line(imgroom, Point(300,100), Point(300,500), Scalar( 40, 40, 40 ), 1, 8, 0 );
    line(imgroom, Point(200,100), Point(200,500), Scalar( 40, 40, 40 ), 1, 8, 0 );
    line(imgroom, Point(100,100), Point(100,500), Scalar( 40, 40, 40 ), 1, 8, 0 );

    line(imgroom, Point(100,500), Point(500,500), Scalar( 40, 40, 40 ), 1, 8, 0 );
    line(imgroom, Point(100,400), Point(500,400), Scalar( 40, 40, 40 ), 1, 8, 0 );
    line(imgroom, Point(100,300), Point(500,300), Scalar( 40, 40, 40 ), 1, 8, 0 );
    line(imgroom, Point(100,200), Point(500,200), Scalar( 40, 40, 40 ), 1, 8, 0 );
    line(imgroom, Point(100,100), Point(500,100), Scalar( 40, 40, 40 ), 1, 8, 0 );


}






