#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/objdetect.hpp"
#include "opencv2/xfeatures2d.hpp"
#include <iostream>
#include <unistd.h>
#include <string>

#include <X11/Xlib.h>

#include <errno.h>
#include <stdio.h>
#include <string.h>

#include <sstream>

#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
            ( std::ostringstream() << std::dec << x ) ).str()

using namespace cv;
using namespace std;
using namespace cv::xfeatures2d;

extern pthread_mutex_t grayimage_mutex;
extern pthread_cond_t  grayimage_cond;

extern Mat graypic;
extern Mat featpic;

extern int newpic;

extern int thresh;

void *imageProcT(void *arg)
{

    std::vector<Rect> faces;
    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;



    RNG rng(12345);


    Mat img_1,img_2;

    while (1)
    {
  
      pthread_mutex_lock( &grayimage_mutex );
      
      pthread_cond_wait( &grayimage_cond, &grayimage_mutex );
      pthread_mutex_unlock( &grayimage_mutex );
      
      img_1 = graypic.clone();

      usleep(1000000);
      
      pthread_mutex_lock( &grayimage_mutex );
      
      pthread_cond_wait( &grayimage_cond, &grayimage_mutex );
      pthread_mutex_unlock( &grayimage_mutex );

      cout << "Feature Calculation start" << endl;
      
      img_2 = graypic.clone(); 

//#define COLOR_FILTER
//#define HOUGH_CIRCLE
//#define FIND_CONTOURS
//#define BLOB_DETECT
#define IMAGE_SEGMENATION

// ****************************************************************************************** //
//    IMAGE_SEGMENATION
// ****************************************************************************************** //
#ifdef IMAGE_SEGMENATION

    // Create a kernel that we will use to sharpen our image
//  Mat kernel = (Mat_<float>(3,3) <<
//                  1,  1, 1,
//                  1, -8, 1,
//                  1,  1, 1); // an approximation of second derivative, a quite strong kernel
    // do the laplacian filtering as it is
    // well, we need to convert everything in something more deeper then CV_8U
    // because the kernel has some negative values,
    // and we can expect in general to have a Laplacian image with negative values
    // BUT a 8bits unsigned int (the one we are working with) can contain values from 0 to 255
    // so the possible negative number will be truncated
//  Mat imgLaplacian;
//  filter2D(graypic, imgLaplacian, CV_32F, kernel);
//  Mat sharp;
//  graypic.convertTo(sharp, CV_32F);
    Mat imgResult = img_2.clone(); //sharp - imgLaplacian;
    // convert back to 8bits gray scale
    imgResult.convertTo(imgResult, CV_8UC3);
//  imgLaplacian.convertTo(imgLaplacian, CV_8UC3);
    // imshow( "Laplace Filtered Image", imgLaplacian );
    //imshow( "New Sharped Image", imgResult );
    // Create binary image from source image
    Mat bw;
//  cvtColor(imgResult, bw, COLOR_BGR2GRAY);
    threshold(img_2, bw, 40, 255, THRESH_BINARY | THRESH_OTSU);
//  imshow("Binary Image", bw);
    // Perform the distance transform algorithm
    Mat dist;
    distanceTransform(bw, dist, DIST_L2, 3);
    // Normalize the distance image for range = {0.0, 1.0}
    // so we can visualize and threshold it
    normalize(dist, dist, 0, 1.0, NORM_MINMAX);
//  imshow("Distance Transform Image", dist);
    // Threshold to obtain the peaks
    // This will be the markers for the foreground objects
    threshold(dist, dist, 0.4, 1.0, THRESH_BINARY);
    // Dilate a bit the dist image
    Mat kernel1 = Mat::ones(3, 3, CV_8U);
    dilate(dist, dist, kernel1);
//  imshow("Peaks", dist);
    // Create the CV_8U version of the distance image
    // It is needed for findContours()
    Mat dist_8u;
    dist.convertTo(dist_8u, CV_8U);
    // Find total markers
    vector<vector<Point> > contours;
    findContours(dist_8u, contours, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
    // Create the marker image for the watershed algorithm
    Mat markers = Mat::zeros(dist.size(), CV_32S);
    // Draw the foreground markers
    for (size_t i = 0; i < contours.size(); i++)
    {
        drawContours(markers, contours, static_cast<int>(i), Scalar(static_cast<int>(i)+1), -1);
    }
    // Draw the background marker
    circle(markers, Point(5,5), 3, Scalar(255), -1);
//  imshow("Markers", markers*10000);
    // Perform the watershed algorithm
 /*   watershed(imgResult, markers);
    Mat mark;
    markers.convertTo(mark, CV_8U);
    bitwise_not(mark, mark);
    //    imshow("Markers_v2", mark); // uncomment this if you want to see how the mark
    // image looks like at that point
    // Generate random colors
    vector<Vec3b> colors;
    for (size_t i = 0; i < contours.size(); i++)
    {
        int b = theRNG().uniform(0, 256);
        int g = theRNG().uniform(0, 256);
        int r = theRNG().uniform(0, 256);
        colors.push_back(Vec3b((uchar)b, (uchar)g, (uchar)r));
    }
    // Create the result image
    featpic = Mat::zeros(markers.size(), CV_8UC3);
    // Fill labeled objects with random colors
    for (int i = 0; i < markers.rows; i++)
    {
        for (int j = 0; j < markers.cols; j++)
        {
            int index = markers.at<int>(i,j);
            if (index > 0 && index <= static_cast<int>(contours.size()))
            {
                featpic.at<Vec3b>(i,j) = colors[index-1];
            }
        }
    }
 */   // Visualize the final image
 
      featpic = dist.clone();
//  imshow("Final Result", dst);


      cout << "Feature Calculation done" << endl;
      newpic = 1;

#endif

// ****************************************************************************************** //
//    Color Filter
// ****************************************************************************************** //
#ifdef COLOR_FILTER

      // Convert input image to HSV
      Mat hsv_image;
      double largest_area = 0.0;
      int largest_contour_index=0;
      Rect bounding_rect;      
      RotatedRect rot_rect;
      
      cvtColor(img_2, hsv_image, COLOR_BGR2HSV);
      
      //threshold the HSV image, keep only the red pixels
      Mat lower_red_hue_range;
      Mat upper_red_hue_range;
      inRange(hsv_image, cv::Scalar(80, 70, 80), cv::Scalar(100 /*10*/, 160, 110 ), lower_red_hue_range);
      //inRange(hsv_image, cv::Scalar(160, 100, 100), cv::Scalar(179, 255, 255), upper_red_hue_range)
      //inRange(hsv_image, cv::Scalar(200, 100, 100), cv::Scalar(200, 255, 255), upper_red_hue_range);
            
      //addWeighted(lower_red_hue_range, 1.0, upper_red_hue_range, 1.0, 0.0, featpic);

      featpic = lower_red_hue_range.clone();
			Canny(featpic, featpic, 0, 30, 3);

      findContours( featpic, contours, hierarchy, RETR_CCOMP, CHAIN_APPROX_SIMPLE, Point(0, 0) ); 

      for( size_t iz = 0; iz< contours.size(); iz++ )
         {
           Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
           drawContours( featpic, contours, (int)iz, color, FILLED, 8, hierarchy, 0, Point() );
           
            //  Find the area of contour
            double a=contourArea( contours[iz],false); 
            if(a>largest_area)
            {
              // Store the index of largest contour
              largest_contour_index=iz;   
              largest_area = a;            
            }
         }

      // Find the bounding rectangle for biggest contour
      bounding_rect=boundingRect(contours[largest_contour_index]);
      rectangle(featpic, bounding_rect,  Scalar(80,80,80),2, 8,0);        

      cout << "Feature Calculation done" << endl;
      newpic = 1;
      
#endif      

// ****************************************************************************************** //
//    Canny, findContours
// ****************************************************************************************** //
#ifdef FIND_CONTOURS

      Mat edges;
      double largest_area = 0.0;
      int largest_contour_index=0;
      Rect bounding_rect;      
      RotatedRect rot_rect;

      GaussianBlur(img_2, edges, Size(7,7), 1.5, 1.5);
			Canny(edges, edges, 0, thresh, 3);
      //findContours( edges, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE, Point(0, 0) );
      findContours( edges, contours, hierarchy, RETR_CCOMP, CHAIN_APPROX_SIMPLE, Point(0, 0) ); 

      featpic = edges.clone();

      for( size_t iz = 0; iz< contours.size(); iz++ )
         {
           Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
           drawContours( featpic, contours, (int)iz, color, FILLED, 8, hierarchy, 0, Point() );
           
            //  Find the area of contour
            double a=contourArea( contours[iz],false); 
            if(a>largest_area)
            {
              // Store the index of largest contour
              largest_contour_index=iz;   
              largest_area = a;            
            }
         }

      // Find the bounding rectangle for biggest contour
      bounding_rect=boundingRect(contours[largest_contour_index]);
      rectangle(featpic, bounding_rect,  Scalar(80,80,80),2, 8,0);        

      rot_rect=minAreaRect(contours[largest_contour_index]);
      Point2f vertices[4];
      rot_rect.points(vertices);
      for (int i = 0; i < 4; i++)
        line(featpic, vertices[i], vertices[(i+1)%4], Scalar(80,80,80), 2);
      
      printf("Rectbox  # : %d \n", largest_contour_index );
      printf("contours # : %d \n", contours.size() );

      cout << "Feature Calculation done" << endl;
      newpic = 1;

#endif

// ****************************************************************************************** //
//    ORB Detector and Flann Matcher
// ****************************************************************************************** //
#ifdef ORB_FLANN

      if( !img_1.data || !img_2.data )
      { std::cout<< " --(!) Error reading images " << std::endl; break ; }
      //-- Step 1: Detect the keypoints using SURF Detector, compute the descriptors
           
      
      Ptr<ORB> detector = ORB::create();
      detector->setEdgeThreshold(thresh);
      
      std::vector<KeyPoint> keypoints_1, keypoints_2;

      Mat descriptors_1, descriptors_2;
      
      detector->detectAndCompute( img_1, Mat(), keypoints_1, descriptors_1 );
      detector->detectAndCompute( img_2, Mat(), keypoints_2, descriptors_2 );

      //-- Step 2: Matching descriptor vectors using FLANN matcher
      
      FlannBasedMatcher matcher(new flann::LshIndexParams(20, 10, 2));
      std::vector< DMatch > matches;
      matcher.match( descriptors_1, descriptors_2, matches );
      double max_dist = 0; double min_dist = 100;
      //-- Quick calculation of max and min distances between keypoints
      for( int i = 0; i < descriptors_1.rows; i++ )
      { double dist = matches[i].distance;
      if( dist < min_dist ) min_dist = dist;
      if( dist > max_dist ) max_dist = dist;
      }
      printf("-- Max dist : %f \n", max_dist );
      printf("-- Min dist : %f \n", min_dist );
      printf("descr_1 #   : %d \n", descriptors_1.rows);
      printf("keypoints1# : %d \n", keypoints_1.size() );
      //-- Draw only "good" matches (i.e. whose distance is less than 2*min_dist,
      //-- or a small arbitary value ( 0.02 ) in the event that min_dist is very
      //-- small)
      //-- PS.- radiusMatch can also be used here.
      std::vector< DMatch > good_matches;
      for( int i = 0; i < descriptors_1.rows; i++ )
      { if( matches[i].distance <= max(2*min_dist, 0.02) )
      { good_matches.push_back( matches[i]); }
      }
      //-- Draw only "good" matches
      Mat img_matches;
      drawMatches( img_1, keypoints_1, img_2, keypoints_2,
                 good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
                 vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );
      //-- Show detected matches
      for( int i = 0; i < (int)good_matches.size(); i++ )
      { printf( "-- Good Match [%d] Keypoint 1: %d  -- Keypoint 2: %d  \n", i, good_matches[i].queryIdx, good_matches[i].trainIdx ); }

      featpic = img_matches.clone();

      cout << "Feature Calculation done" << endl;

      newpic = 1;

#endif

// ****************************************************************************************** //
//    HOUGH_CIRCLE
// ****************************************************************************************** //
           
#ifdef HOUGH_CIRCLE

    Mat edges;

    GaussianBlur(img_2, edges, Size(7,7), 1.5, 1.5);
//    medianBlur(img_2, edges, 5);

    vector<Vec3f> circles;
    HoughCircles(edges, circles, HOUGH_GRADIENT, 1,
                 edges.rows/16,  // change this value to detect circles with different distances to each other
                 thresh, 30, 10, 100); // change the last two parameters
            // (min_radius & max_radius) to detect larger circles

    featpic=edges.clone();

    for( size_t i = 0; i < circles.size(); i++ )
    {
        Vec3i c = circles[i];
        Point center = Point(c[0], c[1]);
        // circle outline
        int radius = c[2];
        circle( featpic, center, radius, Scalar(255,0,255), 3, LINE_AA);
    }

      cout << "Feature Calculation done" << endl;

      newpic = 1;

#endif            
            
//                graypic = edges.clone();
     
//                equalizeHist( edges, edges ); 
//        			  face_cascade.detectMultiScale( edges, faces, 1.1, 2, 0|CASCADE_SCALE_IMAGE, Size(30, 30) );
//                for( size_t i = 0; i < faces.size(); i++ )
//                {
//                    Point center( faces[i].x + faces[i].width/2, faces[i].y + faces[i].height/2 );
//                    ellipse( edges, center, Size( faces[i].width/2, faces[i].height/2), 0, 0, 360, Scalar( 255, 0, 255 ), 4, 8, 0 );
//            
//                    Mat faceROI = edges( faces[i] );
//                    std::vector<Rect> eyes;
//            
//                    //-- In each face, detect eyes
//                    eyes_cascade.detectMultiScale( faceROI, eyes, 1.1, 2, 0 |CASCADE_SCALE_IMAGE, Size(30, 30) );
//            
//                    for( size_t j = 0; j < eyes.size(); j++ )
//                    {
//                        Point eye_center( faces[i].x + eyes[j].x + eyes[j].width/2, faces[i].y + eyes[j].y + eyes[j].height/2 );
//                        int radius = cvRound( (eyes[j].width + eyes[j].height)*0.25 );
//                        circle( edges, eye_center, radius, Scalar( 255, 0, 0 ), 4, 8, 0 );
//                    }
//                }
  

  }  
}
