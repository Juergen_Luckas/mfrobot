#include <errno.h>
#include <stdio.h>
#include <string.h>
#include "queue_job.h"

//int orderbook(struct orderbook *const q, const unsigned int slots)
//{
    //if (!q || slots < 1U)
        //return errno = EINVAL;

    //q->jqueue = malloc(sizeof (struct order) * (size_t)(slots + 1));
    //if (!q->jqueue)
        //return errno = ENOMEM;

    //q->size = slots + 1U; 
    //q->head = 0U;
    //q->tail = 0U;

////    pthread_mutex_init(&q->lock, NULL);
////    pthread_cond_init(&q->wait_room, NULL);
////    pthread_cond_init(&q->wait_data, NULL);

    //return 0;
//}

//int orderbook_currentUsage(struct orderbook *const q)
//{
    //int size;
    //pthread_mutex_lock(&q->lock);
    //size = (q->head + q->size - q->tail) % q->size;
    //pthread_mutex_unlock(&q->lock);
    //return size;
//}

//void orderbook_get(struct orderbook *const q, struct order *const j)
//{

    //pthread_mutex_lock(&q->lock);
    //while (q->head == q->tail)
         //pthread_cond_wait(&q->wait_data, &q->lock);

    //*j = q->jqueue[q->tail];
    //q->tail = (q->tail + 1U) % q->size;

    //pthread_cond_signal(&q->wait_room);

    //pthread_mutex_unlock(&q->lock);
    //return;
//}

//void orderbook_put(struct orderbook *const q, struct order *const j)
//{
    //pthread_mutex_lock(&(q->lock));
    //while ((q->head + 1U) % q->size == q->tail)
        //pthread_cond_wait(&(q->wait_room), &(q->lock));

    //q->jqueue[q->head] = *j;
    //q->head = (q->head + 1U) % q->size;

    //pthread_cond_signal(&q->wait_data);

    //pthread_mutex_unlock(&q->lock);
    //return;
//}

//void orderbook_clear(struct orderbook *const q)
//{
    //pthread_mutex_lock(&(q->lock));
    //q->tail = q->head;

    //pthread_cond_signal(&q->wait_room);
    //pthread_mutex_unlock(&q->lock);
    //return;
//}

// queue starts here
int queue_init(struct queue *const q, const unsigned int slots)
{
    if (!q || slots < 1U)
        return errno = EINVAL;

    q->jqueue = malloc(sizeof (struct job) * (size_t)(slots + 1));
    if (!q->jqueue)
        return errno = ENOMEM;

    q->size = slots + 1U; 
    q->head = 0U;
    q->tail = 0U;

//    pthread_mutex_init(&q->lock, NULL);
//    pthread_cond_init(&q->wait_room, NULL);
//    pthread_cond_init(&q->wait_data, NULL);

    return 0;
}

int queue_currentUsage(struct queue *const q)
{
    int size;
    pthread_mutex_lock(&q->lock);
    size = (q->head + q->size - q->tail) % q->size;
    pthread_mutex_unlock(&q->lock);
    return size;
}

void queue_get(struct queue *const q, struct job *const j)
{

    pthread_mutex_lock(&q->lock);
    while (q->head == q->tail)
         pthread_cond_wait(&q->wait_data, &q->lock);

    *j = q->jqueue[q->tail];
    q->tail = (q->tail + 1U) % q->size;

    pthread_cond_signal(&q->wait_room);

    pthread_mutex_unlock(&q->lock);
    return;
}

void queue_put(struct queue *const q, struct job *const j)
{
    pthread_mutex_lock(&(q->lock));
    while ((q->head + 1U) % q->size == q->tail)
        pthread_cond_wait(&(q->wait_room), &(q->lock));

    q->jqueue[q->head] = *j;
    q->head = (q->head + 1U) % q->size;

    pthread_cond_signal(&q->wait_data);

    pthread_mutex_unlock(&q->lock);
    return;
}

void queue_clear(struct queue *const q)
{
    pthread_mutex_lock(&(q->lock));
    q->tail = q->head;

    pthread_cond_signal(&q->wait_room);
    pthread_mutex_unlock(&q->lock);
    return;
}
