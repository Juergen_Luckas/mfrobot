#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/dnn/dnn.hpp>
#include "dnn_yolo.hpp"

#include <opencv2/core/utility.hpp>
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <string>
#include <math.h>

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>

#include <sstream>

#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
            ( std::ostringstream() << std::dec << x ) ).str()

using namespace cv;
using namespace std;
using namespace cv::dnn;

// FOV 0.5 Horizontal; old 22.5
#define FOVH05   21.8
// FOV 0.5 Vertical; old 26.5
#define FOVV05   25.5
#define CAMANG    4.0
#define CAMHIGHT 20.0

typedef void * (*THREADFUNCPTR)(void *);

dnn_yolo::dnn_yolo()
{
    int err;

    inpWidth = 416;        // Width of network's input image
    inpHeight = 416;       // Height of network's input image
    confThreshold = 0.45; // Confidence threshold
    nmsThreshold = 0.4; // Non-maximum suppression threshold
    modelConfiguration = "../tools/yolodata/eggtree-yolov3-tiny.cfg";
    modelWeights = "../tools/yolodata/eggtree-yolov3-tiny_8000.weights";
    
    dnnBusy = true;
          
    err = pthread_create(&threadIdDNN, NULL, (THREADFUNCPTR) &imageProcDnnThread, this);
    if (err != 0){
      printf("\ncan't create DNN thread :[%s]", strerror(err));
    }
    
}

void dnn_yolo::startDnn(Mat imgForDNN, float angleZ)
{
    if (!dnnBusy)
    {
        dnnBusy = true;
        // Create a 4D blob from a frame.
        blobFromImage(imgForDNN, blob, 1/255.0, Size(inpWidth, inpHeight), Scalar(0,0,0), true, false);
        imageForDNN = imgForDNN;
        imageAngle = angleZ;
        
        objectList[0] = {0.0,0.0,0.0};
        objectList[1] = {0.0,0.0,0.0};
                        
        pthread_cond_signal( &imageDNN_cond );            
        pthread_mutex_unlock( &imageDNN_mutex );                           
    } 
}

void dnn_yolo::getObjects(obj_spec_t *objList, bool *updateFlag)
{
    if (!dnnBusy)
    {
        objList[0] = objectList[0];    
        objList[1] = objectList[1];
        *updateFlag = true;        
    } else {
        *updateFlag = false;
    }     
}

// Get the names of the output layers
vector<String> dnn_yolo::getOutputsNames(const Net& net)
{
    static vector<String> names;
    if (names.empty())
    {
        //Get the indices of the output layers, i.e. the layers with unconnected outputs
        vector<int> outLayers = net.getUnconnectedOutLayers();
         
        //get the names of all the layers in the network
        vector<String> layersNames = net.getLayerNames();
         
        // Get the names of the output layers in names
        names.resize(outLayers.size());
        for (size_t i = 0; i < outLayers.size(); ++i)
        names[i] = layersNames[outLayers[i] - 1];
    }
    return names;
}

// Draw the predicted bounding box
void dnn_yolo::drawPred(int classId, float conf, int left, int top, int right, int bottom, Mat& frame)
{
    //Draw a rectangle displaying the bounding box
    rectangle(frame, Point(left, top), Point(right, bottom), Scalar(0, 0, 255));
     
    //Get the label for the class name and its confidence
    string label = format("%.2f", conf);
    label = to_string(classId) + ":" + label;
     
    //Display the label at the top of the bounding box
    int baseLine;
    Size labelSize = getTextSize(label, FONT_HERSHEY_SIMPLEX, 0.5, 1, &baseLine);
    top = max(top, labelSize.height);
    putText(frame, label, Point(left, top), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(255,255,255));
}

// Remove the bounding boxes with low confidence using non-maxima suppression
void dnn_yolo::postprocess(Mat& frame, const vector<Mat>& outs)
{
    vector<int> classIds;
    vector<float> confidences;
    vector<Rect> boxes;
     
    for (size_t i = 0; i < outs.size(); ++i)
    {
        // Scan through all the bounding boxes output from the network and keep only the
        // ones with high confidence scores. Assign the box's class label as the class
        // with the highest score for the box.
        float* data = (float*)outs[i].data;
        for (int j = 0; j < outs[i].rows; ++j, data += outs[i].cols)
        {
            Mat scores = outs[i].row(j).colRange(5, outs[i].cols);
            Point classIdPoint;
            double confidence;
            // Get the value and location of the maximum score
            minMaxLoc(scores, 0, &confidence, 0, &classIdPoint);
            if (confidence > confThreshold)
            {
                int left = (int)(data[0] * frame.cols);
                int top = (int)(data[1] * frame.rows);
                int width = (int)(data[2] * frame.cols);
                int height = (int)(data[3] * frame.rows);
               
                //cout << data[0]+data[2]*0.5 << " " << data[1]+data[3]*0.5 << " " << data[3] << " " << data[3]/data[2] << " " << endl;                
                
                float tiltangle = CAMANG - imageAngle; 
                float tanp = tan(PI/180.0 * (FOVV05 + tiltangle));
                float tanm = tan(PI/180.0 * (FOVV05 - tiltangle));
                float cos2 = cos(PI/180.0 * tiltangle) * cos(PI/180.0 * tiltangle);
                float y = data[1]+data[3];
                float x = data[0]+0.5*data[2];
                
                float distanceB = CAMHIGHT / tanp; 
                float distanceD = CAMHIGHT *(1.0-y) * (1.0+tanm/tanp) / (y*tanp*cos2 - (1.0-y)*tanm);
                //float distance2 = 6.0 / tan(data[3]/0.5*22.5 * PI/180.0);
                float angle = atan(2.0*(x-0.5)*tan(FOVH05 * PI/180.0));
                
                if (objectList[classIdPoint.x].confidence < confidence) {
                    objectList[classIdPoint.x].confidence = confidence;
                    objectList[classIdPoint.x].distance = distanceB + distanceD;
                    objectList[classIdPoint.x].angleXY = angle;
                }
                 
                classIds.push_back(classIdPoint.x);
                confidences.push_back((float)confidence);
                boxes.push_back(Rect(left, top, width, height));
            }
        }
    }
     
    // Perform non maximum suppression to eliminate redundant overlapping boxes with
    // lower confidences
    vector<int> indices;
    NMSBoxes(boxes, confidences, confThreshold, nmsThreshold, indices);
    for (size_t i = 0; i < indices.size(); ++i)
    {
        int idx = indices[i];
        Rect box = boxes[idx];
        drawPred(classIds[idx], confidences[idx], box.x, box.y,
                 box.x + box.width, box.y + box.height, frame);
    }
}

void *dnn_yolo::imageProcDnnThread(void *arg)
{
    dnn_yolo *pThis = (dnn_yolo*) arg;
    Net net = readNetFromDarknet(pThis->modelConfiguration, pThis->modelWeights);
    net.setPreferableBackend(DNN_BACKEND_OPENCV);
    net.setPreferableTarget(DNN_TARGET_CPU);
    pThis->dnnBusy = false;   
    
    while (1)
    {
        pthread_mutex_lock( &pThis->imageDNN_mutex );
        pthread_cond_wait( &pThis->imageDNN_cond, &pThis->imageDNN_mutex );
        pthread_mutex_unlock( &pThis->imageDNN_mutex );
 
        net.setInput(pThis->blob);
        // Runs the forward pass to get output of the output layers
        vector<Mat> outs;
        net.forward(outs, pThis->getOutputsNames(net));
        pThis->postprocess(pThis->imageForDNN, outs);
     
        pThis->dnnBusy = false;        
    }
}







