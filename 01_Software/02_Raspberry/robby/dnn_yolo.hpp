#include <sys/time.h>
#include <pthread.h>


#ifndef DNN_YOLO_H
#define DNN_YOLO_H

using namespace cv;
using namespace std;
using namespace cv::dnn;

#define PI 3.14159265359

#define DNN_MAX_CLASSES 2

// Initialize the parameters

typedef struct{
    float confidence;
    float distance;
    float angleXY;
} obj_spec_t;

class dnn_yolo {      // The class
  public:             // Access specifier
    dnn_yolo();
    bool dnnBusy;
    void getObjects(obj_spec_t objList[], bool *updateFlag);
    void startDnn(Mat imageForDNN, float angleZ);
  
  private:
    int inpWidth;        // Width of network's input image
    int inpHeight;       // Height of network's input image
    float confThreshold; // Confidence threshold
    float nmsThreshold; // Non-maximum suppression threshold
    float imageAngle;
    obj_spec_t objectList[DNN_MAX_CLASSES];

    Mat blob;
    Mat imageForDNN;
    string modelConfiguration;
    string modelWeights;
    pthread_mutex_t imageDNN_mutex = PTHREAD_MUTEX_INITIALIZER;
    pthread_cond_t  imageDNN_cond  = PTHREAD_COND_INITIALIZER;
    pthread_t threadIdDNN;
    
    static void *imageProcDnnThread(void *arg);
    vector<String> getOutputsNames(const Net& net);
    void drawPred(int classId, float conf, int left, int top, int right, int bottom, Mat& frame);
    void postprocess(Mat& frame, const vector<Mat>& outs);      
};

#endif







