#include <pthread.h>

#ifndef QUEUE_JOB_H
#define QUEUE_JOB_H

struct job
{
    int    id;
    float  f1;
    float  f2;
    float  f3;
}; /* Up to you */

//struct order
//{
    //struct job j;
    //Point destPos;
    //float destAngle;
    //bool  rotateAtStart;
    //Point *objectPtr;
    //bool  tracking;
//}; /* Up to you */

struct queue {
    pthread_mutex_t   lock;
    pthread_cond_t    wait_room;
    pthread_cond_t    wait_data;
    unsigned int      size;
    unsigned int      head;
    unsigned int      tail;
    struct job        *jqueue;
};

typedef struct queue queue_t;

//struct orderbook {
    //pthread_mutex_t   lock;
    //pthread_cond_t    wait_room;
    //pthread_cond_t    wait_data;
    //unsigned int      size;
    //unsigned int      head;
    //unsigned int      tail;
    //struct order      *jqueue;
//};

//int orderbook_init(struct orderbook *const q, const unsigned int slots);

//int orderbook_currentUsage(struct orderbook *const q);

//void orderbook_get(struct orderbook *const q, struct order *const j);

//void orderbook_put(struct orderbook *const q, struct order *const j);

//void orderbook_clear(struct orderbook *const q);


int queue_init(struct queue *const q, const unsigned int slots);

int queue_currentUsage(struct queue *const q);

void queue_get(struct queue *const q, struct job *const j);

void queue_put(struct queue *const q, struct job *const j);

void queue_clear(struct queue *const q);

#endif
