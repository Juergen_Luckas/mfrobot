#include <sys/time.h>
#include <pthread.h>

extern "C" {
#include "queue_job.h"
}
#include "sharedqueue.hpp"

#ifndef WAY_CTRL_H
#define WAY_CTRL_H

using namespace cv;
using namespace std;

#define PI 3.14159265359

struct pathdata_st{
float norm;
float alpha;
float dalpha;
Point pnt;
};

typedef struct pathdata_st pathdata_t[50]; 

// Initialize the parameters

int calcNewPath(pathdata_t pd, float *length, Point startPos, float startAngle, Point destPos, float destAngle, bool rotateAtStart);

void lookAtObject(Point currentPos, float currentAngle, Point objectPos);

typedef struct{
    struct job j;
    Point destPos;
    float destAngle;
    bool  rotateAtStart;
    Point *objectPtr;
    bool  tracking;
} order_t;


class way_ctrl {      // The class
  public:             // Access specifier
    way_ctrl();
    pathdata_t pathData;
    int pathData_cnt;

    void addPathDest(Point destPos, float destAngle, bool rotateAtStart, Point *objectPtr, bool tracking);
    void addDirectJob(struct job *const jq);
    void clearPath(void);
    bool getStatusBusy(void);
   
  private:

    queue_t      q_msg2ser;
    bool         objectTrack;
    Point        objectPos;
    Point        destPos;
    float        destAngle;
    SharedQueue<order_t> orderbook;
    mutex        mutx;

    pthread_mutex_t wayCtrl_mutex = PTHREAD_MUTEX_INITIALIZER;
    pthread_cond_t  wayCtrl_cond  = PTHREAD_COND_INITIALIZER;
    pthread_t threadIdWayCtrl;
    
    static void *pathStepThread(void *arg);
    void recalcPathDest(Point destPos, float destAngle, bool rotateAtStart);

};

#endif







