= UserGuide Vision Robot
Markus Foerstel <qbert@qbert.de>
Version 1.0, 20.12.2017
:toc:
:icons: font
:description: First steps to get the image detection going
:imagesdir: ./images
:quick-uri: http://asciidoctor.org/docs/asciidoc-syntax-quick-reference/

Content entered directly below the header but before the first section heading is called the preamble.

== Ruby Installation
Download the Windows install package to get Ruby installed.
https://rubyinstaller.org/2017/09/15/rubyinstaller-2.4.2-2-released.html
Form version 1.9 onwards Ruby contains the Gem installer as part of the installation package.
The installation requires the Antivirus program to be deactivated.
If not deactivated the Ruby installation will terminate throughout the process.

Given the insatallation options below:
[%hardbreaks]
1 - MSYS2 base installation
2 - MSYS2 system update
3 - MSYS2 and MINGW development toolchain
The base installation option was choosen.
   
WARNING: Add installation path up to including bin folder to system path. This should probably not be necessary and was caused by the Antivirus interrupting the installation process.
   
A "successful" installation can be checked by entering the following command in the DOS box:

 ruby -version
   
As a result the installed Ruby version should be printed. 

An `echo %PATH%"` should also show the path the the Ruby installation folder.
   
If sucessful install the AsciiDoctor gem.
   
   C:\SW\01_Robot>gem install asciidoctor
   Fetching: asciidoctor-1.5.6.1.gem (100%)
   Successfully installed asciidoctor-1.5.6.1
   Parsing documentation for asciidoctor-1.5.6.1
   Installing ri documentation for asciidoctor-1.5.6.1
   Done installing documentation for asciidoctor after 3 seconds
   1 gem installed
   
This is a paragraph with a *bold* word and an _italicized_ word.

.Image caption
image::image-file-name.png[I am the image alt text.]

This is another paragraph.footnote:[I am footnote text and will be displayed at the bottom of the article.]

== OpenCV installation

https://docs.opencv.org/2.4/doc/tutorials/introduction/windows_install/windows_install.html

http://jeanvitor.com/installing-cpp-opencv-3-2/

There is an error message saying that the  sh.exe shall not be in the path.
Therefor a shell (DOS box) is launched. The following commands remove the path to the sh.exe from the PATH.

TIP: The unwanted path is shown in the error text in C-Make


	C:\SW\01_Robot>echo %PATH%
	C:\ProgramData\Oracle\Java\javapath;C:\Program Files\Common Files\Microsoft Shared\Windows Live;C:\Windows\system32;C:\Windows;C:\Windows\System32\Wbe
	m;C:\Windows\System32\WindowsPowerShell\v1.0\;C:\Program Files\Intel\WiFi\bin\;C:\Program Files\Common Files\Intel\WirelessCommon\;C:\Program Files\Eg
	isTec BioExcess\;c:\Program Files\Microsoft SQL Server\100\Tools\Binn\;c:\Program Files\Microsoft SQL Server\100\DTS\Binn\;C:\Program Files\Skype\Phon
	e\;C:\Program Files\Git\cmd;C:\Program Files\Google\Cloud SDK\google-cloud-sdk\bin;C:\Program Files\GtkSharp\2.12\bin;C:\Program Files\NVIDIA Corporat
	ion\PhysX\Common;C:\Android;C:\Program Files\nodejs\;C:\Program Files\MySQL\MySQL Utilities 1.6\;C:\Program Files\GitExtensions\;C:\Program Files\Comm
	on Files\Microsoft Shared\Windows Live;C:\Program Files\Intel\WiFi\bin\;C:\Program Files\Common Files\Intel\WirelessCommon\;C:\Program Files\Android\a
	ndroid-sdk\platform-tools;C:\Program Files\Java\jdk1.8.0_111\bin;C:\Users\Qbert\AppData\Roaming\npm;C:\Program Files\Android\android-sdk\tools;C:\Prog
	ram Files\Java\jre1.8.0_111\bin;C:\SW\gradle-3.2\bin;C:\SW\MinGW\msys\1.0\bin;C:\SW\apache-ant-1.10.1\bin;C:\Ruby24\bin

	C:\SW\01_Robot>set PATH=%PATH:C:\SW\MinGW\msys\1.0\bin;=%

	C:\SW\01_Robot>echo %PATH%
	C:\ProgramData\Oracle\Java\javapath;C:\Program Files\Common Files\Microsoft Shared\Windows Live;C:\Windows\system32;C:\Windows;C:\Windows\System32\Wbe
	m;C:\Windows\System32\WindowsPowerShell\v1.0\;C:\Program Files\Intel\WiFi\bin\;C:\Program Files\Common Files\Intel\WirelessCommon\;C:\Program Files\Eg
	isTec BioExcess\;c:\Program Files\Microsoft SQL Server\100\Tools\Binn\;c:\Program Files\Microsoft SQL Server\100\DTS\Binn\;C:\Program Files\Skype\Phon
	e\;C:\Program Files\Git\cmd;C:\Program Files\Google\Cloud SDK\google-cloud-sdk\bin;C:\Program Files\GtkSharp\2.12\bin;C:\Program Files\NVIDIA Corporat
	ion\PhysX\Common;C:\Android;C:\Program Files\nodejs\;C:\Program Files\MySQL\MySQL Utilities 1.6\;C:\Program Files\GitExtensions\;C:\Program Files\Comm
	on Files\Microsoft Shared\Windows Live;C:\Program Files\Intel\WiFi\bin\;C:\Program Files\Common Files\Intel\WirelessCommon\;C:\Program Files\Android\a
	ndroid-sdk\platform-tools;C:\Program Files\Java\jdk1.8.0_111\bin;C:\Users\Qbert\AppData\Roaming\npm;C:\Program Files\Android\android-sdk\tools;C:\Prog
	ram Files\Java\jre1.8.0_111\bin;C:\SW\gradle-3.2\bin;C:\SW\apache-ant-1.10.1\bin;C:\Ruby24\bin

Run the CMake-gui in the command window with the truncated PATH.

  C:\SW\01_Robot>"C:\Program Files\CMake\bin\cmake-gui.exe"

.cmake step 1
[#img-setup1]
[caption="Figure 1: ",link=http://www.flickr.com/photos/javh/5448336655]
image::cmake_setup1.png[Setup1,500,300]

Make sure to avoid compilation errors by deselecting the precompiled headers.
TIP: PRECOMPILED_HEADERS is un-ticked.

.cmake step 2
[#img-setup2]
[caption="Figure 1: ",link=http://www.flickr.com/photos/javh/5448336655]
image::cmake_setup2.png[Setup2,500,300]

Otherwise the following error shows up.

	...
	[ 32%] Building CXX object modules/core/CMakeFiles/opencv_core.dir/stat.avx2.cpp.obj
	[ 32%] Building RC object modules/core/CMakeFiles/opencv_core.dir/vs_version.rc.obj
	C:\SW\MinGW\bin\windres.exe: invalid option -- W
	Usage: C:\SW\MinGW\bin\windres.exe [option(s)] [input-file] [output-file]
	 The options are:
	  -i --input=<file>            Name input file
	  -o --output=<file>           Name output file
	  -J --input-format=<format>   Specify input format
	  -O --output-format=<format>  Specify output format
	  -F --target=<target>         Specify COFF target
		 --preprocessor=<program>  Program to use to preprocess rc file
		 --preprocessor-arg=<arg>  Additional preprocessor argument
	  -I --include-dir=<dir>       Include directory when preprocessing rc file
	  -D --define <sym>[=<val>]    Define SYM when preprocessing rc file
	  -U --undefine <sym>          Undefine SYM when preprocessing rc file
	  -v --verbose                 Verbose - tells you what it's doing
	  -c --codepage=<codepage>     Specify default codepage
	  -l --language=<val>          Set language when reading rc file
		 --use-temp-file           Use a temporary file instead of popen to read
								   the preprocessor output
		 --no-use-temp-file        Use popen (default)
	  -r                           Ignored for compatibility with rc
	  @<file>                      Read options from <file>
	  -h --help                    Print this help message
	  -V --version                 Print version information
	FORMAT is one of rc, res, or coff, and is deduced from the file name
	extension if not specified.  A single file name is an input file.
	No input-file is stdin, default rc.  No output-file is stdout, default rc.
	C:\SW\MinGW\bin\windres.exe: supported targets: pe-i386 pei-i386 elf32-i386 elf32-little elf32-big plugin srec symbolsrec verilog tekhex binary ihex
	modules\core\CMakeFiles\opencv_core.dir\build.make:1688: recipe for target 'modules/core/CMakeFiles/opencv_core.dir/vs_version.rc.obj' failed
	mingw32-make[2]: *** [modules/core/CMakeFiles/opencv_core.dir/vs_version.rc.obj] Error 1
	CMakeFiles\Makefile2:1667: recipe for target 'modules/core/CMakeFiles/opencv_core.dir/all' failed
	mingw32-make[1]: *** [modules/core/CMakeFiles/opencv_core.dir/all] Error 2
	Makefile:161: recipe for target 'all' failed
	mingw32-make: *** [all] Error 2

There are more compilation problems:

  C:\SW\OpenCV\modules\ts\include\opencv2\ts\ts_gtest.h

Uses _stricmp(s1, s2); which is not found in the MinGW string includes.
Therefor the source code was modified to:

	inline int StrCaseCmp(const char* s1, const char* s2) {
	  return strcmp(s1, s2);
	}

== Building OpenCV programs using Eclipse

https://jeanvitor.com/installing-cpp-opencv-3-2/
	
=== Second level heading

.Unordered list title
* list item 1
** nested list item
*** nested nested list item 1
*** nested nested list item 2
* list item 2

This is a paragraph.

.Example block title
====
Content in an example block is subject to normal substitutions.
====

.Sidebar title
****
Sidebars contain aside text and are subject to normal substitutions.
****

==== Third level heading

[[id-for-listing-block]]
.Listing block title
----
Content in a listing block is subject to verbatim substitutions.
Listing block content is commonly used to preserve code input.
----

===== Fourth level heading

.Table title
|===
|Column heading 1 |Column heading 2

|Column 1, row 1
|Column 2, row 1

|Column 1, row 2
|Column 2, row 2
|===

====== Fifth level heading

[quote, firstname lastname, movie title]
____
I am a block quote or a prose excerpt.
I am subject to normal substitutions.
____

[verse, firstname lastname, poem title and more]
____
I am a verse block.
  Indents and endlines are preserved in verse blocks.
____

== Reading List
http://www.pieter-jan.com/node/11 +
Reading a IMU Without Kalman: The Complementary Filter 

http://www.pieter-jan.com/node/7 +
Getting the angular position from gyroscope data 

http://www.mikrocontroller-elektronik.de/nodemcu-esp8266-tutorial-wlan-board-arduino-ide/ +
NodeMCU und ESP8266 - Einstieg in die Programmierung

http://www.mikrocontroller-elektronik.de/esp12e-tutorial-einstieg-mit-dem-esp8266-modul/ +
ESP12E-Tutorial und Beispielprojekte wie ESP12E-Adapter

https://www.rchelicopterfun.com/rc-lipo-batteries.html +
Erläuterung LIPO Batterien.


TIP: There are five admonition labels: Tip, Note, Important, Caution and Warning.

// I am a comment and won't be rendered.

. ordered list item
.. nested ordered list item
. ordered list item

The text at the end of this sentence is cross referenced to <<_third_level_heading,the third level heading>>

== First level heading

This is a link to the http://asciidoctor.org/docs/user-manual/[Asciidoctor User Manual].
This is an attribute reference {quick-uri}[which links this text to the Asciidoctor Quick Reference Guide].

