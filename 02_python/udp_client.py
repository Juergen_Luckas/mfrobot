import socket
import select

UDP_IP = "192.168.1.108"
UDP_PORT = 5006
MESSAGE = "Hello, World!"

print "UDP target IP:", UDP_IP
print "UDP target port:", UDP_PORT
print "message:", MESSAGE

sock = socket.socket(socket.AF_INET, # Internet
	socket.SOCK_DGRAM) # UDP

sock.setblocking(0)
	
sock.sendto(MESSAGE, (UDP_IP, UDP_PORT))

ready = select.select([sock], [], [], 10)
if ready[0]:
	data = sock.recv(4096)
	print "returned: ", data
	
#data, addr = sock.recvfrom(1024)
#print "echo: ", data
