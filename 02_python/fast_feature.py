import numpy as np
import cv2
from matplotlib import pyplot as plt

def nothing(x):
    pass

	

# CV_LOAD_IMAGE_UNCHANGED (<0) loads the image as is (including the alpha channel if present)
# CV_LOAD_IMAGE_GRAYSCALE ( 0) loads the image as an intensity one
# CV_LOAD_IMAGE_COLOR (>0) loads the image in the BGR format
img = cv2.imread('..\\10_images\\schrank.jpg', 1)
#cv2.imshow('original', img)
#cv2.waitKey()

dummy = np.zeros((1,1))

# Initiate FAST object with default values
fast = cv2.FastFeatureDetector_create()

# Disable nonmaxSuppression
fast.setNonmaxSuppression(True)

#cv2.imshow('setNonmaxSuppression = false', img2)
cv2.namedWindow('setNonmaxSuppression = true')
cv2.createTrackbar('Threshold', 'setNonmaxSuppression = true', 17, 255, nothing)



while(1):
	cv2.setUseOptimized(True);

	# Get refernce time for timing
	e1 = cv2.getTickCount()
	
	# Get Keypoints
	kp = fast.detect(img,None)
	
	e2 = cv2.getTickCount()
	t = (e2 - e1)/cv2.getTickFrequency()

	# Print all default params
	print ("Threshold: ", fast.getThreshold())
	print ("Total Keypoints with nonmaxSuppression: ", len(kp))
	print ("time", t )

	img3 = cv2.drawKeypoints(img, kp, dummy, color=(255,0,0))

	cv2.imshow('setNonmaxSuppression = true', img3)
	
	# ESC to exit
	k = cv2.waitKey(1) & 0xFF
	if k == 27:
		break
		
	# get current threshold from trackbar
	t = cv2.getTrackbarPos('Threshold','setNonmaxSuppression = true')

	fast.setThreshold(t);

#cv2.imwrite('fast_false.png',img3)

cv.destroyAllWindows()